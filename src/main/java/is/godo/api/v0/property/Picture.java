package is.godo.api.v0.property;

import is.godo.api.v0.json.OptionalNull;

public class Picture {
    public OptionalNull<String> getUrl() {
        return url;
    }

    public void setUrl(OptionalNull<String> url) {
        this.url = url;
    }

    private OptionalNull<String> url;

    public String toString() {
        return "url: " + url;
    }
}
