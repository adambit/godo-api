package is.godo.api.v0.property;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import is.godo.api.v0.Authentication;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetPropertyRequest {
    public Authentication getAuthentication() {
        return auth;
    }

    public void setAuthentication(Authentication auth) {
        this.auth = auth;
    }

    private Authentication auth;

}
