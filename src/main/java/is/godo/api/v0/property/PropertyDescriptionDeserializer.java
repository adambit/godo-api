package is.godo.api.v0.property;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import is.godo.api.v0.inventory.Availability;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class PropertyDescriptionDeserializer extends JsonDeserializer<PropertyDescription> {

    @Override
    public PropertyDescription deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {

        PropertyDescription obj = new PropertyDescription();
        HashMap<String,Property> map = new HashMap<>();
        HashMap<String,Room> roomHash = new HashMap<>();

        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        Iterator<Map.Entry<String, JsonNode>> nodes = node.fields();
        while(nodes.hasNext()) {
            Map.Entry<String,JsonNode> pair = nodes.next();
            String key = pair.getKey();
            JsonNode innerNode = pair.getValue();
            if (!node.isObject()) {
                throw new RuntimeException("invalid json");
            }

            if( pair.getKey().equals("rooms") ) {
                JsonNode roomNode = pair.getValue();
                System.out.println("roomNode: " + roomNode.toString() );
                Iterator<Map.Entry<String, JsonNode>> rooms = roomNode.fields();

                while( rooms.hasNext() ) {
                    Map.Entry<String,JsonNode> room = rooms.next();
                    JsonNode theRoom = room.getValue();
                    Room newRoom = jsonParser.getCodec().treeToValue(theRoom, Room.class);
                    roomHash.put( room.getKey(), newRoom );
                }
            }

            else {
                //Long propId = Long.parseLong(key);

                System.out.println("node: " + innerNode.toString());
                Property property = jsonParser.getCodec().treeToValue(innerNode, Property.class);
                System.out.println("Prop: " + property.toString());

                map.put(key, property);
            }
        }

        obj.setProperty(map);
        obj.setRooms(roomHash);

        return obj;
    }

}
