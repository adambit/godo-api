package is.godo.api.v0.property;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import is.godo.api.v0.json.Encoding;
import is.godo.api.v0.json.EncodingAnnotation;
import is.godo.api.v0.json.OptionalNull;

import java.math.BigDecimal;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@JsonInclude(NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RoomType {

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<String> name;  // `json:"name,omitempty"`

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> qty;  // `json:"qty,omitempty"`


    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> roomId;


    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<BigDecimal> minPrice;  // `json:"minPrice,omitempty"`

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> maxPeople;  // `json:"maxPeople,omitempty"`

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> maxAdult;  // `json:"maxAdult,omitempty"`

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> maxChildren;  // `json:"maxChildren,omitempty"`


    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> dependentRoomId1;  // `json:"dependentRoomId1,omitempty"`

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> dependentRoomId2;  // `json:"dependentRoomId2,omitempty"`

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> dependentRoomId3;  // `json:"dependentRoomId3,omitempty"`

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> dependentRoomId4;  // `json:"dependentRoomId4,omitempty"`

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> dependentRoomId5;  // `json:"dependentRoomId5,omitempty"`

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> dependentRoomId6;  // `json:"dependentRoomId6,omitempty"`

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> dependentRoomId7;  // `json:"dependentRoomId7,omitempty"`

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> dependentRoomId8;  // `json:"dependentRoomId8,omitempty"`

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> dependentRoomId9;  // `json:"dependentRoomId9,omitempty"`

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> dependentRoomId10;  // `json:"dependentRoomId10,omitempty"`

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> dependentRoomId11;  // `json:"dependentRoomId11,omitempty"`

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> dependentRoomId12;  // `json:"dependentRoomId12,omitempty"`


    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<RoomLogic> dependentRoomLogic; //    `json:"dependentRoomLogic,omitempty"`
    private OptionalNull<String> unitNames;  // `json:"unitNames,omitempty"`

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> unitAllocationPerGuest;  // `json:"unitAllocationPerGuest,omitempty"`

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> p1Sync;  // `json:"p1Sync,omitempty"`

//    @EncodingAnnotation(Encoding.STRING)
//    private OptionalNull<Long> p2Sync;  // `json:"p2Sync,omitempty"`

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> p3Sync;  // `json:"p3Sync,omitempty"`

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> p4Sync;  // `json:"p4Sync,omitempty"`

    // FIXME: should be a hex/color type.
    private OptionalNull<String> highlightColor;  // `json:"highlightColor,omitempty"`

    @EncodingAnnotation(Encoding.NUMBER)
    private OptionalNull<Boolean> agodaComEnableInventory;  // `json:"agodaComEnableInventory,omitempty"`
    @EncodingAnnotation(Encoding.NUMBER)
    private OptionalNull<Boolean> agodaComEnablePrice;  // `json:"agodaComEnablePrice,omitempty"`
    @EncodingAnnotation(Encoding.NUMBER)
    private OptionalNull<Boolean> agodaComEnableBooking;  // `json:"agodaComEnableBooking,omitempty"`
    private OptionalNull<String> agodaComRoomCode;  // `json:"agodaComRoomCode,omitempty"`

    @EncodingAnnotation(Encoding.NUMBER)
    private OptionalNull<Boolean> airbnbComEnableInventory;  // `json:"airbnbComEnableInventory,omitempty"`
    @EncodingAnnotation(Encoding.NUMBER)
    private OptionalNull<Boolean> airbnbComEnableBooking;  // `json:"aribnbComEnableBooking,omitempty"`
    private OptionalNull<String> airbnbComRoomCode;  // `json:"airbnbComRoomCode,omitempty"`

    @EncodingAnnotation(Encoding.NUMBER)
    private OptionalNull<Boolean> bookingComEnableInventory;  // `json:"bookingConEnableInventory,omitempty"`
    @EncodingAnnotation(Encoding.NUMBER)
    private OptionalNull<Boolean> bookingComEnableBooking;  // `json:"bookingComEnableBooking,omitempty"`
    private OptionalNull<String> bookingComRoomCode;  // `json:"bookingComRoomCode,omitempty"`
    private OptionalNull<String> bookingComRateCode;  // `json:"bookingComRateCode,omitempty"`

    @EncodingAnnotation(Encoding.NUMBER)
    private OptionalNull<Boolean> expediaComEnableInventory;  // `json:"expediaComEnableInventory,omitempty"`
    @EncodingAnnotation(Encoding.NUMBER)
    private OptionalNull<Boolean> expediaComEnablePrice;  // `json:"expediaComEnablePrice,omitempty"`
    @EncodingAnnotation(Encoding.NUMBER)
    private OptionalNull<Boolean> expediaComEnableBooking;  // `json:"expediaComEnableBooking,omitempty"`
    private OptionalNull<String> expediaComRoomCode;  // `json:"expediaComRoomCode,omitempty"`
    private OptionalNull<String> expediaComRateCode;  // `json:"expediaComRateCode,omitempty"`

    @EncodingAnnotation(Encoding.STRING_INT)
    private OptionalNull<String> icalExportEnableType;

    @EncodingAnnotation(Encoding.URL)
    private OptionalNull<String> icalExportUrl;

    @EncodingAnnotation(Encoding.STRING_INT)
    private OptionalNull<String> icalImport1EnableType;

    @EncodingAnnotation(Encoding.URL)
    private OptionalNull<String> icalImport1Url;

    @EncodingAnnotation(Encoding.STRING_INT)
    private OptionalNull<String> icalImport2EnableType;

    @EncodingAnnotation(Encoding.URL)
    private OptionalNull<String> icalImport2Url;

    @EncodingAnnotation(Encoding.STRING_INT)
    private OptionalNull<String> icalImport3EnableType;
    @EncodingAnnotation(Encoding.URL)
    private OptionalNull<String> icalImport3Url;

    @EncodingAnnotation(Encoding.STRING_INT)
    private OptionalNull<String> excludeReports;

    public OptionalNull<String> getName() {
        return name;
    }

    public void setName(OptionalNull<String> name) {
        this.name = name;
    }

    public OptionalNull<Long> getQty() {
        return qty;
    }

    public void setQty(OptionalNull<Long> qty) {
        this.qty = qty;
    }

    public OptionalNull<BigDecimal> getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(OptionalNull<BigDecimal> minPrice) {
        this.minPrice = minPrice;
    }

    public OptionalNull<Long> getMaxPeople() {
        return maxPeople;
    }

    public void setMaxPeople(OptionalNull<Long> maxPeople) {
        this.maxPeople = maxPeople;
    }

    public OptionalNull<Long> getMaxAdult() {
        return maxAdult;
    }

    public void setMaxAdult(OptionalNull<Long> maxAdult) {
        this.maxAdult = maxAdult;
    }

    public OptionalNull<Long> getMaxChildren() {
        return maxChildren;
    }

    public void setMaxChildren(OptionalNull<Long> maxChildren) {
        this.maxChildren = maxChildren;
    }

    public OptionalNull<Long> getDependentRoomId1() {
        return dependentRoomId1;
    }

    public void setDependentRoomId1(OptionalNull<Long> dependentRoomId1) {
        this.dependentRoomId1 = dependentRoomId1;
    }

    public OptionalNull<Long> getDependentRoomId2() {
        return dependentRoomId2;
    }

    public void setDependentRoomId2(OptionalNull<Long> dependentRoomId2) {
        this.dependentRoomId2 = dependentRoomId2;
    }

    public OptionalNull<Long> getDependentRoomId3() {
        return dependentRoomId3;
    }

    public void setDependentRoomId3(OptionalNull<Long> dependentRoomId3) {
        this.dependentRoomId3 = dependentRoomId3;
    }

    public OptionalNull<Long> getDependentRoomId4() {
        return dependentRoomId4;
    }

    public void setDependentRoomId4(OptionalNull<Long> dependentRoomId4) {
        this.dependentRoomId4 = dependentRoomId4;
    }

    public OptionalNull<Long> getDependentRoomId5() {
        return dependentRoomId5;
    }

    public void setDependentRoomId5(OptionalNull<Long> dependentRoomId5) {
        this.dependentRoomId5 = dependentRoomId5;
    }

    public OptionalNull<Long> getDependentRoomId6() {
        return dependentRoomId6;
    }

    public void setDependentRoomId6(OptionalNull<Long> dependentRoomId6) {
        this.dependentRoomId6 = dependentRoomId6;
    }

    public OptionalNull<Long> getDependentRoomId7() {
        return dependentRoomId7;
    }

    public void setDependentRoomId7(OptionalNull<Long> dependentRoomId7) {
        this.dependentRoomId7 = dependentRoomId7;
    }

    public OptionalNull<Long> getDependentRoomId8() {
        return dependentRoomId8;
    }

    public void setDependentRoomId8(OptionalNull<Long> dependentRoomId8) {
        this.dependentRoomId8 = dependentRoomId8;
    }

    public OptionalNull<Long> getDependentRoomId9() {
        return dependentRoomId9;
    }

    public void setDependentRoomId9(OptionalNull<Long> dependentRoomId9) {
        this.dependentRoomId9 = dependentRoomId9;
    }

    public OptionalNull<Long> getDependentRoomId10() {
        return dependentRoomId10;
    }

    public void setDependentRoomId10(OptionalNull<Long> dependentRoomId10) {
        this.dependentRoomId10 = dependentRoomId10;
    }

    public OptionalNull<Long> getDependentRoomId11() {
        return dependentRoomId11;
    }

    public void setDependentRoomId11(OptionalNull<Long> dependentRoomId11) {
        this.dependentRoomId11 = dependentRoomId11;
    }

    public OptionalNull<Long> getDependentRoomId12() {
        return dependentRoomId12;
    }

    public void setDependentRoomId12(OptionalNull<Long> dependentRoomId12) {
        this.dependentRoomId12 = dependentRoomId12;
    }

    public OptionalNull<RoomLogic> getDependentRoomLogic() {
        return dependentRoomLogic;
    }

    public void setDependentRoomLogic(OptionalNull<RoomLogic> dependentRoomLogic) {
        this.dependentRoomLogic = dependentRoomLogic;
    }

    public OptionalNull<String> getUnitNames() {
        return unitNames;
    }

    public void setUnitNames(OptionalNull<String> unitNames) {
        this.unitNames = unitNames;
    }

    public OptionalNull<Long> getUnitAllocationPerGuest() {
        return unitAllocationPerGuest;
    }

    public void setUnitAllocationPerGuest(OptionalNull<Long> unitAllocationPerGuest) {
        this.unitAllocationPerGuest = unitAllocationPerGuest;
    }

    public OptionalNull<Long> getP1Sync() {
        return p1Sync;
    }

    public void setP1Sync(OptionalNull<Long> p1Sync) {
        this.p1Sync = p1Sync;
    }

//    public OptionalNull<Long> getP2Sync() {
//        return p2Sync;
//    }
//
//    public void setP2Sync(OptionalNull<Long> p2Sync) {
//        this.p2Sync = p2Sync;
//    }

    public OptionalNull<Long> getP3Sync() {
        return p3Sync;
    }

    public void setP3Sync(OptionalNull<Long> p3Sync) {
        this.p3Sync = p3Sync;
    }

    public OptionalNull<Long> getP4Sync() {
        return p4Sync;
    }

    public void setP4Sync(OptionalNull<Long> p4Sync) {
        this.p4Sync = p4Sync;
    }

    public OptionalNull<String> getHighlightColor() {
        return highlightColor;
    }

    public void setHighlightColor(OptionalNull<String> highlightColor) {
        this.highlightColor = highlightColor;
    }

    public OptionalNull<Boolean> getAgodaComEnableInventory() {
        return agodaComEnableInventory;
    }

    public void setAgodaComEnableInventory(OptionalNull<Boolean> agodaComEnableInventory) {
        this.agodaComEnableInventory = agodaComEnableInventory;
    }

    public OptionalNull<Boolean> getAgodaComEnablePrice() {
        return agodaComEnablePrice;
    }

    public void setAgodaComEnablePrice(OptionalNull<Boolean> agodaComEnablePrice) {
        this.agodaComEnablePrice = agodaComEnablePrice;
    }

    public OptionalNull<Boolean> getAgodaComEnableBooking() {
        return agodaComEnableBooking;
    }

    public void setAgodaComEnableBooking(OptionalNull<Boolean> agodaComEnableBooking) {
        this.agodaComEnableBooking = agodaComEnableBooking;
    }

    public OptionalNull<String> getAgodaComRoomCode() {
        return agodaComRoomCode;
    }

    public void setAgodaComRoomCode(OptionalNull<String> agodaComRoomCode) {
        this.agodaComRoomCode = agodaComRoomCode;
    }

    public OptionalNull<Boolean> getAirbnbComEnableInventory() {
        return airbnbComEnableInventory;
    }

    public void setAirbnbComEnableInventory(OptionalNull<Boolean> airbnbComEnableInventory) {
        this.airbnbComEnableInventory = airbnbComEnableInventory;
    }

    public OptionalNull<Boolean> getAirbnbComEnableBooking() {
        return airbnbComEnableBooking;
    }

    public void setAirbnbComEnableBooking(OptionalNull<Boolean> airbnbComEnableBooking) {
        this.airbnbComEnableBooking = airbnbComEnableBooking;
    }

    public OptionalNull<String> getAirbnbComRoomCode() {
        return airbnbComRoomCode;
    }

    public void setAirbnbComRoomCode(OptionalNull<String> airbnbComRoomCode) {
        this.airbnbComRoomCode = airbnbComRoomCode;
    }

    public OptionalNull<Boolean> getBookingComEnableInventory() {
        return bookingComEnableInventory;
    }

    public void setBookingComEnableInventory(OptionalNull<Boolean> bookingComEnableInventory) {
        this.bookingComEnableInventory = bookingComEnableInventory;
    }

    public OptionalNull<Boolean> getBookingComEnableBooking() {
        return bookingComEnableBooking;
    }

    public void setBookingComEnableBooking(OptionalNull<Boolean> bookingComEnableBooking) {
        this.bookingComEnableBooking = bookingComEnableBooking;
    }

    public OptionalNull<String> getBookingComRoomCode() {
        return bookingComRoomCode;
    }

    public void setBookingComRoomCode(OptionalNull<String> bookingComRoomCode) {
        this.bookingComRoomCode = bookingComRoomCode;
    }

    public OptionalNull<String> getBookingComRateCode() {
        return bookingComRateCode;
    }

    public void setBookingComRateCode(OptionalNull<String> bookingComRateCode) {
        this.bookingComRateCode = bookingComRateCode;
    }

    public OptionalNull<Boolean> getExpediaComEnableInventory() {
        return expediaComEnableInventory;
    }

    public void setExpediaComEnableInventory(OptionalNull<Boolean> expediaComEnableInventory) {
        this.expediaComEnableInventory = expediaComEnableInventory;
    }

    public OptionalNull<Boolean> getExpediaComEnablePrice() {
        return expediaComEnablePrice;
    }

    public void setExpediaComEnablePrice(OptionalNull<Boolean> expediaComEnablePrice) {
        this.expediaComEnablePrice = expediaComEnablePrice;
    }

    public OptionalNull<Boolean> getExpediaComEnableBooking() {
        return expediaComEnableBooking;
    }

    public void setExpediaComEnableBooking(OptionalNull<Boolean> expediaComEnableBooking) {
        this.expediaComEnableBooking = expediaComEnableBooking;
    }

    public OptionalNull<String> getExpediaComRoomCode() {
        return expediaComRoomCode;
    }

    public void setExpediaComRoomCode(OptionalNull<String> expediaComRoomCode) {
        this.expediaComRoomCode = expediaComRoomCode;
    }

    public OptionalNull<String> getExpediaComRateCode() {
        return expediaComRateCode;
    }

    public void setExpediaComRateCode(OptionalNull<String> expediaComRateCode) {
        this.expediaComRateCode = expediaComRateCode;
    }

    public OptionalNull<Long> getRoomId() {
        return roomId;
    }

    public void setRoomId(OptionalNull<Long> roomId) {
        this.roomId = roomId;
    }

    public OptionalNull<String> getIcalExportEnableType() {
        return icalExportEnableType;
    }

    public void setIcalExportEnableType(OptionalNull<String> icalExportEnableType) {
        this.icalExportEnableType = icalExportEnableType;
    }

    public OptionalNull<String> getIcalExportUrl() {
        return icalExportUrl;
    }

    public void setIcalExportUrl(OptionalNull<String> icalExportUrl) {
        this.icalExportUrl = icalExportUrl;
    }

    public OptionalNull<String> getIcalImport1EnableType() {
        return icalImport1EnableType;
    }

    public void setIcalImport1EnableType(OptionalNull<String> icalImport1EnableType) {
        this.icalImport1EnableType = icalImport1EnableType;
    }

    public OptionalNull<String> getIcalImport1Url() {
        return icalImport1Url;
    }

    public void setIcalImport1Url(OptionalNull<String> icalImport1Url) {
        this.icalImport1Url = icalImport1Url;
    }

    public OptionalNull<String> getIcalImport2EnableType() {
        return icalImport2EnableType;
    }

    public void setIcalImport2EnableType(OptionalNull<String> icalImport2EnableType) {
        this.icalImport2EnableType = icalImport2EnableType;
    }

    public OptionalNull<String> getIcalImport2Url() {
        return icalImport2Url;
    }

    public void setIcalImport2Url(OptionalNull<String> icalImport2Url) {
        this.icalImport2Url = icalImport2Url;
    }

    public OptionalNull<String> getIcalImport3EnableType() {
        return icalImport3EnableType;
    }

    public void setIcalImport3EnableType(OptionalNull<String> icalImport3EnableType) {
        this.icalImport3EnableType = icalImport3EnableType;
    }

    public OptionalNull<String> getIcalImport3Url() {
        return icalImport3Url;
    }

    public void setIcalImport3Url(OptionalNull<String> icalImport3Url) {
        this.icalImport3Url = icalImport3Url;
    }

    public OptionalNull<String> getExcludeReports() {
        return excludeReports;
    }

    public void setExcludeReports(OptionalNull<String> excludeReports) {
        this.excludeReports = excludeReports;
    }
}
