package is.godo.api.v0.property;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import is.godo.api.v0.json.Encoding;
import is.godo.api.v0.json.EncodingAnnotation;
import is.godo.api.v0.json.OptionalNull;

import java.util.HashMap;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Property {

    private OptionalNull<String> propKey;
    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> propId;
    private OptionalNull<String> name;

    public OptionalNull<String> getPropName() {
        return propName;
    }

    public void setPropName(OptionalNull<String> propName) {
        this.propName = propName;
    }

    private OptionalNull<String> propName;
    private OptionalNull<String> propDesc1;
    private OptionalNull<String> propDesc2;
    private OptionalNull<String> propBookDesc1;
    private OptionalNull<String> propBookDesc2;

    public OptionalNull<HashMap<String, Room>> getRooms() {
        return rooms;
    }

    public void setRooms(OptionalNull<HashMap<String, Room>> rooms) {
        this.rooms = rooms;
    }

    private OptionalNull<List<Picture>> pictures;

    private OptionalNull<HashMap<String,Room>> rooms;

    public OptionalNull<String> getPropDesc1() {
        return propDesc1;
    }

    public void setPropDesc1(OptionalNull<String> propDesc1) {
        this.propDesc1 = propDesc1;
    }

    public OptionalNull<String> getPropDesc2() {
        return propDesc2;
    }

    public void setPropDesc2(OptionalNull<String> propDesc2) {
        this.propDesc2 = propDesc2;
    }

    public OptionalNull<String> getPropBookDesc1() {
        return propBookDesc1;
    }

    public void setPropBookDesc1(OptionalNull<String> propBookDesc1) {
        this.propBookDesc1 = propBookDesc1;
    }

    public OptionalNull<String> getPropBookDesc2() {
        return propBookDesc2;
    }

    public void setPropBookDesc2(OptionalNull<String> propBookDesc2) {
        this.propBookDesc2 = propBookDesc2;
    }

    public OptionalNull<List<Picture>> getPictures() {
        return pictures;
    }

    public void setPictures(OptionalNull<List<Picture>> pictures) {
        this.pictures = pictures;
    }

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> ownerId;
    private OptionalNull<String> currency;
    private OptionalNull<String> address;
    private OptionalNull<String> city;
    private OptionalNull<String> state;
    private OptionalNull<String> latitude;
    private OptionalNull<String> longitude;
    private OptionalNull<String> phone;
    private OptionalNull<String> email;
    private OptionalNull<String> web;
    private OptionalNull<String> fax;
    private OptionalNull<String> template1;
    private OptionalNull<String> template2;
    private OptionalNull<String> template3;
    private OptionalNull<String> template4;
    private OptionalNull<String> notifyUrl;
    private OptionalNull<String> agodaComPropertyCode;
    private OptionalNull<String> bookingComPropertyCode;
    private OptionalNull<String> expediaComUsername;
    private OptionalNull<String> expediaComPassword;
    private OptionalNull<String> expediaComPropertyCode;
    private OptionalNull<String> expediaComCurrency;
    private OptionalNull<String> icalExportTokenSalt;
    private OptionalNull<String> icalExportDescription;
    private OptionalNull<String> icalImportOption;
    private OptionalNull<List<RoomType>> roomTypes;
    private OptionalNull<String> action;

    public OptionalNull<String> getAction() {
        return action;
    }

    public void setAction(OptionalNull<String> action) {
        this.action = action;
    }

    public OptionalNull<String> getPropKey() {
        return propKey;
    }

    public void setPropKey(OptionalNull<String> propKey) {
        this.propKey = propKey;
    }

    public OptionalNull<Long> getPropId() {
        return propId;
    }

    public void setPropId(OptionalNull<Long> propId) {
        this.propId = propId;
    }

    public OptionalNull<String> getName() {
        return name;
    }

    public void setName(OptionalNull<String> name) {
        this.name = name;
    }

    public OptionalNull<Long> getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(OptionalNull<Long> ownerId) {
        this.ownerId = ownerId;
    }

    public OptionalNull<String> getCurrency() {
        return currency;
    }

    public void setCurrency(OptionalNull<String> currency) {
        this.currency = currency;
    }

    public OptionalNull<String> getAddress() {
        return address;
    }

    public void setAddress(OptionalNull<String> address) {
        this.address = address;
    }

    public OptionalNull<String> getCity() {
        return city;
    }

    public void setCity(OptionalNull<String> city) {
        this.city = city;
    }

    public OptionalNull<String> getState() {
        return state;
    }

    public void setState(OptionalNull<String> state) {
        this.state = state;
    }

    public OptionalNull<String> getPhone() {
        return phone;
    }

    public void setPhone(OptionalNull<String> phone) {
        this.phone = phone;
    }

    public OptionalNull<String> getEmail() {
        return email;
    }

    public void setEmail(OptionalNull<String> email) {
        this.email = email;
    }

    public OptionalNull<String> getWeb() {
        return web;
    }

    public void setWeb(OptionalNull<String> web) {
        this.web = web;
    }

    public OptionalNull<String> getLatitude() {
        return latitude;
    }

    public void setLatitude(OptionalNull<String> latitude) {
        this.latitude = latitude;
    }

    public OptionalNull<String> getLongitude() {
        return longitude;
    }

    public void setLongitude(OptionalNull<String> longitude) {
        this.longitude = longitude;
    }

    public OptionalNull<String> getTemplate1() {
        return template1;
    }

    public void setTemplate1(OptionalNull<String> template1) {
        this.template1 = template1;
    }

    public OptionalNull<String> getTemplate2() {
        return template2;
    }

    public void setTemplate2(OptionalNull<String> template2) {
        this.template2 = template2;
    }

    public OptionalNull<String> getTemplate3() {
        return template3;
    }

    public void setTemplate3(OptionalNull<String> template3) {
        this.template3 = template3;
    }

    public OptionalNull<String> getTemplate4() {
        return template4;
    }

    public void setTemplate4(OptionalNull<String> template4) {
        this.template4 = template4;
    }

    public OptionalNull<String> getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(OptionalNull<String> notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public OptionalNull<String> getAgodaComPropertyCode() {
        return agodaComPropertyCode;
    }

    public void setAgodaComPropertyCode(OptionalNull<String> agodaComPropertyCode) {
        this.agodaComPropertyCode = agodaComPropertyCode;
    }

    public OptionalNull<String> getBookingComPropertyCode() {
        return bookingComPropertyCode;
    }

    public void setBookingComPropertyCode(OptionalNull<String> bookingComPropertyCode) {
        this.bookingComPropertyCode = bookingComPropertyCode;
    }

    public OptionalNull<String> getExpediaComUsername() {
        return expediaComUsername;
    }

    public void setExpediaComUsername(OptionalNull<String> expediaComUsername) {
        this.expediaComUsername = expediaComUsername;
    }

    public OptionalNull<String> getExpediaComPassword() {
        return expediaComPassword;
    }

    public void setExpediaComPassword(OptionalNull<String> expediaComPassword) {
        this.expediaComPassword = expediaComPassword;
    }

    public OptionalNull<String> getExpediaComPropertyCode() {
        return expediaComPropertyCode;
    }

    public void setExpediaComPropertyCode(OptionalNull<String> expediaComPropertyCode) {
        this.expediaComPropertyCode = expediaComPropertyCode;
    }

    public OptionalNull<String> getExpediaComCurrency() {
        return expediaComCurrency;
    }

    public void setExpediaComCurrency(OptionalNull<String> expediaComCurrency) {
        this.expediaComCurrency = expediaComCurrency;
    }

    public OptionalNull<String> getIcalExportTokenSalt() {
        return icalExportTokenSalt;
    }

    public void setIcalExportTokenSalt(OptionalNull<String> icalExportTokenSalt) {
        this.icalExportTokenSalt = icalExportTokenSalt;
    }

    public OptionalNull<String> getIcalExportDescription() {
        return icalExportDescription;
    }

    public void setIcalExportDescription(OptionalNull<String> icalExportDescription) {
        this.icalExportDescription = icalExportDescription;
    }

    public OptionalNull<String> getIcalImportOption() {
        return icalImportOption;
    }

    public void setIcalImportOption(OptionalNull<String> icalImportOption) {
        this.icalImportOption = icalImportOption;
    }

    public OptionalNull<List<RoomType>> getRoomTypes() {
        return roomTypes;
    }

    public void setRoomTypes(OptionalNull<List<RoomType>> roomTypes) {
        this.roomTypes = roomTypes;
    }

    public OptionalNull<String> getFax() {
        return fax;
    }

    public void setFax(OptionalNull<String> fax) {
        this.fax = fax;
    }

    public String toString() {
        String result = "";
        if( name != null )
            result += name.get() + " - ";
        if( template2 != null  )
            result += template2.get() + " - ";
        return result;
    }
}

