package is.godo.api.v0.property;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import is.godo.api.v0.json.Encoding;
import is.godo.api.v0.json.EncodingAnnotation;
import is.godo.api.v0.json.OptionalNull;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PropertyListItem {

    private String name;
    private String propKey;
    private String currency;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> propId;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> ownerId;

    private List<RoomType> roomTypes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OptionalNull<Long> getPropId() {
        return propId;
    }

    public void setPropId(OptionalNull<Long> propId) {
        this.propId = propId;
    }

    public void setOwnerId(OptionalNull<Long> ownerId) {
        this.ownerId = ownerId;
    }

    public String getPropKey() {
        return propKey;
    }

    public void setPropKey(String propKey) {
        this.propKey = propKey;
    }

    public List<RoomType> getRoomTypes() {
        return roomTypes;
    }

    public void setRoomTypes(List<RoomType> roomTypes) {
        this.roomTypes = roomTypes;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public OptionalNull<Long> getOwnerId() {
        return ownerId;
    }

    @Override
    public String toString() {
        return "Property name: " + name;
    }
}
