package is.godo.api.v0.property;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import is.godo.api.v0.inventory.GetAvailabilityResponseDeserializer;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@JsonInclude(NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetDescriptionResponse {
    @JsonValue
    public PropertyDescription getProperties() {
        return properties;
    }

    public void setProperties(PropertyDescription properties) {
        this.properties = properties;
    }

    private PropertyDescription properties;
}
