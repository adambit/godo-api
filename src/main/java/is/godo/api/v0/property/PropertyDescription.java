package is.godo.api.v0.property;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@JsonDeserialize(using=PropertyDescriptionDeserializer.class)
public class PropertyDescription {

    public PropertyDescription() {

    }

    @JsonCreator
    public PropertyDescription( HashMap<String,Property> property ) {
        setProperty(property);
    }

    public HashMap<String,Property> getProperty() {
        return property;
    }

    public Property getDefaultProperty() {
        Property result = null;

        if( property != null && property.entrySet().size() > 0 ) {
            Object[] keys = property.keySet().toArray();
            result = property.get(keys[0]);
        }

        return result;
    }

    public void setProperty(HashMap<String,Property> property) {
        this.property = property;
    }

    private HashMap<String,Property> property;

    private HashMap<String,Room> rooms;

    public HashMap<String, Room> getRooms() {
        return rooms;
    }

    public void setRooms(HashMap<String, Room> rooms) {
        this.rooms = rooms;
    }

    @JsonIgnore
    public List<Room> getRoomList() {
        return new ArrayList<>(getRooms().values());
    }

    public String toString() {
        if( property != null )
            return property.toString();
        return "No property set";
    }

    /*
    @JsonAnySetter
    public void setMapProperty(String name, Property value){
        setProperty(value);
    }
    */

}
