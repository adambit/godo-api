package is.godo.api.v0.property;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import is.godo.api.v0.json.BaseResponse;

import java.util.List;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@JsonInclude(NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetPropertiesResponse extends BaseResponse {
    private List<PropertyListItem> getProperties;


    public List<PropertyListItem> getGetProperties() {
        return getProperties;
    }

    public void setGetProperties(List<PropertyListItem> getProperties) {
        this.getProperties = getProperties;
    }
}
