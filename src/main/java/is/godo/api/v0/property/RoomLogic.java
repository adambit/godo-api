package is.godo.api.v0.property;


import is.godo.api.v0.json.NumericEnum;

public class RoomLogic  extends NumericEnum {
    public static final RoomLogic ALL_ROOMS = new RoomLogic(0L);
    public static final RoomLogic ANY_ROOM = new RoomLogic(1L);
    public static final RoomLogic SUM_OF_ALL_BOOKINGS = new RoomLogic(2L);

    public RoomLogic(Long id) {
        super(id);
    }

    public RoomLogic(String id) {
        super(id);
    }


    public Boolean equals(RoomLogic other) {
        return super.equals(other);
    }
}
