package is.godo.api.v0.property;

import com.fasterxml.jackson.annotation.*;
import is.godo.api.v0.json.BaseResponse;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetPropertyResponse extends BaseResponse {
    @JsonProperty("getProperty")
    private List<Property> property;

    public GetPropertyResponse() {
    }

    public GetPropertyResponse(List<Property> property) {
        this.property = property;
    }

    @JsonValue
    public List<Property> getProperty() {
        return property;
    }

    public void setProperty(List<Property> property) {
        this.property = property;
    }
}
