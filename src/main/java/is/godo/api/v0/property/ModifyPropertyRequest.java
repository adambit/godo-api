package is.godo.api.v0.property;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import is.godo.api.v0.Authentication;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ModifyPropertyRequest {
    private Authentication authentication;

    private ArrayList<Property> modifyProperty;

    public Authentication getAuthentication() {
        return authentication;
    }

    public void setAuthentication(Authentication auth) {
        this.authentication = auth;
    }

    public ArrayList<Property> getModifyProperty() {
        return modifyProperty;
    }

    public void setModifyProperty(ArrayList<Property> modifyProperty) {
        this.modifyProperty = modifyProperty;
    }
}
