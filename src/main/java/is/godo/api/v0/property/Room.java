package is.godo.api.v0.property;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import is.godo.api.v0.json.OptionalNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Room {

    OptionalNull<Long> roomId;
    OptionalNull<String> roomName;
    OptionalNull<String> roomType;
    OptionalNull<String> roomDesc1;
    OptionalNull<Integer> sellPriority;
    OptionalNull<Integer> maxPeople;
    OptionalNull<Integer> minStay;
    OptionalNull<Integer> maxStay;
    OptionalNull<Integer> maxChildren;

    OptionalNull<Map<String,Offer>> offers;

    public OptionalNull<Map<String, Offer>> getOffers() {
        return offers;
    }

    public void setOffers(OptionalNull<Map<String, Offer>> offers) {
        this.offers = offers;
    }

    public OptionalNull<List<Picture>> getPictures() {
        return pictures;
    }

    public void setPictures(OptionalNull<List<Picture>> pictures) {
        this.pictures = pictures;
    }

    OptionalNull<Integer> maxAdult;
    OptionalNull<List<Picture>> pictures;

    public OptionalNull<String> getRoomType() {
        return roomType;
    }

    public void setRoomType(OptionalNull<String> roomType) {
        this.roomType = roomType;
    }

    public OptionalNull<String> getRoomDesc1() {
        return roomDesc1;
    }

    public void setRoomDesc1(OptionalNull<String> roomDesc1) {
        this.roomDesc1 = roomDesc1;
    }

    public OptionalNull<Integer> getSellPriority() {
        return sellPriority;
    }

    public void setSellPriority(OptionalNull<Integer> sellPriority) {
        this.sellPriority = sellPriority;
    }

    public OptionalNull<Integer> getMaxPeople() {
        return maxPeople;
    }

    public void setMaxPeople(OptionalNull<Integer> maxPeople) {
        this.maxPeople = maxPeople;
    }

    public OptionalNull<Integer> getMinStay() {
        return minStay;
    }

    public void setMinStay(OptionalNull<Integer> minStay) {
        this.minStay = minStay;
    }

    public OptionalNull<Integer> getMaxStay() {
        return maxStay;
    }

    public void setMaxStay(OptionalNull<Integer> maxStay) {
        this.maxStay = maxStay;
    }

    public OptionalNull<Integer> getMaxChildren() {
        return maxChildren;
    }

    public void setMaxChildren(OptionalNull<Integer> maxChildren) {
        this.maxChildren = maxChildren;
    }

    public OptionalNull<Integer> getMaxAdult() {
        return maxAdult;
    }

    public void setMaxAdult(OptionalNull<Integer> maxAdult) {
        this.maxAdult = maxAdult;
    }

    public OptionalNull<Long> getRoomId() {
        return roomId;
    }

    public void setRoomId(OptionalNull<Long> roomId) {
        this.roomId = roomId;
    }

    public OptionalNull<String> getRoomName() {
        return roomName;
    }

    public void setRoomName(OptionalNull<String> roomName) {
        this.roomName = roomName;
    }

    public String toString() {
        String result = "ID: " + roomId + ", room name: " + roomName;
        if( pictures != null && pictures.isPresent() ) {
            result += "\n - Pictures - \n";
            for( Picture p : pictures.get() ) {
                result += p.toString();
            }
        }

        return result;
    }

    public List<Offer> getOfferList() {
        return new ArrayList<>(offers.get().values());
    }
}
