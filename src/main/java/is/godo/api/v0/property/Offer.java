package is.godo.api.v0.property;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import is.godo.api.v0.json.OptionalNull;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Offer {

    private OptionalNull<Long> offerId;
    private OptionalNull<String> offerName;
    private OptionalNull<String> offerDesc1;
    private OptionalNull<List<Picture>> pictures;

    public OptionalNull<List<Picture>> getPictures() {
        return pictures;
    }

    public void setPictures(OptionalNull<List<Picture>> pictures) {
        this.pictures = pictures;
    }

    public OptionalNull<Long> getOfferId() {
        return offerId;
    }

    public void setOfferId(OptionalNull<Long> offerId) {
        this.offerId = offerId;
    }

    public OptionalNull<String> getOfferName() {
        return offerName;
    }

    public void setOfferName(OptionalNull<String> offerName) {
        this.offerName = offerName;
    }

    public OptionalNull<String> getOfferDesc1() {
        return offerDesc1;
    }

    public void setOfferDesc1(OptionalNull<String> offerDesc1) {
        this.offerDesc1 = offerDesc1;
    }

    public OptionalNull<String> getOfferDesc2() {
        return offerDesc2;
    }

    public void setOfferDesc2(OptionalNull<String> offerDesc2) {
        this.offerDesc2 = offerDesc2;
    }

    private OptionalNull<String> offerDesc2;

}
