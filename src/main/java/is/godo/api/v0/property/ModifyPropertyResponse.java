package is.godo.api.v0.property;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import is.godo.api.v0.json.BaseResponse;

import java.util.ArrayList;
import java.util.HashMap;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ModifyPropertyResponse extends BaseResponse{
    private ArrayList<HashMap<String,String>> modifyProperty;

    public ArrayList<HashMap<String, String>> getModifyProperty() {
        return modifyProperty;
    }

    public void setModifyProperty(ArrayList<HashMap<String, String>> modifyProperty) {
        this.modifyProperty = modifyProperty;
    }
}
