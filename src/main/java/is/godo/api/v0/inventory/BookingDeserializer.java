package is.godo.api.v0.inventory;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import is.godo.api.v0.booking.*;
import is.godo.api.v0.json.OptionalNull;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

public class BookingDeserializer extends JsonDeserializer<Booking> {
    private static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyyMMdd");

    @Override
    public Booking deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        Map<Long,Availability> obj = new HashMap<>();
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        ObjectMapper mapper = new ObjectMapper();

        System.out.println("In deserializer");
        Booking booking = new Booking();

        try {
            Iterator<Map.Entry<String, JsonNode>> nodes = node.fields();
            JsonNode errorNode = node.get("error");
            if (errorNode != null) {
                String error = errorNode.asText();
                throw new IOException("API call returned an error: " + error);
            }
            booking.setBookId(new OptionalNull<>(node.get("bookId").asLong()));
            booking.setRoomId(new OptionalNull<>(node.get("roomId").asLong()));
            booking.setUnitId(new OptionalNull<>(node.get("unitId").asLong()));
            booking.setRoomQty(new OptionalNull<>(node.get("roomQty").asLong()));
            long status = node.get("status").asLong();
            BookingStatus bookingStatus = new BookingStatus(status);
            booking.setStatus(new OptionalNull<>(bookingStatus));
            booking.setFirstNight(new OptionalNull<>(FORMAT.parse(node.get("firstNight").asText())));
            booking.setLastNight(new OptionalNull<>(FORMAT.parse(node.get("lastNight").asText())));
            booking.setNumAdult(new OptionalNull<>(node.get("numAdult").asLong()));
            booking.setNumChild(new OptionalNull<>(node.get("numChild").asLong()));

            booking.setGuestTitle(new OptionalNull<>(node.get("guestTitle").asText()));
            booking.setGuestName(new OptionalNull<>(node.get("guestName").asText()));
            booking.setGuestFirstName(new OptionalNull<>(node.get("guestFirstName").asText()));
            booking.setGuestEmail(new OptionalNull<>(node.get("guestEmail").asText()));
            booking.setGuestPhone(new OptionalNull<>(node.get("guestPhone").asText()));
            booking.setGuestMobile(new OptionalNull<>(node.get("guestMobile").asText()));
            booking.setGuestFax(new OptionalNull<>(node.get("guestFax").asText()));
            booking.setGuestAddress(new OptionalNull<>(node.get("guestAddress").asText()));
            booking.setGuestCity(new OptionalNull<>(node.get("guestCity").asText()));
            booking.setGuestPostcode(new OptionalNull<>(node.get("guestPostcode").asText()));
            booking.setGuestCountry(new OptionalNull<>(node.get("guestCountry").asText()));
            booking.setGuestCountry2(new OptionalNull<>(node.get("guestCountry2").asText()));
            booking.setGuestVoucher(new OptionalNull<>(node.get("guestVoucher").asText()));
            booking.setGuestComments(new OptionalNull<>(node.get("guestComments").asText()));
            booking.setNotes(new OptionalNull<>(node.get("notes").asText()));
            booking.setMessage(new OptionalNull<>(node.get("message").asText()));
            booking.setCustom1(new OptionalNull<>(node.get("custom1").asText()));
            booking.setCustom2(new OptionalNull<>(node.get("custom2").asText()));
            booking.setCustom3(new OptionalNull<>(node.get("custom3").asText()));
            booking.setCustom4(new OptionalNull<>(node.get("custom4").asText()));
            booking.setCustom5(new OptionalNull<>(node.get("custom5").asText()));
            booking.setCustom6(new OptionalNull<>(node.get("custom6").asText()));
            booking.setCustom7(new OptionalNull<>(node.get("custom7").asText()));
            booking.setCustom8(new OptionalNull<>(node.get("custom8").asText()));
            booking.setCustom9(new OptionalNull<>(node.get("custom9").asText()));
            booking.setCustom10(new OptionalNull<>(node.get("custom10").asText()));

            booking.setFlagColor(new OptionalNull<>(node.get("flagColor").asText()));
            booking.setFlagText(new OptionalNull<>(node.get("flagText").asText()));
            booking.setLang(new OptionalNull<>(node.get("lang").asText()));

            booking.setPrice(new OptionalNull<>(new BigDecimal(node.get("price").asDouble())));
            booking.setDeposit(new OptionalNull<>(new BigDecimal(node.get("deposit").asDouble())));
            booking.setTax(new OptionalNull<>(new BigDecimal(node.get("tax").asDouble())));
            booking.setCommission(new OptionalNull<>(new BigDecimal(node.get("commission").asDouble())));

            booking.setCurrency(new OptionalNull<>(node.get("currency").asText()));
            booking.setOfferId(new OptionalNull<>(node.get("offerId").asLong()));
            booking.setRateDescription(new OptionalNull<>(node.get("rateDescription").asText()));
            booking.setReferer(new OptionalNull<>(node.get("referer").asText()));
            booking.setRefererEditable(new OptionalNull<>(node.get("refererEditable").asText()));
            booking.setReference(new OptionalNull<>(node.get("reference").asText()));
            Long apiSourceNum = node.get("apiSource").asLong();
            Channel apiSource = new Channel(apiSourceNum);
            booking.setApiSource(new OptionalNull<>(apiSource));
            booking.setApiReference(new OptionalNull<>(node.get("apiReference").asText()));
            booking.setApiMessage(new OptionalNull<>(node.get("apiMessage").asText()));
            booking.setMasterId(new OptionalNull<>(node.get("masterId").asLong()));

            booking.setBookingTime(new OptionalNull<>(FORMAT.parse(node.get("bookingTime").asText())));
            booking.setModified(new OptionalNull<>(FORMAT.parse(node.get("modified").asText())));

            JsonNode invoiceNode = node.get("invoice");
            booking.setInvoice(new OptionalNull<>(mapper.readValue(invoiceNode.asText(), mapper.getTypeFactory().constructCollectionType(List.class, InvoiceLine.class))));

            JsonNode infoNode = node.get("infoItems");
            booking.setInfoItems(new OptionalNull<>(mapper.readValue(infoNode.asText(), mapper.getTypeFactory().constructCollectionType(List.class, InfoItem.class))));

            booking.setInvoiceNumber(new OptionalNull<>(node.get("invoiceNumber").asLong()));
            booking.setAllowChannelUpdate(new OptionalNull<>(node.get("allowChannelUpdate").asBoolean()));


            Iterator group = node.get("group").elements();
            List<Long> groupList = new ArrayList<>();
            while (group.hasNext()) {
                Map.Entry<String, JsonNode> entry = (Map.Entry<String, JsonNode>) group.next();
                JsonNode item = entry.getValue();
                System.out.println("This is the item we are about to parse: " + item.asText());
                // InvoiceLine invoiceLine = mapper.readValue(entry)
            }

/*
            while (nodes.hasNext()) {
                Map.Entry<String, JsonNode> entry = (Map.Entry<String,JsonNode>) nodes.next();

                if( entry.getKey().matches("([12]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\\d|3[01]))") ) {
                    // Key is a date
                    System.out.println("******Key is a date");
                    JsonNode dailyAvailability = entry.getValue().get("avail");

                    DailyAvailability a = mapper.readValue(dailyAvailability.asText(), DailyAvailability.class);
                    dateString = entry.getKey();
                    avMap.put( FORMAT.parse(dateString), a );
                    System.out.println("date: " + dateString + " dailyAvailability: " + a.toString());
                }
            }
            gdar.setDailyAvailability(avMap);


            dateString = node.get("checkIn").asText();
            gar.setCheckIn( new OptionalNull<>(FORMAT.parse(dateString)));
            dateString = node.get("lastNight").asText();
            gar.setLastNight( new OptionalNull<>(FORMAT.parse(dateString)));
            dateString = node.get("checkOut").asText();
            gar.setCheckOut( new OptionalNull<>(FORMAT.parse(dateString)));
            Long propId = node.get("propId").asLong();
            gar.setPropId(new OptionalNull<>(propId));
            Long numAdult = node.get("numAdult").asLong();
            gar.setNumAdult(new OptionalNull<>(numAdult));

            while(nodes.hasNext()) {
                Map.Entry<String,JsonNode> pair = nodes.next();
                String roomIdStr = pair.getKey();

                Long roomId = Long.parseLong(roomIdStr);

                JsonNode availabilityNode = pair.getValue();
                if (!availabilityNode.isObject()){
                    throw new RuntimeException("invalid json");
                }

                Availability availability = jsonParser.getCodec().treeToValue(availabilityNode, Availability.class);

                obj.put(roomId, availability);

            }
        } catch (Exception e) {
            throw new IOException(e.getMessage() + " - received json: " + node.toString());
        }

        return obj;*/
        } catch (Exception e) {
            throw new IOException(e.getMessage() + " - received json: " + node.toString());
        }

        return null;
    }

}
