package is.godo.api.v0.inventory;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import is.godo.api.v0.json.BaseResponse;

import java.util.Date;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonDeserialize(using=RoomDatesResponseDeserializer.class)
public class GetRoomDatesResponse extends BaseResponse {

    @JsonSerialize(using=RoomDatesMapSerializer.class)
    @JsonDeserialize(using=RoomDatesMapDeserializer.class)
    private Map<Date,RoomDate> roomDates;

    public GetRoomDatesResponse() {}
/*
    @JsonCreator
    public GetRoomDatesResponse(Map<Long,Availability> availabilities) {
        this.availabilities = availabilities;
    }
    */

    public Map<Date, RoomDate> getRoomDates() {
        return roomDates;
    }

    public void setRoomDates(Map<Date, RoomDate> roomDates) {
        this.roomDates = roomDates;
    }
}
