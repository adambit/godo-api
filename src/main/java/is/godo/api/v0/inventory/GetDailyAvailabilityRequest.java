package is.godo.api.v0.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import is.godo.api.v0.json.EncodingAnnotation;
import is.godo.api.v0.json.OptionalNull;

import java.util.Date;
import java.util.List;

/**
 * Created by: Gísli Leifsson
 *
 * Example document to send:
 * {
 *  "checkIn": "20180501",
 *  "checkOut": "20180508",
 *  "roomIds": [
 *  34500,
 *  34501
 *  ]
 *  }
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetDailyAvailabilityRequest {
    @EncodingAnnotation(pattern="yyyyMMdd")
    private OptionalNull<Date> checkIn;

    @EncodingAnnotation(pattern="yyyyMMdd")
    private OptionalNull<Date> checkOut;

    public OptionalNull<Date> getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(OptionalNull<Date> checkIn) {
        this.checkIn = checkIn;
    }

    public OptionalNull<Date> getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(OptionalNull<Date> checkOut) {
        this.checkOut = checkOut;
    }

    public OptionalNull<Boolean> getQtyAvailable() {
        return qtyAvailable;
    }

    public void setQtyAvailable(OptionalNull<Boolean> qtyAvailable) {
        this.qtyAvailable = qtyAvailable;
    }

    public OptionalNull<List<Long>> getRoomIds() {
        return roomIds;
    }

    public void setRoomIds(OptionalNull<List<Long>> roomIds) {
        this.roomIds = roomIds;
    }

    private OptionalNull<List<Long>> roomIds;

    private OptionalNull<Boolean> qtyAvailable;
}
