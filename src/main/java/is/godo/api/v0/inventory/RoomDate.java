package is.godo.api.v0.inventory;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import is.godo.api.v0.json.Encoding;
import is.godo.api.v0.json.EncodingAnnotation;
import is.godo.api.v0.json.OptionalNull;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RoomDate {

    @JsonProperty(value = "p1")
    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<BigDecimal> price1;

    @JsonProperty(value = "p2")
    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<BigDecimal> price2;

    @JsonProperty(value = "p3")
    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<BigDecimal> price3;

    @JsonProperty(value = "p4")
    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<BigDecimal> price4;

    @JsonProperty(value="i")
    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> inventory;

    @JsonProperty(value="m")
    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> minimumStay;
    @JsonProperty(value = "o")
    private OptionalNull<OverrideStatus> overrideStatus;

    @JsonProperty(value = "x")
    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<BigDecimal> multiplier;

    public OptionalNull<Long> getInventory() {
        return inventory;
    }

    public void setInventory(OptionalNull<Long> inventory) {
        this.inventory = inventory;
    }

    public OptionalNull<Long> getMinimumStay() {
        return minimumStay;
    }

    public void setMinimumStay(OptionalNull<Long> minimumStay) {
        this.minimumStay = minimumStay;
    }

    public OptionalNull<BigDecimal> getPrice1() {
        return price1;
    }

    public void setPrice1(OptionalNull<BigDecimal> price1) {
        this.price1 = price1;
    }

    public OptionalNull<BigDecimal> getPrice2() {
        return price2;
    }

    public void setPrice2(OptionalNull<BigDecimal> price2) {
        this.price2 = price2;
    }

    public OptionalNull<BigDecimal> getPrice3() {
        return price3;
    }

    public void setPrice3(OptionalNull<BigDecimal> price3) {
        this.price3 = price3;
    }

    public OptionalNull<BigDecimal> getPrice4() {
        return price4;
    }

    public void setPrice4(OptionalNull<BigDecimal> price4) {
        this.price4 = price4;
    }

    public OptionalNull<OverrideStatus> getOverrideStatus() {
        return overrideStatus;
    }

    public void setOverrideStatus(OptionalNull<OverrideStatus> overrideStatus) {
        this.overrideStatus = overrideStatus;
    }

    @JsonIgnore
    public OptionalNull<BigDecimal> getMultiplierDecimal() {

        if(multiplier != null && multiplier.isPresent() && multiplier.get() != null){
            // convert from percentage to decimal.
            OptionalNull<BigDecimal> b = new OptionalNull<>(new BigDecimal("0.01").multiply(multiplier.get().setScale(0,BigDecimal.ROUND_HALF_UP)));
            b.setEncoding(multiplier.getEncoding());
            return b;
        }
        return multiplier;
    }

    @JsonIgnore
    public void setMultiplierDecimal(OptionalNull<BigDecimal> multiplier) {
        if(multiplier != null && multiplier.isPresent() && multiplier.get() != null){
            // convert from decimal to percentage
            this.multiplier = new OptionalNull<>(new BigDecimal("100").multiply(multiplier.get()).setScale(0, BigDecimal.ROUND_HALF_UP));
            this.multiplier.setEncoding(multiplier.getEncoding());
            return;
        }
        this.multiplier = multiplier;
    }



    public OptionalNull<BigDecimal> getMultiplier(){
        return multiplier;
    }
    public void setMultiplier(OptionalNull<BigDecimal> multiplier){
        this.multiplier=multiplier;
    }

    @Override
    public String toString() {
        String result = "Inventory: " + inventory.get().toString() + "\n";
        return result;
    }
}
