package is.godo.api.v0.inventory;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import is.godo.api.v0.GodoPropertyException;
import is.godo.api.v0.json.OptionalNull;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class GetAvailabilityResponseDeserializer extends StdDeserializer<GetAvailabilityResponse> {

    private static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyyMMdd");

    public GetAvailabilityResponseDeserializer() {
        this(null);
    }

    public GetAvailabilityResponseDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public GetAvailabilityResponse deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException {
        GetAvailabilityResponse gar = new GetAvailabilityResponse();

        JsonNode node = jp.getCodec().readTree(jp);
        ObjectMapper mapper = new ObjectMapper();

        String dateString = "";

        try {
            Map<Long,Availability> avMap = new HashMap<>();
            dateString = node.get("checkIn").asText();
            gar.setCheckIn( new OptionalNull<>(FORMAT.parse(dateString)));
            dateString = node.get("lastNight").asText();
            if (!dateString.isEmpty())
              gar.setLastNight( new OptionalNull<>(FORMAT.parse(dateString)));
            dateString = node.get("checkOut").asText();
            gar.setCheckOut( new OptionalNull<>(FORMAT.parse(dateString)));
            Long propId = node.get("propId").asLong();
            gar.setPropId(new OptionalNull<>(propId));
            Long numAdult = node.get("numAdult").asLong();
            gar.setNumAdult(new OptionalNull<>(numAdult));

            Iterator<Map.Entry<String, JsonNode>> nodes = node.fields();
            while (nodes.hasNext()) {
                Map.Entry<String, JsonNode> entry = (Map.Entry<String,JsonNode>) nodes.next();

                if( entry.getKey().matches("-?\\d+(\\.\\d+)?") ) {
                    //System.out.println("   key is a number");
                    Long roomid = Long.valueOf(entry.getKey());
                    Availability a = mapper.readValue(entry.getValue().toString(), Availability.class);
                    avMap.put( roomid, a );
                    //System.out.println("roomId: " + roomid + " a: " + a.toString());
                }
            }
            gar.setAvailabilities(avMap);
        }
        catch(ParseException pex) {
            System.err.println("Cannot parse date: '" + dateString + "'");
        }
        catch( Exception e ) {
            throw new IOException("Error while deserializing getAvailabilityResponse. " + e.getMessage()
            + " - received json: " + node.toString());
        }

        return gar;
    }
}
