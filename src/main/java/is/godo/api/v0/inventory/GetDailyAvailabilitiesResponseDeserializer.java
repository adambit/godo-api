package is.godo.api.v0.inventory;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import is.godo.api.v0.json.OptionalNull;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class GetDailyAvailabilitiesResponseDeserializer extends JsonDeserializer<GetDailyAvailabilityResponse> {

    private static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyyMMdd");


    @Override
    public GetDailyAvailabilityResponse deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException {
        GetDailyAvailabilityResponse gdar = new GetDailyAvailabilityResponse();

        JsonNode node = jp.getCodec().readTree(jp);
        ObjectMapper mapper = new ObjectMapper();

        String dateString = "";

        try {

            JsonNode errorNode  = node.get("error");
            if(errorNode != null) {
                String error = errorNode.asText();
                throw new IOException("API call returned an error: " + error);
            }

            Map<Date,DailyAvailability> avMap = new HashMap<>();

            Iterator<Map.Entry<String, JsonNode>> nodes = node.fields();
            while (nodes.hasNext()) {
                Map.Entry<String, JsonNode> entry = (Map.Entry<String,JsonNode>) nodes.next();

                if( entry.getKey().matches("([12]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\\d|3[01]))") ) {
                    // Key is a date
                    System.out.println("******Key is a date");
                    JsonNode dailyAvailability = entry.getValue().get("avail");

                    DailyAvailability a = mapper.readValue(dailyAvailability.asText(), DailyAvailability.class);
                    dateString = entry.getKey();
                    avMap.put( FORMAT.parse(dateString), a );
                    System.out.println("date: " + dateString + " dailyAvailability: " + a.toString());
                }
            }
            gdar.setDailyAvailability(avMap);
        }
        catch(ParseException pex) {
            System.err.println("Cannot parse date: '" + dateString + "'");
        }
        catch( Exception e ) {
            throw new IOException("Error while deserializing getAvailabilityResponse. " + e.getMessage()
                    + " - received json: " + node.toString());
        }

        return gdar;
    }

}
