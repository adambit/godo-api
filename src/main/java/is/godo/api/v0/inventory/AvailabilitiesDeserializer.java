package is.godo.api.v0.inventory;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import org.omg.CORBA.SystemException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by zuzana on 2.11.2017.
 */
public class AvailabilitiesDeserializer extends JsonDeserializer<Map<Long,Availability>> {
    @Override
    public Map<Long, Availability> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        Map<Long,Availability> obj = new HashMap<>();
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        System.out.println("In deserializer");

        try {
            Iterator<Map.Entry<String, JsonNode>> nodes = node.fields();
            JsonNode errorNode  = node.get("error");
            if(errorNode != null) {
                String error = errorNode.asText();
                throw new IOException("API call returned an error: " + error);
            }
            while(nodes.hasNext()) {
                System.out.println("Made it inside of if loop");
                Map.Entry<String,JsonNode> pair = nodes.next();
                String roomIdStr = pair.getKey();
                System.out.println("Got this roomIdStr:" + roomIdStr);
                Long roomId = Long.parseLong(roomIdStr);

                System.out.println("Got this roomId long: " + roomId);

                JsonNode availabilityNode = pair.getValue();
                if (!availabilityNode.isObject()){
                    throw new RuntimeException("invalid json");
                }

                System.out.println("About to parse availability");

                Availability availability = jsonParser.getCodec().treeToValue(availabilityNode, Availability.class);

                System.out.println("Parsed availability");

                obj.put(roomId, availability);

            }
        } catch (Exception e) {
            throw new IOException(e.getMessage() + " - received json: " + node.toString());
        }

        return obj;
    }
}
