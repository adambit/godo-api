package is.godo.api.v0.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import is.godo.api.v0.json.Encoding;
import is.godo.api.v0.json.EncodingAnnotation;
import is.godo.api.v0.json.OptionalNull;

import java.util.Date;

/**
 * Created by zuzana on 2.11.2017.
 *
 * The getAvailabilites parameters checkIn, (one of lastNight or checkOut)
 * and (one of roomId, propId or ownerId) are required.
 * To return a price at least one of numAdult or numChild should be specified.
 * apiKey and propKey are not required to authenticate access to this function.
 * If both lastNight and checkOut are specified, lastNight will be ignored.
 *
 * Example data:
 *     {
 *      "checkIn": "20151001",
 *      "lastNight": "20151002",
 *      "checkOut": "20151003",
 *      "roomId": "12345",
 *      "propId": "1234",
 *      "ownerId": "123",
 *      "numAdult": "2",
 *      "numChild": "0",
 *      "offerId": "1",
 *      "voucherCode": "",
 *      "ignoreAvail": false
 *     }
 */
// https://api.godo.is/v0/getAvailabilities
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetAvailabilityRequest {

    @EncodingAnnotation(pattern="yyyyMMdd")
    private OptionalNull<Date> checkIn;

    @EncodingAnnotation(pattern="yyyyMMdd")
    private OptionalNull<Date> lastNight;

    @EncodingAnnotation(pattern="yyyyMMdd")
    private OptionalNull<Date> checkOut;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> roomId;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> propId;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> ownerId;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> numAdult;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> numChild;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> offerId;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<String> voucherCode;

    @EncodingAnnotation(Encoding.BOOLEAN)
    private OptionalNull<Boolean> ignoreAvail;

    public OptionalNull<Date> getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(OptionalNull<Date> checkIn) {
        this.checkIn = checkIn;
    }

    public OptionalNull<Date> getLastNight() {
        return lastNight;
    }

    public void setLastNight(OptionalNull<Date> lastNight) {
        this.lastNight = lastNight;
    }

    public OptionalNull<Date> getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(OptionalNull<Date> checkOut) {
        this.checkOut = checkOut;
    }

    public OptionalNull<Long> getRoomId() {
        return roomId;
    }

    public void setRoomId(OptionalNull<Long> roomId) {
        this.roomId = roomId;
    }

    public OptionalNull<Long> getPropId() {
        return propId;
    }

    public void setPropId(OptionalNull<Long> propId) {
        this.propId = propId;
    }

    public OptionalNull<Long> getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(OptionalNull<Long> ownerId) {
        this.ownerId = ownerId;
    }

    public OptionalNull<Long> getNumAdult() {
        return numAdult;
    }

    public void setNumAdult(OptionalNull<Long> numAdult) {
        this.numAdult = numAdult;
    }

    public OptionalNull<Long> getNumChild() {
        return numChild;
    }

    public void setNumChild(OptionalNull<Long> numChild) {
        this.numChild = numChild;
    }

    public OptionalNull<Long> getOfferId() {
        return offerId;
    }

    public void setOfferId(OptionalNull<Long> offerId) {
        this.offerId = offerId;
    }

    public OptionalNull<String> getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(OptionalNull<String> voucherCode) {
        this.voucherCode = voucherCode;
    }

    public OptionalNull<Boolean> getIgnoreAvail() {
        return ignoreAvail;
    }

    public void setIgnoreAvail(OptionalNull<Boolean> ignoreAvail) {
        this.ignoreAvail = ignoreAvail;
    }
}
