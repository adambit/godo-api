package is.godo.api.v0.inventory;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import is.godo.api.v0.Authentication;
import is.godo.api.v0.json.Encoding;
import is.godo.api.v0.json.EncodingAnnotation;
import is.godo.api.v0.json.OptionalNull;

import java.util.Date;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SetRoomDatesRequest {
    private Authentication authentication;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> roomId;

    @JsonSerialize(using=RoomDatesMapSerializer.class)
    @JsonDeserialize(using=RoomDatesMapDeserializer.class)
    private Map< Date, RoomDate> dates;

    public Authentication getAuthentication() {
        return authentication;
    }

    public void setAuthentication(Authentication authentication) {
        this.authentication = authentication;
    }

    public OptionalNull<Long> getRoomId() {
        return roomId;
    }

    public void setRoomId(OptionalNull<Long> roomId) {
        this.roomId = roomId;
    }

    public Map<Date, RoomDate> getDates() {
        return dates;
    }

    public void setDates(Map<Date, RoomDate> dates) {
        this.dates = dates;
    }
}
