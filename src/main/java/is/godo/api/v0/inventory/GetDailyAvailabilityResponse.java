package is.godo.api.v0.inventory;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.Date;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
// @JsonDeserialize(using=AvailabilitiesDeserializer.class)
public class GetDailyAvailabilityResponse {

    Map<Date, DailyAvailability> dailyAvailability;

    public GetDailyAvailabilityResponse() {
    }

    public Map<Date, DailyAvailability> getDailyAvailability() {
        return dailyAvailability;
    }

    public void setDailyAvailability(Map<Date, DailyAvailability> dailyAvailability) {
        this.dailyAvailability = dailyAvailability;
    }

    @JsonCreator
    public GetDailyAvailabilityResponse(Map<Date,DailyAvailability> availability) {
        this.dailyAvailability = availability;
    }

}
