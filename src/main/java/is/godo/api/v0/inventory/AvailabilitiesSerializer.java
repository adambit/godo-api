package is.godo.api.v0.inventory;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Map;

/**
 * Created by zuzana on 2.11.2017.
 */
public class AvailabilitiesSerializer extends JsonSerializer<Map<Long,Availability>> {

    @Override
    public void serialize(Map<Long, Availability> availabilityMap, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeStartObject();
        availabilityMap.forEach(
                (Long roomId, Availability availability) -> {
                    try{
                        jsonGenerator.writeObjectField(roomId.toString(), availability);
                    } catch (IOException e){
                        throw new UncheckedIOException(e);
                    }
        });
        jsonGenerator.writeEndArray();
    }


}

