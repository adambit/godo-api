package is.godo.api.v0.inventory;

import com.fasterxml.jackson.annotation.*;

import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DailyAvailability {

    public List<AvailabilityEntry> getRoomsAvailable() {
        return roomsAvailable;
    }

    public void setRoomsAvailable(List<AvailabilityEntry> roomsAvailable) {
        this.roomsAvailable = roomsAvailable;
    }

    @JsonProperty("avail")
    private List<AvailabilityEntry> roomsAvailable;

    public DailyAvailability() {

    }

    @Override
    public String toString() {
        String result = "";
        if( roomsAvailable != null ) {
            for (AvailabilityEntry ae : roomsAvailable) {
                result += ae + ", ";
            }
        }
        return result;
    }
}
