package is.godo.api.v0.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import is.godo.api.v0.json.Encoding;
import is.godo.api.v0.json.EncodingAnnotation;
import is.godo.api.v0.json.OptionalNull;

/**
 * Created by zuzana on 2.11.2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Availability {

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> roomId;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> propId;

    @EncodingAnnotation(Encoding.NUMBER)
    private OptionalNull<Long> roomsavail;

    @EncodingAnnotation(Encoding.NUMBER)
    private OptionalNull<Long> price;

    @EncodingAnnotation(Encoding.NUMBER)
    @JsonProperty("priceFormated")
    private OptionalNull<String> priceFormatted;

    @Override
    public String toString() {
        String result = "";
        result += "Room ID: " + roomId + "\n"
                + "Prop ID: " + propId + "\n"
                + "Rooms Avail: " + roomsavail + "\n"
                + "Price: " + price + "\n"
                + "Price formatted: " + priceFormatted + "\n";

        return result;
    }

    public OptionalNull<Long> getRoomId() {
        return roomId;
    }

    public void setRoomId(OptionalNull<Long> roomId) {
        this.roomId = roomId;
    }

    public OptionalNull<Long> getPropId() {
        return propId;
    }

    public void setPropId(OptionalNull<Long> propId) {
        this.propId = propId;
    }

    public OptionalNull<Long> getRoomsavail() {
        return roomsavail;
    }

    public void setRoomsavail(OptionalNull<Long> roomsavail) {
        this.roomsavail = roomsavail;
    }

    public OptionalNull<Long> getPrice() {
        return price;
    }

    public void setPrice(OptionalNull<Long> price) {
        this.price = price;
    }

    public OptionalNull<String> getPriceFormatted() {
        return priceFormatted;
    }

    public void setPriceFormatted(OptionalNull<String> priceFormatted) {
        this.priceFormatted = priceFormatted;
    }
}
