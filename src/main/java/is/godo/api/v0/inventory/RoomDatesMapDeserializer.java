package is.godo.api.v0.inventory;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class RoomDatesMapDeserializer extends JsonDeserializer<Map<Date,RoomDate>> {

    private static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyyMMdd");

    @Override
    public Map<Date, RoomDate> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        Map<Date,RoomDate> obj = new HashMap<>();

        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        Iterator<Map.Entry<String, JsonNode>> nodes = node.fields();
        try {
            while(nodes.hasNext()) {
                Map.Entry<String, JsonNode> pair = nodes.next();
                String dateStr = pair.getKey();

                Date date = FORMAT.parse(dateStr);

                JsonNode roomTypeNode = pair.getValue();
                if(!roomTypeNode.isObject()){
                    throw new RuntimeException("invalid json");
                }
                RoomDate roomDate = jsonParser.getCodec().treeToValue(roomTypeNode, RoomDate.class);

                obj.put(date, roomDate);
            }

        } catch (Exception e) {
            throw new RuntimeException(e.getMessage() + " - received json: " + node.toString());
        }

        return obj;
    }
}
