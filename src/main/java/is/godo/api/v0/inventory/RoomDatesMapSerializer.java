package is.godo.api.v0.inventory;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class RoomDatesMapSerializer extends JsonSerializer<Map<Date,RoomDate>> {
    private final static SimpleDateFormat FORMAT = new SimpleDateFormat("yyyyMMdd");

    @Override
    public void serialize(Map<Date, RoomDate> dateRoomDateMap, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeStartObject();
        dateRoomDateMap.forEach((Date date,RoomDate rd) -> {
            try {
                String str = FORMAT.format(date);
                jsonGenerator.writeObjectField(str, rd);

            } catch(IOException e){
                throw new UncheckedIOException(e);
            }
        } );
        jsonGenerator.writeEndObject();
    }
}
