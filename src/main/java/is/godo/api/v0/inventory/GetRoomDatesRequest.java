package is.godo.api.v0.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import is.godo.api.v0.Authentication;
import is.godo.api.v0.json.Encoding;
import is.godo.api.v0.json.EncodingAnnotation;
import is.godo.api.v0.json.OptionalNull;

import java.util.Date;

/**
 *Example Data
 *  {
 *  "authentication":
 *      {
 *      "apiKey": "apiKeyAsSetInAccountSettings",
 *      "propKey": "propKeyAsSetForTheProperty"
 *      },
 *  "roomId": 12345,
 *  "from": "20171118",
 *  "to": "20171217",
 *  "incOverride": 0,
 *  "incMultiplier": 0,
 *  "allowInventoryNegative": 0
 *  }
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetRoomDatesRequest {

    private Authentication authentication;

    @EncodingAnnotation(Encoding.NUMBER)
    private OptionalNull<Long> roomId;

    @EncodingAnnotation(pattern="yyyyMMdd")
    private OptionalNull<Date> from;

    @EncodingAnnotation(pattern="yyyyMMdd")
    private OptionalNull<Date> to;

    @EncodingAnnotation(Encoding.NUMBER)
    private OptionalNull<Boolean> incOverride;
    @EncodingAnnotation(Encoding.NUMBER)
    private OptionalNull<Boolean> incMultiplier;
    @EncodingAnnotation(Encoding.NUMBER)
    private OptionalNull<Boolean> allowInventoryNegative;

    public Authentication getAuthentication() {
        return authentication;
    }

    public void setAuthentication(Authentication authentication) {
        this.authentication = authentication;
    }

    public OptionalNull<Long> getRoomId() {
        return roomId;
    }

    public void setRoomId(OptionalNull<Long> roomId) {
        this.roomId = roomId;
    }

    public OptionalNull<Date> getFrom() {
        return from;
    }

    public void setFrom(OptionalNull<Date> from) {
        this.from = from;
    }

    public OptionalNull<Date> getTo() {
        return to;
    }

    public void setTo(OptionalNull<Date> to) {
        this.to = to;
    }

    public OptionalNull<Boolean> getIncOverride() {
        return incOverride;
    }

    public void setIncOverride(OptionalNull<Boolean> incOverride) {
        this.incOverride = incOverride;
    }

    public OptionalNull<Boolean> getIncMultiplier() {
        return incMultiplier;
    }

    public void setIncMultiplier(OptionalNull<Boolean> incMultiplier) {
        this.incMultiplier = incMultiplier;
    }

    public OptionalNull<Boolean> getAllowInventoryNegative() {
        return allowInventoryNegative;
    }

    public void setAllowInventoryNegative(OptionalNull<Boolean> allowInventoryNegative) {
        this.allowInventoryNegative = allowInventoryNegative;
    }
}
