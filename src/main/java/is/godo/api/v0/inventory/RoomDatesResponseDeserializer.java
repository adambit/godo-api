package is.godo.api.v0.inventory;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import is.godo.api.v0.GodoPropertyException;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class RoomDatesResponseDeserializer extends StdDeserializer<GetRoomDatesResponse> {

    private static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyyMMdd");

    public RoomDatesResponseDeserializer() {
        this(null);
    }

    public RoomDatesResponseDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public GetRoomDatesResponse deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException, JsonProcessingException {
        GetRoomDatesResponse gr = new GetRoomDatesResponse();

        JsonNode node = jp.getCodec().readTree(jp);
        ObjectMapper mapper = new ObjectMapper();

        String dateString = "";

        try {
            Map<Date, RoomDate> mymap = new HashMap<>();
            Iterator<Map.Entry<String, JsonNode>> nodes = node.fields();
            JsonNode errorNode  = node.get("error");
            if(errorNode != null) {
                String error = errorNode.asText();
                throw new IOException("API call returned an error: " + error);
            }
            while (nodes.hasNext()) {
                Map.Entry<String, JsonNode> entry = (Map.Entry<String, JsonNode>) nodes.next();
                //if( entry.getKey().matches("-?\\d+(\\.\\d+)?") ) {
                    dateString = entry.getKey().toString();
                    Date date = FORMAT.parse(dateString);
                    RoomDate x = mapper.readValue(entry.getValue().toString(), RoomDate.class);
                    mymap.put( date, x );
                    gr.setRoomDates(mymap);
                //}
            }
        }
        catch( IOException ex ) {
            throw new IOException(ex.getMessage() + " received JSON: " + node.toString());
        }
        catch( ParseException pex ) {
            throw new IOException("Response contained a date that couldn't be parsed: " + dateString);
        }

        return gr;
    }
}
