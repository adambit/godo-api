package is.godo.api.v0.inventory;


import is.godo.api.v0.json.NumericEnum;

/**
 * OverrideStatus represents the overridden availability of a RoomType at a specific date.
 *
 */
public class OverrideStatus  extends NumericEnum {

    // NONE is the default value.
    public final static OverrideStatus NONE = new OverrideStatus(0L);

    // BLACKOUT means nothing can be booked at that date.
    public final static OverrideStatus BLACKOUT = new OverrideStatus(1L);

    // NO_CHECKIN means no guest can checkin at that date e.g. roomtype is not available if first night has NO_CHECKIN.
    public final static OverrideStatus NO_CHECKIN = new OverrideStatus(2L);

    // NO_CHECKOUT means no guest can checkout at that date e.g. roomtype is not available if last night +1 has NO_CHECKOUT.
    public final static OverrideStatus NO_CHECKOUT = new OverrideStatus(3L);

    // NO_CHECKIN_OR_CHECKOUT means no guest can checkin or checkout that date e.g. roomtype is not available if first night or last nights has NO_CHECKIN_OR_CHECKOUT.
    public final static OverrideStatus NO_CHECKIN_OR_CHECKOUT = new OverrideStatus(3L);

    // FIXME: No clue...
    public final static OverrideStatus EXCEPTIONAL_PERIOD = new OverrideStatus(2L);

    public OverrideStatus(Long id) {
        super(id);
    }

    public OverrideStatus(String id) {
        super(id);
    }


    public Boolean equals(OverrideStatus other) {
        return super.equals(other);
    }
}
