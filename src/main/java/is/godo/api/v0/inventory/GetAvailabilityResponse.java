package is.godo.api.v0.inventory;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import is.godo.api.v0.json.BaseResponse;
import is.godo.api.v0.json.OptionalNull;

import java.util.Date;
import java.util.Map;

/**
 * Created by zuzana on 2.11.2017.

 *
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonDeserialize(using=GetAvailabilityResponseDeserializer.class)
public class GetAvailabilityResponse extends BaseResponse {

    private OptionalNull<Date> checkIn;
    private OptionalNull<Date> lastNight;
    private OptionalNull<Date> checkOut;
    private OptionalNull<Long> propId;
    private OptionalNull<Long> numAdult;

    @JsonSerialize(using=AvailabilitiesSerializer.class)
    @JsonDeserialize(using=AvailabilitiesDeserializer.class)
    private Map<Long,Availability> availabilities;

    public GetAvailabilityResponse() {
    }

    @JsonCreator
    public GetAvailabilityResponse(Map<Long,Availability> availabilities) {
        this.availabilities = availabilities;
    }

    @JsonValue
    public Map<Long, Availability> getAvailabilities() {
        return availabilities;
    }

    public void setAvailabilities(Map<Long, Availability> availabilities) {
        this.availabilities = availabilities;
    }

    public OptionalNull<Date> getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(OptionalNull<Date> checkIn) {
        this.checkIn = checkIn;
    }

    public OptionalNull<Date> getLastNight() {
        return lastNight;
    }

    public void setLastNight(OptionalNull<Date> lastNight) {
        this.lastNight = lastNight;
    }

    public OptionalNull<Date> getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(OptionalNull<Date> checkOut) {
        this.checkOut = checkOut;
    }

    public OptionalNull<Long> getPropId() {
        return propId;
    }

    public void setPropId(OptionalNull<Long> propId) {
        this.propId = propId;
    }

    public OptionalNull<Long> getNumAdult() {
        return numAdult;
    }

    public void setNumAdult(OptionalNull<Long> numAdult) {
        this.numAdult = numAdult;
    }

    @Override
    public String toString() {
        return "GetAvailabilityResponse: checkIn=" + checkIn + "\n"
                + "lastNight: " + lastNight + "\n"
                + "checkOut: " + checkOut;
    }
}
