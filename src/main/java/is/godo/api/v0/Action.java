package is.godo.api.v0;

import com.fasterxml.jackson.annotation.JsonValue;

// Acton represents different actions when using various setters in the godo API.
public enum Action {
    NEW("new"),
    MODIFY("modify"),
    DELETE("delete");

    public final String jsonValue;

    Action(String s){
       this.jsonValue = s;
    }

    @Override
    @JsonValue
    public String toString() {
        return this.jsonValue;
    }

    public Boolean equals(Action other){
        return this.jsonValue.equals(other.jsonValue);
    }

}
