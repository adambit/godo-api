package is.godo.api.v0;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import is.godo.api.v0.account.Account;
import is.godo.api.v0.account.GetAccountRequest;
import is.godo.api.v0.account.GetAccountResponse;
import is.godo.api.v0.booking.*;
import is.godo.api.v0.inventory.*;
import is.godo.api.v0.json.BaseResponse;
import is.godo.api.v0.json.EncodingAnnotationApplier;
import is.godo.api.v0.json.OptionalNull;
import is.godo.api.v0.property.*;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.*;

import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.SimpleClientHttpRequestFactory;

public class GodoPropertyClient {
    private String baseEndpoint = "https://api.beds24.com/json";
    //private String baseEndpoint = "https://api.godo.is/v0";
    private static final String GET_ACCOUNT = "/getAccount";
    private static final String GET_ROOM_DATES = "/getRoomDates";
    private static final String SET_ROOM_DATES = "/setRoomDates";
    private static final String GET_BOOKINGS = "/getBookings";
    private static final String SET_BOOKING = "/setBooking";
    private static final String GET_PROPERTY = "/getProperty";
    private static final String SET_PROPERTY = "/setProperty";
    private static final String GET_PROPERTIES = "/getProperties";
    private static final String GET_AVAILABILITIES = "/getAvailabilities";
    private static final String GET_DAILY_AVAILABILITIES = "/getDateAvailabilities";
    private static final String GET_INVOICEES = "/getInvoicees";
    private static final String GET_DESCRIPTION = "/getDescriptions";
    private static final String MODIFY_PROPERTY = "/modifyProperty";


    private RestTemplate rt = new RestTemplate();
    private ObjectMapper om;

    private RestTemplate rt2 = new RestTemplate(new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory()));

    public GodoPropertyClient() {
        om = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.setSerializerModifier(new EncodingAnnotationApplier());
        om.registerModule(module);
    }

    public GodoPropertyClient(String url) {
        this();
        this.baseEndpoint = url;
    }

    private HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

    public Account getAccount(String apiKey, boolean verbose ) throws GodoPropertyException {
        Authentication auth = new Authentication(apiKey);
        GetAccountRequest gar = new GetAccountRequest();
        gar.setAuthentication(auth);

        List<ClientHttpRequestInterceptor> interceptors = new java.util.ArrayList<>();
        if( verbose ) {
            interceptors.add(new is.godo.api.v0.LoggingRequestInterceptor());
        }
        rt2.setInterceptors(interceptors);

        HttpEntity<GetAccountRequest> req = new HttpEntity<>(gar, getHeaders());

        ResponseEntity<GetAccountResponse> resp = rt2.exchange(baseEndpoint + GET_ACCOUNT, HttpMethod.POST, req, GetAccountResponse.class);

        //GetRoomDatesResponse body = resp.getBody();
        return resp.getBody().getAccounts().get(0); //body.getRoomDates();
    }

    public PropertyDescription getDescription( Long propId ) {
        return getDescription( propId, false );
    }

    public PropertyDescription getDescription( Long propId, boolean verbose ) {
        GetDescriptionRequest r = new GetDescriptionRequest();
        r.setPropId( propId );
        r.setRoomId( "true" );

        List<ClientHttpRequestInterceptor> interceptors = new java.util.ArrayList<>();
        if( verbose ) {
            interceptors.add(new is.godo.api.v0.LoggingRequestInterceptor());
        }
        rt2.setInterceptors(interceptors);

        HttpEntity<GetDescriptionRequest> req = new HttpEntity<>(r, getHeaders());
        ResponseEntity<GetDescriptionResponse> resp = rt2.exchange(baseEndpoint + GET_DESCRIPTION, HttpMethod.POST, req, GetDescriptionResponse.class);

        return resp.getBody().getProperties();
    }

    public Map<Date, RoomDate> getRoomDates(String apiKey,
                                            String propKey,
                                            GetRoomDatesRequest ent)
            throws GodoPropertyException {
        return getRoomDates( apiKey, propKey, ent, false );
    }

    public Map<Date, RoomDate> getRoomDates(String apiKey,
                                            String propKey,
                                            GetRoomDatesRequest ent,
                                            boolean verbose)
            throws GodoPropertyException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        Authentication auth = new Authentication(apiKey, propKey);
        ent.setAuthentication(auth);

        List<ClientHttpRequestInterceptor> interceptors = new java.util.ArrayList<>();
        //interceptors.add(new is.godo.api.v0.LoggingRequestInterceptor());
        rt2.setInterceptors(interceptors);

        HttpEntity<GetRoomDatesRequest> req = new HttpEntity<>(ent, headers);
        ResponseEntity<GetRoomDatesResponse> resp = rt2.exchange(baseEndpoint + GET_ROOM_DATES, HttpMethod.POST, req, GetRoomDatesResponse.class);

        GetRoomDatesResponse body = resp.getBody();

        return body.getRoomDates();
    }

    public List<PropertyListItem> getProperties(String apiKey) throws GodoPropertyException {
        GetPropertiesRequest ent = new GetPropertiesRequest();
        Authentication auth = new Authentication(apiKey);
        ent.setAuthentication(auth);

        List<ClientHttpRequestInterceptor> interceptors = new java.util.ArrayList<>();
        //interceptors.add(new is.godo.api.v0.LoggingRequestInterceptor());
        rt2.setInterceptors(interceptors);

        HttpEntity<String> req;
        try {
            req = new HttpEntity<>(om.writeValueAsString(ent));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        ResponseEntity<GetPropertiesResponse> resp = rt2.exchange(baseEndpoint + GET_PROPERTIES, HttpMethod.POST, req, GetPropertiesResponse.class);

        GetPropertiesResponse body = resp.getBody();
        checkIsApiError(body);

        return body.getGetProperties();
    }

    public Property getProperty(GetPropertyRequest req) throws GodoPropertyException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        List<ClientHttpRequestInterceptor> interceptors = new java.util.ArrayList<>();
        //interceptors.add(new is.godo.api.v0.LoggingRequestInterceptor());
        rt2.setInterceptors(interceptors);

        HttpEntity<GetPropertyRequest> req2 = new HttpEntity<>(req, headers);
        ResponseEntity<GetPropertyResponse> resp = rt2.exchange(baseEndpoint + GET_PROPERTY, HttpMethod.POST, req2, GetPropertyResponse.class);

        GetPropertyResponse body = resp.getBody();
        checkIsApiError(body);

        return body.getProperty().get(0);
    }

    public Property getProperty(String apiKey, String propKey) throws GodoPropertyException {
        GetPropertyRequest req = new GetPropertyRequest();
        Authentication auth = new Authentication(apiKey, propKey);
        req.setAuthentication(auth);
        return getProperty(req);
    }

    public HashMap<String,String> modifyProperty( String apiKey, String propKey, Property p ) throws GodoPropertyException {
        ModifyPropertyRequest req = new ModifyPropertyRequest();
        Authentication auth = new Authentication(apiKey, propKey);
        req.setAuthentication(auth);
        ArrayList<Property> a = new ArrayList<>();
        if( p.getAction() == null ) {
            p.setAction(new OptionalNull<>("modify"));
        }
        a.add(p);
        req.setModifyProperty(a);
        HttpHeaders headers = getHeaders();

        List<ClientHttpRequestInterceptor> interceptors = new java.util.ArrayList<>();

        if (false) {
            interceptors.add(new is.godo.api.v0.LoggingRequestInterceptor());
        }

        rt2.setInterceptors(interceptors);

        HttpEntity<ModifyPropertyRequest> req2 = new HttpEntity<>(req, headers);
        ResponseEntity<ModifyPropertyResponse> resp = rt2.exchange(baseEndpoint + MODIFY_PROPERTY, HttpMethod.POST, req2, ModifyPropertyResponse.class);

        ModifyPropertyResponse body = resp.getBody();
        checkIsApiError(body);

        return body.getModifyProperty().get(0);
    }

    public List<Booking> getBookings(GetBookingsRequest req) throws GodoPropertyException {
        return getBookings(req, false);
    }

    public List<Booking> getBookings(GetBookingsRequest req, boolean verbose) throws GodoPropertyException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        List<ClientHttpRequestInterceptor> interceptors = new java.util.ArrayList<>();
        if (verbose) {
            interceptors.add(new is.godo.api.v0.LoggingRequestInterceptor());
        }
        rt2.setInterceptors(interceptors);

        HttpEntity<GetBookingsRequest> req2 = new HttpEntity<>(req, headers);
        ResponseEntity<GetBookingsResponse> resp = rt2.exchange(baseEndpoint + GET_BOOKINGS, HttpMethod.POST, req2, GetBookingsResponse.class);

        GetBookingsResponse body = resp.getBody();
        checkIsApiError(body);

        return body.getBookings();
    }

    public List<Booking> getBookings(String apiKey, String propKey, Long bookId) throws GodoPropertyException {
        GetBookingsRequest br = new GetBookingsRequest();
        Authentication auth = new Authentication(apiKey, propKey);
        br.setAuthentication(auth);
        br.setBookId(new OptionalNull<>(bookId));
        return getBookings(br);
    }

    public List<Booking> getBookingsByMasterId( String apiKey, String propKey, Long masterId) throws GodoPropertyException {
        GetBookingsRequest br = new GetBookingsRequest();
        Authentication auth = new Authentication(apiKey, propKey);
        br.setAuthentication(auth);
        br.setMasterId(new OptionalNull<>(masterId));
        return getBookings(br);
    }

    public Long setMessageForBooking(String apiKey, String propKey, Long bookId, String message) throws GodoPropertyException {
        SetBookingRequest req = new SetBookingRequest();
        Authentication auth = new Authentication(apiKey, propKey);

        req.setAuthentication(auth);
        req.setBookId(new OptionalNull<>(bookId));
        req.setMessage(new OptionalNull<>(message));
        req.setCustom10(new OptionalNull<>(message));

        return setBooking(req);
    }

    public Long setBooking(SetBookingRequest req) throws GodoPropertyException {
        return setBooking(req, false);
    }

    public Long setBooking(SetBookingRequest req, boolean verbose) throws GodoPropertyException {
        Long result = null;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        List<ClientHttpRequestInterceptor> interceptors = new java.util.ArrayList<>();
        if (verbose) {
            interceptors.add(new is.godo.api.v0.LoggingRequestInterceptor());
        }
        rt2.setInterceptors(interceptors);

        HttpEntity<SetBookingRequest> req2 = new HttpEntity<>(req, headers);
        ResponseEntity<SetBookingResponse> resp = rt2.exchange(baseEndpoint + SET_BOOKING, HttpMethod.POST, req2, SetBookingResponse.class);

        checkIsApiError(resp.getBody());

        result = resp.getBody().getBookId();
        return result;
    }

    public Map<Long, Availability> getAvailabilities(GetAvailabilityRequest req)
            throws GodoPropertyException {
        return getAvailabilities( req, false);
    }

        public  Map<Long, Availability>  getAvailabilities(GetAvailabilityRequest req, boolean verbose)
            throws GodoPropertyException {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<GetAvailabilityRequest> req2 = new HttpEntity<>(req, headers);

        List<ClientHttpRequestInterceptor> interceptors = new java.util.ArrayList<>();
        if( verbose ) {
            interceptors.add(new is.godo.api.v0.LoggingRequestInterceptor());
        }
        rt2.setInterceptors(interceptors);

        ResponseEntity<GetAvailabilityResponse> resp = rt2.exchange(baseEndpoint + GET_AVAILABILITIES, HttpMethod.POST, req2, GetAvailabilityResponse.class);

        GetAvailabilityResponse body = resp.getBody();

        return body.getAvailabilities();
    }

    public  Map<Date, DailyAvailability>  getDailyAvailabilities(GetDailyAvailabilityRequest req)
            throws GodoPropertyException {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<GetDailyAvailabilityRequest> req2 = new HttpEntity<>(req, headers);

        List<ClientHttpRequestInterceptor> interceptors = new java.util.ArrayList<>();
        interceptors.add(new is.godo.api.v0.LoggingRequestInterceptor());

        rt2.setInterceptors(interceptors);

        ResponseEntity<GetDailyAvailabilityResponse> resp = rt2.exchange(baseEndpoint + GET_DAILY_AVAILABILITIES, HttpMethod.POST, req2, GetDailyAvailabilityResponse.class);
        GetDailyAvailabilityResponse body = resp.getBody();

        return body.getDailyAvailability();
    }

    public List<Invoicee> getInvoicees(GetInvoiceesRequest req)
    throws GodoPropertyException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<GetInvoiceesRequest> req2 = new HttpEntity<>(req, headers);

        List<ClientHttpRequestInterceptor> interceptors = new java.util.ArrayList<>();
        //interceptors.add(new is.godo.api.v0.LoggingRequestInterceptor());
        rt2.setInterceptors(interceptors);

        ResponseEntity<GetInvoiceesResponse> resp = rt2.exchange(baseEndpoint + GET_INVOICEES, HttpMethod.POST, req2, GetInvoiceesResponse.class);
        GetInvoiceesResponse body = resp.getBody();

        return body.getInvoicees();
    }

    public HashMap<Long,Invoicee> getInvoiceesForApiKey( String apiKey )
            throws GodoPropertyException {
        GetInvoiceesRequest ir = new GetInvoiceesRequest();
        Authentication auth = new Authentication(apiKey,"");
        ir.setAuthentication(auth);
        List<Invoicee> invoicees = getInvoicees(ir);

        HashMap<Long,Invoicee> result = new HashMap<>();
        for( Invoicee inv : invoicees )
            result.put(inv.getId().get(), inv);

        return result;
    }

        public String getBaseEndpoint() {
        return baseEndpoint;
    }

    public void setBaseEndpoint(String baseEndpoint) {
        this.baseEndpoint = baseEndpoint;
    }

    // checkIsApiError throws GodoPropertyException if the api returns an error (like authentication etc).
    private void checkIsApiError(BaseResponse resp) throws GodoPropertyException {
        if(!resp.isSuccess()){
            throw new GodoPropertyException(resp.getError(), resp.getErrorCode());
        }
    }
}
