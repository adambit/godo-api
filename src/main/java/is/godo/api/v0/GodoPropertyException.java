package is.godo.api.v0;

import is.godo.api.v0.json.ErrorCode;

public class GodoPropertyException extends Exception {

    private ErrorCode errorCode;

    public GodoPropertyException(String message) {
        super(message);

    }

    public GodoPropertyException(String message, Throwable cause) {
        super(message, cause);

    }
    public GodoPropertyException(String message, ErrorCode errorCode){
        super(message);
        this.errorCode=errorCode;
    }

    public GodoPropertyException(String message, Throwable cause, ErrorCode errorCode){
        super(message, cause);
        this.errorCode=errorCode;
    }


    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }
}
