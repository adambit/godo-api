package is.godo.api.v0.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OptionalNullSerializer<T> extends JsonSerializer<OptionalNull<T>> {
/*
    public OptionalNullSerializer() {
        this(null);
    }

    public OptionalNullSerializer(Class<OptionalNull> t){
        super(t);
    }*/


    public void serialize(OptionalNull<T> optionalNull, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
            throws IOException, JsonProcessingException {

        // if isPresent returns false then the field was not filtered
        if(!optionalNull.isPresent()){
            throw new IOException("not supposed to serialize optional nulls if they are not present");
        }



        if(optionalNull.get() instanceof Boolean){
            serialize_bool((OptionalNull<Boolean>)optionalNull, jsonGenerator, serializerProvider);
            return;
        }
        else if (optionalNull.get() instanceof Long) {
            serialize_long((OptionalNull<Long>)optionalNull,jsonGenerator,serializerProvider);
            return;
        }
        else if (optionalNull.get() instanceof BigDecimal) {
            serialize_bigdecimal((OptionalNull<BigDecimal>)optionalNull,jsonGenerator,serializerProvider);
            return;
        }
        else if (optionalNull.get() instanceof NumericEnum){
            serialize_numeric_enum((OptionalNull<NumericEnum>)optionalNull, jsonGenerator, serializerProvider);
            return;
        }
        else if (optionalNull.get() instanceof String){
            serialize_string((OptionalNull<String>)optionalNull, jsonGenerator, serializerProvider);
            return;
        }
        else if (optionalNull.get() instanceof Date){
            serialize_date((OptionalNull<Date>)optionalNull, jsonGenerator, serializerProvider);
            return;
        }
        serializerProvider.defaultSerializeValue(optionalNull.get(), jsonGenerator);

    }

    public void serialize_string(OptionalNull<String> optionalNull, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
            throws IOException, JsonProcessingException {
        if(optionalNull.getEncoding() == Encoding.URL){

            jsonGenerator.writeRawValue("\""+StringUtils.replace(optionalNull.get(), "/", "\\/")+"\"");
            //serializerProvider.defaultSerializeValue(StringUtils.replace(optionalNull.get(), "/", "\/"), jsonGenerator);
            return;
        }
        serializerProvider.defaultSerializeValue(optionalNull.get(), jsonGenerator);
    }

    public void serialize_bool(OptionalNull<Boolean> optionalNull, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
            throws IOException, JsonProcessingException {

        // if isPresent returns false then the field was not filtered
        if(!optionalNull.isPresent()){
            throw new IOException("not supposed to serialize optional nulls if they are not present");
        }
        switch(optionalNull.getEncoding()){
            case BOOLEAN:
                serializerProvider.defaultSerializeValue(optionalNull.get(), jsonGenerator);
                break;
            case NUMBER:
                serializerProvider.defaultSerializeValue(optionalNull.get() ? 1 : 0, jsonGenerator);
                break;
            case STRING:
                serializerProvider.defaultSerializeValue(optionalNull.get() ? "true" : "false", jsonGenerator);
                break;
            case STRING_INT:
                serializerProvider.defaultSerializeValue(optionalNull.get() ? "1" : "0", jsonGenerator);
                break;
            default:
                serializerProvider.defaultSerializeValue(optionalNull.get(), jsonGenerator);
        }
    }
    public void serialize_long(OptionalNull<Long> optionalNull, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
            throws IOException, JsonProcessingException {
      //  System.out.println(optionalNull.getEncoding());
        // if isPresent returns false then the field was not filtered
        if(!optionalNull.isPresent()){
            throw new IOException("not supposed to serialize optional nulls if they are not present");
        }
        switch(optionalNull.getEncoding()){
            case BOOLEAN:
                if(optionalNull.get() == 0) {
                    serializerProvider.defaultSerializeValue(false, jsonGenerator);
                    return;
                }
                serializerProvider.defaultSerializeValue(optionalNull.get(), jsonGenerator);
                break;
            case NUMBER:
                serializerProvider.defaultSerializeValue(optionalNull.get() , jsonGenerator);
                break;
            case STRING:
                serializerProvider.defaultSerializeValue(optionalNull.get().toString(), jsonGenerator);
                break;
            case STRING_INT:
                serializerProvider.defaultSerializeValue(optionalNull.get().toString(), jsonGenerator);
                break;
            default:
                serializerProvider.defaultSerializeValue(optionalNull.get(), jsonGenerator);
        }
    }

    public void serialize_numeric_enum(OptionalNull<NumericEnum> numericEnum, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
            throws IOException, JsonProcessingException {
       // System.out.println(numericEnum.getEncoding());
        // if isPresent returns false then the field was not filtered
        if(!numericEnum.isPresent()){
            throw new IOException("not supposed to serialize optional nulls if they are not present");
        }
        switch(numericEnum.getEncoding()){
            case BOOLEAN:
                if(numericEnum.get().getId() == 0) {
                    serializerProvider.defaultSerializeValue(false, jsonGenerator);
                }
                serializerProvider.defaultSerializeValue(numericEnum.get().getId(), jsonGenerator);
                break;
            case NUMBER:
                serializerProvider.defaultSerializeValue(numericEnum.get().getId() , jsonGenerator);
                break;
            case STRING:
                serializerProvider.defaultSerializeValue(numericEnum.get().getId().toString(), jsonGenerator);
                break;
            case STRING_INT:
                serializerProvider.defaultSerializeValue(numericEnum.get().getId().toString(), jsonGenerator);
                break;
            default:
                serializerProvider.defaultSerializeValue(numericEnum.get().getId(), jsonGenerator);
        }
    }

    public void serialize_bigdecimal(OptionalNull<BigDecimal> optionalNull, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
            throws IOException, JsonProcessingException {
      //  System.out.println(optionalNull.getEncoding());
        // if isPresent returns false then the field was not filtered
        if(!optionalNull.isPresent()){
            throw new IOException("not supposed to serialize optional nulls if they are not present");
        }
        switch(optionalNull.getEncoding()){
            case BOOLEAN:
                if(optionalNull.get().equals(new BigDecimal(0))) {
                    serializerProvider.defaultSerializeValue(false, jsonGenerator);
                }
                serializerProvider.defaultSerializeValue(optionalNull.get(), jsonGenerator);
                break;
            case NUMBER:
                serializerProvider.defaultSerializeValue(optionalNull.get() , jsonGenerator);
                break;
            case STRING:
                serializerProvider.defaultSerializeValue(optionalNull.get().toString(), jsonGenerator);
                break;
            case STRING_INT:
                serializerProvider.defaultSerializeValue(optionalNull.get().toString(), jsonGenerator);
                break;
            default:
                serializerProvider.defaultSerializeValue(optionalNull.get(), jsonGenerator);
        }
    }


    public void serialize_date(OptionalNull<Date> optionalNull, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
            throws IOException, JsonProcessingException {
    //    System.out.println(optionalNull.getEncoding());
        // if isPresent returns false then the field was not filtered
        if(!optionalNull.isPresent()){
            throw new IOException("not supposed to serialize optional nulls if they are not present");
        }

        if(optionalNull.getPattern() != null) {
            SimpleDateFormat fmt = new SimpleDateFormat(optionalNull.getPattern());
            String dateStr = fmt.format(optionalNull.get());
            serializerProvider.defaultSerializeValue(dateStr, jsonGenerator);
            return;
        }
        serializerProvider.defaultSerializeValue(optionalNull.get(), jsonGenerator);

    }



}
