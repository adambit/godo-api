package is.godo.api.v0.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(using = NumericEnumSerializer.class)
public abstract class NumericEnum {
    private final Long id;

    @JsonCreator
    public NumericEnum(Long id){
        this.id = id;
    }

    @JsonCreator
    public NumericEnum(String id){
        this.id = Long.valueOf(id);

    }

    @JsonGetter
    public Long getId() {
        return id;
    }

    public Boolean equals(NumericEnum other){
        return this.id.equals(other.id);
    }

    @JsonValue
    public String toString(){
        return id.toString();
    }
}
