package is.godo.api.v0.json;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class EncodingApplier {
    public static Object apply(Object o){
        try {
            //System.out.println(t.getClass().toGenericString());
            if (o.getClass().isAnnotationPresent(EncodingAnnotation.class)) {
                //System.out.println("optionalNull2 has annotation");
            } else {
                // System.out.println("optionalNull2 does not have annotation");
            }

            for (Field f : o.getClass().getDeclaredFields()) {
                //System.out.println(f.getName());
                if (f.isAnnotationPresent(EncodingAnnotation.class)) {
                    //  System.out.println( ""+f.getName()+" has annotation");


                    if (f.getType() == OptionalNull.class) {
                        // getFieldName;
                        String getterName = "get" + f.getName().substring(0, 1).toUpperCase() + f.getName().substring(1);
                       // System.out.println("type is OptionalNull Yay: method " + getterName);
                        Method getter = o.getClass().getMethod(getterName, null);

                        OptionalNull obj = (OptionalNull) getter.invoke(o);
                        if (obj != null) {
                            obj.setEncoding(f.getAnnotation(EncodingAnnotation.class).value());
                            obj.setPattern(f.getAnnotation(EncodingAnnotation.class).pattern());
                        }
                    } else {
                     //   System.out.println("type is not optional null boooo");
                    }

                }
            }
            return o;
            //serializer.serialize(o, jsonGenerator, serializerProvider);
        } catch(NoSuchMethodException|IllegalAccessException|InvocationTargetException e){
            throw new RuntimeException(e);
        }
    }
}
