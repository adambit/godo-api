package is.godo.api.v0.json;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.ser.BeanSerializerModifier;

/**
 * EncodinAnnotationApplier reads EncodingAnnotation and applies it to the OptionalNull instance
 * for access at runtime during serialization.
 *
 */
@JsonAutoDetect
public class EncodingAnnotationApplier extends BeanSerializerModifier {
    @Override
    public JsonSerializer<?> modifySerializer(
            final SerializationConfig serializationConfig,
            final BeanDescription beanDescription,
            final JsonSerializer<?> jsonSerializer) {
        return new EncodingAnnotationSerializer((JsonSerializer<Object>) jsonSerializer);
    }


}

