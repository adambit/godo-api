package is.godo.api.v0.json;

public enum Encoding {
    NUMBER,
    NULL,
    STRING,
    BOOLEAN,
    STRING_INT,
    OBJECT,
    URL,
}
