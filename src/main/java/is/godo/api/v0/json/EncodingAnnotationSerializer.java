package is.godo.api.v0.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.ResolvableSerializer;

import java.io.IOException;

public class EncodingAnnotationSerializer extends JsonSerializer<Object> implements  ResolvableSerializer {

        private final JsonSerializer<Object> serializer;


        public EncodingAnnotationSerializer(final JsonSerializer<Object> jsonSerializer) {
            this.serializer  = jsonSerializer;
        }

        @Override
        public void serialize(
                final Object o,
                final JsonGenerator jsonGenerator,
                final SerializerProvider serializerProvider)
                throws IOException {

               // System.out.println(o.toString());
            serializer.serialize(EncodingApplier.apply(o), jsonGenerator, serializerProvider);



        }

    @Override
    public void resolve(SerializerProvider serializerProvider) throws JsonMappingException {
        if(serializer instanceof ResolvableSerializer) {
            ((ResolvableSerializer)serializer).resolve(serializerProvider);
        }
    }
}
