package is.godo.api.v0.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class StringEnumSerializer extends JsonSerializer<StringEnum> {

    @Override
    public void serialize(StringEnum stringEnum, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        serializerProvider.defaultSerializeValue(stringEnum.getValue(), jsonGenerator);
    }

}




