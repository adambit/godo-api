package is.godo.api.v0.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;

public class NumberStringDeserializer extends JsonDeserializer<OptionalNull<Long>> {

    @Override
    public OptionalNull<Long> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        if(node.canConvertToLong()){
            OptionalNull<Long> val = new OptionalNull<>(node.longValue());
            val.setEncoding(Encoding.NUMBER);
            return val;
        }
        return null;
    }
}
