package is.godo.api.v0.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OptionalNullDeserializer extends JsonDeserializer<OptionalNull<?>> implements ContextualDeserializer {

    private JavaType valueType;
    private Encoding encoding;
    private String pattern;



    @Override
    public OptionalNull<?> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        if(valueType.getRawClass().equals(Boolean.class)){
            String text = jsonParser.getText();
    //        System.out.println(text);
  //          String value = jsonParser.getValueAsString();
//            System.out.println(value);
            switch(text){
                case "1":
                    return new OptionalNull<>(true);
                case "true":
                    return new OptionalNull<>(true);
                case "false":
                    return new OptionalNull<>(false);
                case "0":
                    return new OptionalNull<>(false);
            }
        } else if (valueType.getRawClass().equals(String.class)){
            //String text jsonParser.
        }
        if(valueType.getRawClass().equals(Date.class) && pattern != null){
            String text = jsonParser.getText();
            if( text.isEmpty() )
                return new OptionalNull<>(null);
            if(!pattern.isEmpty()){
                SimpleDateFormat fmt = new SimpleDateFormat(pattern);
                try {
                    return new OptionalNull<>(fmt.parse(text));
                } catch(ParseException e){
                    throw new RuntimeException(e);
                }
            }
        }
        return new OptionalNull<>(deserializationContext.readValue(jsonParser, valueType));

    }

    @Override
    public JsonDeserializer<?> createContextual(DeserializationContext ctxt, BeanProperty property) throws JsonMappingException {
        JavaType optionalNullType = property.getType();
        JavaType valueType = optionalNullType.containedType(0);


        OptionalNullDeserializer deserializer = new OptionalNullDeserializer();
        deserializer.valueType = valueType;
        if(property.getAnnotation(EncodingAnnotation.class)  != null) {
            deserializer.encoding = property.getAnnotation(EncodingAnnotation.class).value();
            deserializer.pattern = property.getAnnotation(EncodingAnnotation.class).pattern();
        }
        return deserializer;
    }

}
