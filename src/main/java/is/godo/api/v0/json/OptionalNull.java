package is.godo.api.v0.json;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * OptionalNull allows null values to be serialized on demand.
 */
@JsonSerialize(using=OptionalNullSerializer.class)
@JsonDeserialize(using=OptionalNullDeserializer.class)
public class OptionalNull<T> {
    private T value;
    private Boolean isPresent;
    private Encoding encoding = Encoding.OBJECT;
    private String pattern = "yyyyMMdd";

    public OptionalNull(T value) {
        this.value = value;
        this.isPresent = true;
    }


    public T get(){
        return value;
    }
    public Boolean isPresent(){
        return isPresent;
    };

    public Encoding getEncoding() {
        return encoding;
    }

    public OptionalNull<T> setEncoding(Encoding encoding) {
        this.encoding = encoding;
        return this;
    }

    public Boolean equals(OptionalNull<T> other){
        if(other == null){
            System.out.println("other null");
            return false;
        }
        Boolean present = this.isPresent().equals(other.isPresent());
        if(!present){
            System.out.println("not present in both");
        }
        Boolean gets = this.get().equals(other.get());
        if(!gets){
            System.out.println("not gets");
        }
        return present && gets;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public void setAsNull() {
        value = null;
        isPresent = false;
    }

    public String toString() {
        if( value != null )
            return value.toString();
        else
            return "N/A";
    }
}
