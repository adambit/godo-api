package is.godo.api.v0.json;


import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class NumericEnumSerializer extends JsonSerializer<NumericEnum> {

    @Override
    public void serialize(NumericEnum numericEnum, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        serializerProvider.defaultSerializeValue(numericEnum.getId(), jsonGenerator);
    }


}
