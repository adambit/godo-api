package is.godo.api.v0.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(using = StringEnumSerializer.class)
public abstract class StringEnum {
    private final String value;

    @JsonCreator
    public StringEnum(String value) {
        this.value = value.trim();

    }

    @JsonGetter
    public String getValue() {
        return value.trim();
    }

    public Boolean equals(StringEnum other) {
        return this.value.trim().equals(other.value.trim());
    }

    @JsonValue
    public String toString() {
        return value.trim();
    }
}


