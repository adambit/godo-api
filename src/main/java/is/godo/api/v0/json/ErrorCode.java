package is.godo.api.v0.json;

public class ErrorCode extends  StringEnum  {

    public static final ErrorCode AUTH_ERROR = new ErrorCode("1000");
    public static final ErrorCode AUTH_ERROR2 = new ErrorCode("1002");
    public static final ErrorCode MALFORMED_JSON = new ErrorCode("103");

    public ErrorCode(String value) {
        super(value.trim());
    }
    public ErrorCode(Long value){
        super(value.toString());
    }

    public Boolean equals(ErrorCode other) {
        return this.getValue().equals(other.getValue());
    }

}
