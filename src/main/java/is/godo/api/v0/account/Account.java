package is.godo.api.v0.account;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Account {
    private Long id;
    private String balance;

    private String template1;
    private String template2;
    private String template3;
    private String template4;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getCharge() {
        return charge;
    }

    public void setCharge(String charge) {
        this.charge = charge;
    }

    public Usage getUsage() {
        return usage;
    }

    public void setUsage(Usage usage) {
        this.usage = usage;
    }

    public List<Subaccount> getSubaccounts() {
        return subaccounts;
    }

    public void setSubaccounts(List<Subaccount> subaccounts) {
        this.subaccounts = subaccounts;
    }

    private String charge;
    private Usage usage;

    @JsonDeserialize(using=SubaccountDeserializer.class)
    private List<Subaccount> subaccounts;

    public String getTemplate1() {
        return template1;
    }

    public void setTemplate1(String template1) {
        this.template1 = template1;
    }

    public String getTemplate2() {
        return template2;
    }

    public void setTemplate2(String template2) {
        this.template2 = template2;
    }

    public String getTemplate3() {
        return template3;
    }

    public void setTemplate3(String template3) {
        this.template3 = template3;
    }

    public String getTemplate4() {
        return template4;
    }

    public void setTemplate4(String template4) {
        this.template4 = template4;
    }

    public String toString() {
        String result = "id: " + id + "\n";
        if( subaccounts != null ) {
            for (Subaccount sa : subaccounts) {
                result += sa;
            }
        }
        return result;
    }
}
