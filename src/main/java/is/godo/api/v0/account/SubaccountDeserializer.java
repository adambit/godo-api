package is.godo.api.v0.account;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import is.godo.api.v0.inventory.Availability;

import java.io.IOException;
import java.util.*;

public class SubaccountDeserializer extends JsonDeserializer<List<Subaccount>> {

    public List<Subaccount> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        List<Subaccount> obj = new ArrayList<>();

        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        Iterator<Map.Entry<String, JsonNode>> nodes = node.fields();
        while(nodes.hasNext()) {
            Map.Entry<String,JsonNode> pair = nodes.next();

            JsonNode subaccountNode = pair.getValue();
            if (!subaccountNode.isObject()){
                throw new RuntimeException("invalid json");
            }

            Subaccount sa = jsonParser.getCodec().treeToValue(subaccountNode, Subaccount.class);

            obj.add(sa);

        }
        return obj;
    }
}
