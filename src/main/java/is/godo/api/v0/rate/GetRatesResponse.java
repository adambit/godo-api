package is.godo.api.v0.rate;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonValue;
import is.godo.api.v0.json.BaseResponse;

import java.util.List;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@JsonInclude(NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetRatesResponse extends BaseResponse {
    private List<Rate> getRates;


    public GetRatesResponse() {
    }

    @JsonCreator
    public GetRatesResponse(List<Rate> getRates) {
        this.getRates = getRates;
    }

    @JsonValue
    public List<Rate> getGetRates() {
        return getRates;
    }

    public void setGetRates(List<Rate> getRates) {
        this.getRates = getRates;
    }
}
