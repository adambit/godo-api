package is.godo.api.v0.rate;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonValue;
import is.godo.api.v0.json.BaseResponse;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SetRatesResponse extends BaseResponse {


    private List<RateResponse> resp;

    public SetRatesResponse() {
    }

    @JsonCreator
    public SetRatesResponse(List<RateResponse> resp) {
        this.resp = resp;
    }

    @JsonValue
    public List<RateResponse> getResp() {
        return resp;
    }

    public void setResp(List<RateResponse> resp) {
        this.resp = resp;
    }
}
