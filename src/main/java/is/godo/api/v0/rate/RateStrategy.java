package is.godo.api.v0.rate;

import is.godo.api.v0.json.NumericEnum;

public class RateStrategy extends NumericEnum {
    public static final RateStrategy ALLOW_LOWER_PRICES = new RateStrategy(0L);
    public static final RateStrategy DISALLOW_LOWER_PRICES_OR_SHORTER_STAYS = new RateStrategy(1L);
    public static final RateStrategy DISALLOW_OTHER_RATES = new RateStrategy(2L);

    public RateStrategy(Long id) {
        super(id);
    }

    public RateStrategy(String id) {
        super(id);
    }

    public Boolean equals(RateStrategy other) {
        return super.equals(other);
    }
}


