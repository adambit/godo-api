package is.godo.api.v0.rate;

import com.fasterxml.jackson.annotation.*;
import is.godo.api.v0.Action;
import is.godo.api.v0.booking.Channel;
import is.godo.api.v0.json.Encoding;
import is.godo.api.v0.json.EncodingAnnotation;
import is.godo.api.v0.json.OptionalNull;

import java.math.BigDecimal;
import java.util.*;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
//@JsonSerialize(using=RateSerializer.class)
//@JsonDeserialize(using=RateDeserializer.class)
public class Rate {

    private Action action;
    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> rateId;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> roomId;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> offerId;

    @EncodingAnnotation(pattern="yyyy-MM-dd")
    private OptionalNull<Date> firstNight;

    @EncodingAnnotation(pattern="yyyy-MM-dd")
    private OptionalNull<Date> lastNight;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<String> name;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> minNights;
    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> maxNights;
    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> minAdvance;
    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> maxAdvance;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<RateStrategy> strategy;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<BigDecimal> roomPrice;

    @EncodingAnnotation(Encoding.STRING_INT)
    private OptionalNull<Boolean> roomPriceEnable;
    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> roomPriceGuests;

    @JsonProperty("1pPrice")
    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<BigDecimal> price1p;

    @JsonProperty("1pPriceEnable")
    @EncodingAnnotation(Encoding.STRING_INT)
    private OptionalNull<Boolean> price1pEnable;

    @JsonProperty("2pPrice")
    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<BigDecimal> price2p;

    @JsonProperty("2pPriceEnable")
    @EncodingAnnotation(Encoding.STRING_INT)
    private OptionalNull<Boolean> price2pEnable;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<BigDecimal> extraPersonPrice;
    @EncodingAnnotation(Encoding.STRING_INT)
    private OptionalNull<Boolean> extraPersonPriceEnable;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<BigDecimal> extraChildPrice;
    @EncodingAnnotation(Encoding.STRING_INT)
    private OptionalNull<Boolean> extraChildPriceEnable;

    @EncodingAnnotation(Encoding.STRING_INT)
    private OptionalNull<Boolean> canInMon;
    @EncodingAnnotation(Encoding.STRING_INT)
    private OptionalNull<Boolean> canInTue;
    @EncodingAnnotation(Encoding.STRING_INT)
    private OptionalNull<Boolean> canInWed;
    @EncodingAnnotation(Encoding.STRING_INT)
    private OptionalNull<Boolean> canInThu;
    @EncodingAnnotation(Encoding.STRING_INT)
    private OptionalNull<Boolean> canInFri;
    @EncodingAnnotation(Encoding.STRING_INT)
    private OptionalNull<Boolean> canInSat;
    @EncodingAnnotation(Encoding.STRING_INT)
    private OptionalNull<Boolean> canInSun;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<String> agodacomRateCode;
    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<String> bookingcomRateCode;
    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<String> budgetplacescomRateCode;
    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<String> expediacomRateCode;
    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<String> feratelRateCode;
    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<String> hotelbedscomRateCode;
    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<String> lastminutecomRateCode;
    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<String> lateroomscomRateCode;
    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<String> otaRateCode;
    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<String> tablethotelscomRateCode;
    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<String> travelocitycomRateCode;

    private Map<String,Object> other = new LinkedHashMap<>(); // use linked hashmap to preserve insertion order.


    public OptionalNull<Long> getRateId() {
        return rateId;
    }

    public void setRateId(OptionalNull<Long> rateId) {
        this.rateId = rateId;
    }

    public OptionalNull<Long> getRoomId() {
        return roomId;
    }

    public void setRoomId(OptionalNull<Long> roomId) {
        this.roomId = roomId;
    }

    public OptionalNull<Long> getOfferId() {
        return offerId;
    }

    public void setOfferId(OptionalNull<Long> offerId) {
        this.offerId = offerId;
    }

    public OptionalNull<Date> getFirstNight() {
        return firstNight;
    }

    public void setFirstNight(OptionalNull<Date> firstNight) {
        this.firstNight = firstNight;
    }

    public OptionalNull<Date> getLastNight() {
        return lastNight;
    }

    public void setLastNight(OptionalNull<Date> lastNight) {
        this.lastNight = lastNight;
    }

    public OptionalNull<String> getName() {
        return name;
    }

    public void setName(OptionalNull<String> name) {
        this.name = name;
    }

    public void setStrategy(OptionalNull<RateStrategy> strategy) {
        this.strategy = strategy;
    }

    public OptionalNull<Long> getMinNights() {
        return minNights;
    }

    public void setMinNights(OptionalNull<Long> minNights) {
        this.minNights = minNights;
    }

    public OptionalNull<Long> getMaxNights() {
        return maxNights;
    }

    public void setMaxNights(OptionalNull<Long> maxNights) {
        this.maxNights = maxNights;
    }

    public OptionalNull<Long> getMinAdvance() {
        return minAdvance;
    }

    public void setMinAdvance(OptionalNull<Long> minAdvance) {
        this.minAdvance = minAdvance;
    }

    public OptionalNull<Long> getMaxAdvance() {
        return maxAdvance;
    }

    public void setMaxAdvance(OptionalNull<Long> maxAdvance) {
        this.maxAdvance = maxAdvance;
    }


    public OptionalNull<BigDecimal> getRoomPrice() {
        return roomPrice;
    }

    public void setRoomPrice(OptionalNull<BigDecimal> roomPrice) {
        this.roomPrice = roomPrice;
    }

    public OptionalNull<Boolean> getRoomPriceEnable() {
        return roomPriceEnable;
    }

    public void setRoomPriceEnable(OptionalNull<Boolean> roomPriceEnable) {
        this.roomPriceEnable = roomPriceEnable;
    }

    public OptionalNull<Boolean> getPrice2pEnable() {
        return price2pEnable;
    }

    public void setPrice2pEnable(OptionalNull<Boolean> price2pEnable) {
        this.price2pEnable = price2pEnable;
    }

    public OptionalNull<Long> getRoomPriceGuests() {
        return roomPriceGuests;
    }

    public void setRoomPriceGuests(OptionalNull<Long> roomPriceGuests) {
        this.roomPriceGuests = roomPriceGuests;
    }

    public OptionalNull<BigDecimal> getPrice1p() {
        return price1p;
    }

    public void setPrice1p(OptionalNull<BigDecimal> price1p) {
        this.price1p = price1p;
    }

    public OptionalNull<Boolean> getPrice1pEnable() {
        return price1pEnable;
    }

    public void setPrice1pEnable(OptionalNull<Boolean> price1pEnable) {
        this.price1pEnable = price1pEnable;
    }

    public OptionalNull<BigDecimal> getPrice2p() {
        return price2p;
    }

    public void setPrice2p(OptionalNull<BigDecimal> price2p) {
        this.price2p = price2p;
    }



    public OptionalNull<BigDecimal> getExtraPersonPrice() {
        return extraPersonPrice;
    }

    public void setExtraPersonPrice(OptionalNull<BigDecimal> extraPersonPrice) {
        this.extraPersonPrice = extraPersonPrice;
    }

    public OptionalNull<Boolean> getExtraPersonPriceEnable() {
        return extraPersonPriceEnable;
    }

    public void setExtraPersonPriceEnable(OptionalNull<Boolean> extraPersonPriceEnable) {
        this.extraPersonPriceEnable = extraPersonPriceEnable;
    }

    public OptionalNull<BigDecimal> getExtraChildPrice() {
        return extraChildPrice;
    }

    public void setExtraChildPrice(OptionalNull<BigDecimal> extraChildPrice) {
        this.extraChildPrice = extraChildPrice;
    }

    public OptionalNull<Boolean> getExtraChildPriceEnable() {
        return extraChildPriceEnable;
    }

    public void setExtraChildPriceEnable(OptionalNull<Boolean> extraChildPriceEnable) {
        this.extraChildPriceEnable = extraChildPriceEnable;
    }

    public OptionalNull<Boolean> getCanInMon() {
        return canInMon;
    }

    public void setCanInMon(OptionalNull<Boolean> canInMon) {
        this.canInMon = canInMon;
    }

    public OptionalNull<Boolean> getCanInTue() {
        return canInTue;
    }

    public void setCanInTue(OptionalNull<Boolean> canInTue) {
        this.canInTue = canInTue;
    }

    public OptionalNull<Boolean> getCanInWed() {
        return canInWed;
    }

    public void setCanInWed(OptionalNull<Boolean> canInWed) {
        this.canInWed = canInWed;
    }

    public OptionalNull<Boolean> getCanInThu() {
        return canInThu;
    }

    public void setCanInThu(OptionalNull<Boolean> canInThu) {
        this.canInThu = canInThu;
    }

    public OptionalNull<Boolean> getCanInFri() {
        return canInFri;
    }

    public void setCanInFri(OptionalNull<Boolean> canInFri) {
        this.canInFri = canInFri;
    }

    public OptionalNull<Boolean> getCanInSat() {
        return canInSat;
    }

    public void setCanInSat(OptionalNull<Boolean> canInSat) {
        this.canInSat = canInSat;
    }

    public OptionalNull<Boolean> getCanInSun() {
        return canInSun;
    }

    public void setCanInSun(OptionalNull<Boolean> canInSun) {
        this.canInSun = canInSun;
    }

    public OptionalNull<String> getAgodacomRateCode() {
        return agodacomRateCode;
    }

    public void setAgodacomRateCode(OptionalNull<String> agodacomRateCode) {
        this.agodacomRateCode = agodacomRateCode;
    }

    public OptionalNull<String> getBookingcomRateCode() {
        return bookingcomRateCode;
    }

    public void setBookingcomRateCode(OptionalNull<String> bookingcomRateCode) {
        this.bookingcomRateCode = bookingcomRateCode;
    }

    public OptionalNull<String> getBudgetplacescomRateCode() {
        return budgetplacescomRateCode;
    }

    public void setBudgetplacescomRateCode(OptionalNull<String> budgetplacescomRateCode) {
        this.budgetplacescomRateCode = budgetplacescomRateCode;
    }

    public OptionalNull<String> getExpediacomRateCode() {
        return expediacomRateCode;
    }

    public void setExpediacomRateCode(OptionalNull<String> expediacomRateCode) {
        this.expediacomRateCode = expediacomRateCode;
    }

    public OptionalNull<String> getFeratelRateCode() {
        return feratelRateCode;
    }

    public void setFeratelRateCode(OptionalNull<String> feratelRateCode) {
        this.feratelRateCode = feratelRateCode;
    }

    public OptionalNull<String> getHotelbedscomRateCode() {
        return hotelbedscomRateCode;
    }

    public void setHotelbedscomRateCode(OptionalNull<String> hotelbedscomRateCode) {
        this.hotelbedscomRateCode = hotelbedscomRateCode;
    }

    public OptionalNull<String> getLastminutecomRateCode() {
        return lastminutecomRateCode;
    }

    public void setLastminutecomRateCode(OptionalNull<String> lastminutecomRateCode) {
        this.lastminutecomRateCode = lastminutecomRateCode;
    }

    public OptionalNull<String> getLateroomscomRateCode() {
        return lateroomscomRateCode;
    }

    public void setLateroomscomRateCode(OptionalNull<String> lateroomscomRateCode) {
        this.lateroomscomRateCode = lateroomscomRateCode;
    }

    public OptionalNull<String> getOtaRateCode() {
        return otaRateCode;
    }

    public void setOtaRateCode(OptionalNull<String> otaRateCode) {
        this.otaRateCode = otaRateCode;
    }

    public OptionalNull<String> getTablethotelscomRateCode() {
        return tablethotelscomRateCode;
    }

    public void setTablethotelscomRateCode(OptionalNull<String> tablethotelscomRateCode) {
        this.tablethotelscomRateCode = tablethotelscomRateCode;
    }

    public OptionalNull<String> getTravelocitycomRateCode() {
        return travelocitycomRateCode;
    }

    public void setTravelocitycomRateCode(OptionalNull<String> travelocitycomRateCode) {
        this.travelocitycomRateCode = travelocitycomRateCode;
    }

    public OptionalNull<RateStrategy> getStrategy() {
        return strategy;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    @JsonAnyGetter
    public Map<String, Object> any() {
        return this.other;
    }


    @JsonAnySetter
    public void set(String name, Object value){
        other.put(name, value);
    }


    public List<ChannelEnabled> getChannelEnabledList() {
        /*
        * The list of enabled channels follow the channelXXX format where XXX is numeric.
        * We put these JSON fields into the hashmap this.other and
        * */
        List<ChannelEnabled> channels = new ArrayList<>();
        for(Map.Entry<String,Object> pair: this.other.entrySet()){
            if(pair.getKey().startsWith("channel")){
                // FIXME: could use regex.
                String[] no = pair.getKey().split("channel");
                if(no.length != 2){
                    // ignore fields that don't match correctly.
                    continue;
                }
                Long channelId;
                try {
                   channelId = Long.valueOf(no[1]);
                } catch ( NumberFormatException e){
                    // ignore fields that don't have a number after the "channel" part.
                    continue;
                }

                Boolean enabled = false;
                if(pair.getValue().equals(1) || pair.getValue().equals("1") || pair.getValue().equals(true) || pair.getValue().equals("true") ){
                    enabled = true;
                }
                ChannelEnabled c = new ChannelEnabled(new Channel(channelId), enabled);
                channels.add(c);

            }
        }
        return channels;
    }

    @JsonIgnore
    public void setChannelEnabledList(List<ChannelEnabled> channelEnabledList) {
        for(ChannelEnabled c: channelEnabledList){
            this.other.put("channel"+String.format("%03d",c.getChannel().getId()), c.getEnabled() ? "1" : "0");
        }
    }
}
