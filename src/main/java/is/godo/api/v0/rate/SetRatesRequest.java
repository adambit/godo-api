package is.godo.api.v0.rate;



import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import is.godo.api.v0.Authentication;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SetRatesRequest {

    private Authentication authentication;

    private List<Rate> setRates;

    public Authentication getAuthentication() {
        return authentication;
    }

    public void setAuthentication(Authentication authentication) {
        this.authentication = authentication;
    }

    public List<Rate> getSetRates() {
        return setRates;
    }

    public void setSetRates(List<Rate> setRates) {
        this.setRates = setRates;
    }
}
