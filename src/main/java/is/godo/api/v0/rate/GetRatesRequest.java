package is.godo.api.v0.rate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import is.godo.api.v0.Authentication;
import is.godo.api.v0.json.OptionalNull;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

// https://api.godo.com/json/getRates
@JsonInclude(NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetRatesRequest {
    private Authentication authentication;
    private OptionalNull<Long> roomId;

    public Authentication getAuthentication() {
        return authentication;
    }

    public void setAuthentication(Authentication authentication) {
        this.authentication = authentication;
    }

    public OptionalNull<Long> getRoomId() {
        return roomId;
    }

    public void setRoomId(OptionalNull<Long> roomId) {
        this.roomId = roomId;
    }
}
