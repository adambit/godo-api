package is.godo.api.v0.rate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RateResponse{
    String error;
    String success;
    Long errorCode;
    Long rateId;

    @JsonIgnore
    public Boolean isSuccess(){
        return success != null;
    }

    @JsonIgnore
    public String getMeessage(){
        if (error != null){
            return error;
        }
        return success;
    }
}
