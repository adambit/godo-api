package is.godo.api.v0.rate;

import is.godo.api.v0.booking.Channel;

public class ChannelEnabled {
    private Channel channel;
    private Boolean enabled;

    public ChannelEnabled() {
    }

    public ChannelEnabled(Channel channel, Boolean enabled) {
        this.channel = channel;
        this.enabled = enabled;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
