package is.godo.api.v0.booking;

import is.godo.api.v0.json.NumericEnum;

public class LineType2 extends NumericEnum {

    public LineType2(Long id) {
        super(id);
    }

    public LineType2(String id) {
        super(id);
    }


    public Boolean equals(LineType2 other) {
        return super.equals(other);
    }
}
