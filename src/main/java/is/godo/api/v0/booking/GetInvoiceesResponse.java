package is.godo.api.v0.booking;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetInvoiceesResponse {
    private List<Invoicee> invoicees;

    public GetInvoiceesResponse() {
    }

    @JsonCreator
    public GetInvoiceesResponse(List<Invoicee> invoicees) {
        this.invoicees = invoicees;
    }

    public List<Invoicee> getInvoicees() {
        return invoicees;
    }

    public void setInvoicees(List<Invoicee> invoicees) {
        this.invoicees = invoicees;
    }
}
