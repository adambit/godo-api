package is.godo.api.v0.booking;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import is.godo.api.v0.json.OptionalNull;

import java.math.BigDecimal;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class InvoiceLine implements Comparable<InvoiceLine> {

    private OptionalNull<Long> invoiceId;
    private OptionalNull<String> description;
    private OptionalNull<BigDecimal> price;
    private OptionalNull<Long> qty;
    private OptionalNull<String> status;
    private OptionalNull<LineType> type;
    private OptionalNull<LineType2> type2;
    private OptionalNull<BigDecimal> vatRate;

    private OptionalNull<Date> time;

    private OptionalNull<Long> invoiceeId;

    public OptionalNull<Long> getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(OptionalNull<Long> invoiceId) {
        this.invoiceId = invoiceId;
    }

    public OptionalNull<String> getDescription() {
        return description;
    }

    public void setDescription(OptionalNull<String> description) {
        this.description = description;
    }

    public OptionalNull<BigDecimal> getPrice() {
        return price;
    }

    public void setPrice(OptionalNull<BigDecimal> price) {
        this.price = price;
    }

    public OptionalNull<Long> getQty() {
        return qty;
    }

    public void setQty(OptionalNull<Long> qty) {
        this.qty = qty;
    }

    public OptionalNull<String> getStatus() {
        return status;
    }

    public void setStatus(OptionalNull<String> status) {
        this.status = status;
    }

    public OptionalNull<LineType> getType() {
        return type;
    }

    public void setType(OptionalNull<LineType> type) {
        this.type = type;
    }

    public OptionalNull<LineType2> getType2() {
        return type2;
    }

    public void setType2(OptionalNull<LineType2> type2) {
        this.type2 = type2;
    }

    public OptionalNull<BigDecimal> getVatRate() {
        return vatRate;
    }

    public void setVatRate(OptionalNull<BigDecimal> vatRate) {
        this.vatRate = vatRate;
    }

    public void setTime(OptionalNull<Date> time) {
        this.time = time;
    }

    public OptionalNull<Date> getTime() {
        return time;
    }


    /*public void delete() {
        this.description = new OptionalNull<>("");

    }*/

    public String toString() {
        return description + " - " + type + " - " + type2 + " - " + invoiceeId;
    }

    public OptionalNull<Long> getInvoiceeId() {
        return invoiceeId;
    }

    public void setInvoiceeId(OptionalNull<Long> invoiceeId) {
        this.invoiceeId = invoiceeId;
    }

    public int compareTo( InvoiceLine il ) {
        if( invoiceeId.get() == null || il.invoiceeId.get() == null )
            return -1;
        return il.invoiceeId.get().compareTo( il.invoiceeId.get() );
    }
}
