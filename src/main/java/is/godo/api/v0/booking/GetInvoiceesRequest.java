package is.godo.api.v0.booking;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import is.godo.api.v0.Authentication;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetInvoiceesRequest {
    private Authentication auth;

    public Authentication getAuthentication() {
        return auth;
    }

    public void setAuthentication(Authentication auth) {
        this.auth = auth;
    }
}
