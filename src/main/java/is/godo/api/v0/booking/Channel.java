package is.godo.api.v0.booking;

import is.godo.api.v0.json.NumericEnum;

/**
 * Channel represents a booking channel, the source of a booking, in godo
 *.
 */
public class Channel extends NumericEnum {
    public static final Channel GODO_PROPERTY_AGENT = new Channel(999L);
    public static final Channel GODO_PROPERTY = new Channel(0L);
    public static final Channel BOOKIT = new Channel(2L);
    public static final Channel NZAA = new Channel(3L);
    public static final Channel LATEROOMS_COM = new Channel(8L);
    public static final Channel ARIBNB_COM = new Channel(10L);
    public static final Channel FLIPKEY_COM = new Channel(12L);
    public static final Channel GUESTLINK_CO_UK = new Channel(13L);
    public static final Channel EXPEDIA_COM = new Channel(14L);
    public static final Channel WIMDU_COM = new Channel(15L);
    public static final Channel ICAL_BOOKINGS = new Channel(16L);
    public static final Channel AGODA_API = new Channel(17L);
    public static final Channel TRAVELO_CITY_COM = new Channel(18L);
    public static final Channel BOOKING_API = new Channel(19L);
    public static final Channel TRIPADVISOR_COM = new Channel(20L);
    public static final Channel ICAL_IMPORT1 = new Channel(21L);
    public static final Channel BUDGET_PLACES_COM = new Channel(22L);
    public static final Channel TABLET_HOTELS_COM = new Channel(23L);
    public static final Channel HOSTEL_WORLD_COM = new Channel(24L);
    public static final Channel VISIT_SCOTLAND_COM = new Channel(25L);
    public static final Channel HOLIDAY_LETTINGS_COM = new Channel(26L);
    public static final Channel BED_AND_BREAKFAST_EU = new Channel(27L);
    public static final Channel ICAL_IMPORT2 = new Channel(28L);
    public static final Channel ICAL_IMPORT3 = new Channel(29L);
    public static final Channel HOMEAWAY_COM = new Channel(30L);
    public static final Channel BED_AND_BREAKFAST_NL = new Channel(31L);
    public static final Channel ATRAVEO_DE = new Channel(32L);
    public static final Channel FERATEL = new Channel(33L);
    public static final Channel WEBROOMS_CO_NZ = new Channel(34L);
    public static final Channel LASTMINUTE_COM = new Channel(35L);
    public static final Channel HOTELBEDS_COM = new Channel(36L);
    public static final Channel HOUSETRIP_COM = new Channel(37L);
    public static final Channel NINEFLATS_COM = new Channel(38L);
    public static final Channel HOTELSCOMBINED = new Channel(39L);
    public static final Channel HOMEAWAY_ICAL = new Channel(40L);
    public static final Channel STAY_IN_TOWN = new Channel(41L);
    public static final Channel OTA = new Channel(42L);
    public static final Channel TRIVAGO_COM = new Channel(43L);
    public static final Channel HOSTEL_INTERNATIONAL = new Channel(44L);
    public static final Channel KASHFLOW_COM = new Channel(45L);
    public static final Channel AIRBNB_XML = new Channel(46L);

    public Channel(Long id) {
        super(id);
    }

    public Channel(String id) {
        super(id);
    }


    public Boolean equals(Channel other) {
        return super.equals(other);
    }
}
