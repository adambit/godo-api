package is.godo.api.v0.booking;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonValue;
import is.godo.api.v0.json.BaseResponse;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetBookingsResponse extends BaseResponse {

    private List<Booking> bookings;

    public GetBookingsResponse() {
    }

    @JsonCreator
    public GetBookingsResponse(List<Booking> bookings) {
        this.bookings = bookings;
    }


    @JsonValue
    public List<Booking> getBookings() {
        return bookings;
    }

    public void setBookings(List<Booking> bookings) {
        this.bookings = bookings;
    }
}
