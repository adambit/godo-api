package is.godo.api.v0.booking;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import is.godo.api.v0.Authentication;
import is.godo.api.v0.json.Encoding;
import is.godo.api.v0.json.EncodingAnnotation;
import is.godo.api.v0.json.OptionalNull;

import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SetBookingRequest {

    private Authentication authentication;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> roomId;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> bookId;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> masterId;

    @EncodingAnnotation(pattern = "yyyyMMdd")
    private OptionalNull<Date> arrivalFrom;

    @EncodingAnnotation(pattern = "yyyyMMdd")
    private OptionalNull<Date> arrivalTo;

    @EncodingAnnotation(pattern = "yyyyMMdd")
    private OptionalNull<Date> departureFrom;

    @EncodingAnnotation(pattern = "yyyyMMdd")
    private OptionalNull<Date> departureTo;

    @EncodingAnnotation(pattern = "yyyyMMdd")
    private OptionalNull<Date> firstNight;

    @EncodingAnnotation(pattern = "yyyyMMdd")
    private OptionalNull<Date> lastNight;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> roomQty;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> numAdult;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<String> guestName;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> price;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<BookingStatus> status;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> numChild;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<String> guestTitle;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<String> guestFirstName;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<String> guestEmail;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<String> guestPhone;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<String> guestMobile;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<String> guestAddress;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<String> guestCity;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<String> guestPostcode;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<String> guestCountry;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<String> guestComments;

    public OptionalNull<String> getNotifyHost() {
        return notifyHost;
    }

    public void setNotifyHost(OptionalNull<String> notifyHost) {
        this.notifyHost = notifyHost;
    }

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<String> notes;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<String> notifyHost;

    private OptionalNull<List<InvoiceLine>> invoice;

    public OptionalNull<List<InvoiceLine>> getInvoice() {
        return invoice;
    }

    public void setInvoice(OptionalNull<List<InvoiceLine>> invoice) {
        this.invoice = invoice;
    }

    public OptionalNull<String> getCustom10() {
        return custom10;
    }

    public void setCustom10(OptionalNull<String> custom10) {
        this.custom10 = custom10;
    }

    private OptionalNull<String> custom10;

    public OptionalNull<String> getMessage() {
        return message;
    }

    public void setMessage(OptionalNull<String> message) {
        this.message = message;
    }

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<String> message;

    public Authentication getAuthentication() {
        return authentication;
    }

    public void setAuthentication(Authentication authentication) {
        this.authentication = authentication;
    }

    public OptionalNull<Long> getRoomId() {
        return roomId;
    }

    public void setRoomId(OptionalNull<Long> roomId) {
        this.roomId = roomId;
    }

    public OptionalNull<Long> getBookId() {
        return bookId;
    }

    public void setBookId(OptionalNull<Long> bookId) {
        this.bookId = bookId;
    }

    public OptionalNull<Long> getMasterId() {
        return masterId;
    }

    public void setMasterId(OptionalNull<Long> masterId) {
        this.masterId = masterId;
    }

    public OptionalNull<Date> getArrivalFrom() {
        return arrivalFrom;
    }

    public void setArrivalFrom(OptionalNull<Date> arrivalFrom) {
        this.arrivalFrom = arrivalFrom;
    }

    public OptionalNull<Date> getArrivalTo() {
        return arrivalTo;
    }

    public void setArrivalTo(OptionalNull<Date> arrivalTo) {
        this.arrivalTo = arrivalTo;
    }

    public OptionalNull<Date> getDepartureFrom() {
        return departureFrom;
    }

    public void setDepartureFrom(OptionalNull<Date> departureFrom) {
        this.departureFrom = departureFrom;
    }

    public OptionalNull<Date> getDepartureTo() {
        return departureTo;
    }

    public void setDepartureTo(OptionalNull<Date> departureTo) {
        this.departureTo = departureTo;
    }

    public OptionalNull<Date> getFirstNight() {
        return firstNight;
    }

    public void setFirstNight(OptionalNull<Date> firstNight) {
        this.firstNight = firstNight;
    }

    public OptionalNull<Date> getLastNight() {
        return lastNight;
    }

    public void setLastNight(OptionalNull<Date> lastNight) {
        this.lastNight = lastNight;
    }

    public OptionalNull<Long> getRoomQty() {
        return roomQty;
    }

    public void setRoomQty(OptionalNull<Long> roomQty) {
        this.roomQty = roomQty;
    }

    public OptionalNull<Long> getNumAdult() {
        return numAdult;
    }

    public void setNumAdult(OptionalNull<Long> numAdult) {
        this.numAdult = numAdult;
    }

    public OptionalNull<String> getGuestName() {
        return guestName;
    }

    public void setGuestName(OptionalNull<String> guestName) {
        this.guestName = guestName;
    }

    public OptionalNull<Long> getPrice() {
        return price;
    }

    public void setPrice(OptionalNull<Long> price) {
        this.price = price;
    }

    public OptionalNull<BookingStatus> getStatus() {
        return status;
    }

    public void setStatus(OptionalNull<BookingStatus> status) {
        this.status = status;
    }

    public OptionalNull<Long> getNumChild() {
        return numChild;
    }

    public void setNumChild(OptionalNull<Long> numChild) {
        this.numChild = numChild;
    }

    public OptionalNull<String> getGuestTitle() {
        return guestTitle;
    }

    public void setGuestTitle(OptionalNull<String> guestTitle) {
        this.guestTitle = guestTitle;
    }

    public OptionalNull<String> getGuestFirstName() {
        return guestFirstName;
    }

    public void setGuestFirstName(OptionalNull<String> guestFirstName) {
        this.guestFirstName = guestFirstName;
    }

    public OptionalNull<String> getGuestEmail() {
        return guestEmail;
    }

    public void setGuestEmail(OptionalNull<String> guestEmail) {
        this.guestEmail = guestEmail;
    }

    public OptionalNull<String> getGuestPhone() {
        return guestPhone;
    }

    public void setGuestPhone(OptionalNull<String> guestPhone) {
        this.guestPhone = guestPhone;
    }

    public OptionalNull<String> getGuestMobile() {
        return guestMobile;
    }

    public void setGuestMobile(OptionalNull<String> guestMobile) {
        this.guestMobile = guestMobile;
    }

    public OptionalNull<String> getGuestAddress() {
        return guestAddress;
    }

    public void setGuestAddress(OptionalNull<String> guestAddress) {
        this.guestAddress = guestAddress;
    }

    public OptionalNull<String> getGuestCity() {
        return guestCity;
    }

    public void setGuestCity(OptionalNull<String> guestCity) {
        this.guestCity = guestCity;
    }

    public OptionalNull<String> getGuestPostcode() {
        return guestPostcode;
    }

    public void setGuestPostcode(OptionalNull<String> guestPostcode) {
        this.guestPostcode = guestPostcode;
    }

    public OptionalNull<String> getGuestCountry() {
        return guestCountry;
    }

    public void setGuestCountry(OptionalNull<String> guestCountry) {
        this.guestCountry = guestCountry;
    }

    public OptionalNull<String> getGuestComments() {
        return guestComments;
    }

    public void setGuestComments(OptionalNull<String> guestComments) {
        this.guestComments = guestComments;
    }

    public OptionalNull<String> getNotes() {
        return notes;
    }

    public void setNotes(OptionalNull<String> notes) {
        this.notes = notes;
    }
}