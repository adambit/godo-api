package is.godo.api.v0.booking;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import is.godo.api.v0.json.OptionalNull;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Invoicee {
    public OptionalNull<Long> getId() {
        return id;
    }

    public void setId(OptionalNull<Long> id) {
        this.id = id;
    }

    public OptionalNull<EnableCode> getEnable() {
        return enable;
    }

    public void setEnable(OptionalNull<EnableCode> enable) {
        this.enable = enable;
    }

    public OptionalNull<String> getName() {
        return name;
    }

    public void setName(OptionalNull<String> name) {
        this.name = name;
    }

    public OptionalNull<String> getCode() {
        return code;
    }

    public void setCode(OptionalNull<String> code) {
        this.code = code;
    }

    public OptionalNull<Long> getInvoice() {
        return invoice;
    }

    public void setInvoice(OptionalNull<Long> invoice) {
        this.invoice = invoice;
    }

    public OptionalNull<String> getTemplate1() {
        return template1;
    }

    public void setTemplate1(OptionalNull<String> template1) {
        this.template1 = template1;
    }

    public OptionalNull<String> getTemplate2() {
        return template2;
    }

    public void setTemplate2(OptionalNull<String> template2) {
        this.template2 = template2;
    }

    public OptionalNull<String> getTemplate3() {
        return template3;
    }

    public void setTemplate3(OptionalNull<String> template3) {
        this.template3 = template3;
    }

    public OptionalNull<String> getTemplate4() {
        return template4;
    }

    public void setTemplate4(OptionalNull<String> template4) {
        this.template4 = template4;
    }

    public enum EnableCode {
        NO,
        YES,
        YES_WITH_BOOKING
    }

    private OptionalNull<Long> id;
    private OptionalNull<EnableCode> enable;
    private OptionalNull<String> name;
    private OptionalNull<String> code;
    private OptionalNull<Long> invoice;
    private OptionalNull<String> template1;
    private OptionalNull<String> template2;
    private OptionalNull<String> template3;
    private OptionalNull<String> template4;

    public String toString() {
        String result = "Id: " + id
                + "\n name: " + name
                + "\n enable: " + enable
                + "\n code: " + code;
        return result;
    }
}
