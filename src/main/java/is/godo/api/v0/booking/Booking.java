package is.godo.api.v0.booking;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import is.godo.api.v0.inventory.BookingDeserializer;
import is.godo.api.v0.inventory.GetAvailabilityResponseDeserializer;
import is.godo.api.v0.json.Encoding;
import is.godo.api.v0.json.EncodingAnnotation;
import is.godo.api.v0.json.OptionalNull;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Booking {

    @EncodingAnnotation(Encoding.STRING_INT)
    private OptionalNull<Long> bookId;
    @EncodingAnnotation(Encoding.STRING_INT)
    private OptionalNull<Long> roomId;
    @EncodingAnnotation(Encoding.STRING_INT)
    private OptionalNull<Long> unitId;
    @EncodingAnnotation(Encoding.STRING_INT)
    private OptionalNull<Long> roomQty;

    @EncodingAnnotation(Encoding.STRING_INT)
    private OptionalNull<BookingStatus> status;

    @EncodingAnnotation(pattern="yyyy-MM-dd")
    private OptionalNull<Date> firstNight;
    @EncodingAnnotation( pattern="yyyy-MM-dd")
    private OptionalNull<Date> lastNight;

    @EncodingAnnotation(Encoding.STRING_INT)
    private OptionalNull<Long> numAdult;
    @EncodingAnnotation(Encoding.STRING_INT)
    private OptionalNull<Long> numChild;

    // Guest
    private OptionalNull<String> guestTitle;
    private OptionalNull<String> guestName;
    private OptionalNull<String> guestFirstName;
    private OptionalNull<String> guestEmail;
    private OptionalNull<String> guestPhone;
    private OptionalNull<String> guestMobile;
    private OptionalNull<String> guestFax;
    private OptionalNull<String> guestAddress;
    private OptionalNull<String> guestCity;
    private OptionalNull<String> guestPostcode;
    private OptionalNull<String> guestCountry;
    private OptionalNull<String> guestCountry2;
    private OptionalNull<String> guestArrivalTime;
    private OptionalNull<String> guestVoucher;
    private OptionalNull<String> guestComments;

    private OptionalNull<String> notes;
    private OptionalNull<String> message;

    private OptionalNull<String> custom1;
    private OptionalNull<String> custom2;
    private OptionalNull<String> custom3;
    private OptionalNull<String> custom4;
    private OptionalNull<String> custom5;
    private OptionalNull<String> custom6;
    private OptionalNull<String> custom7;
    private OptionalNull<String> custom8;
    private OptionalNull<String> custom9;
    private OptionalNull<String> custom10;

    private OptionalNull<String> flagColor;
    private OptionalNull<String> flagText;

    @EncodingAnnotation(Encoding.STRING_INT)
    private OptionalNull<Long> statusCode;

    private OptionalNull<String> lang;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<BigDecimal> price;
    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<BigDecimal> deposit;
    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<BigDecimal> tax;
    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<BigDecimal> commission;

    private OptionalNull<String> currency;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> offerId;

    private OptionalNull<String> rateDescription;
    private OptionalNull<String> referer;         //> Not editable
    private OptionalNull<String> refererEditable; // Editable in> the GUI
    private OptionalNull<String> reference;
    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Channel> apiSource;

    private OptionalNull<String> apiReference;
    private OptionalNull<String> apiMessage;

    private OptionalNull<Long> masterId;

    @EncodingAnnotation(pattern="yyyy-MM-dd HH:mm:ss")
    private OptionalNull<Date> bookingTime;
    @EncodingAnnotation(pattern="yyyy-MM-dd HH:mm:ss")
    private OptionalNull<Date> modified;
    private OptionalNull<List<InvoiceLine>> invoice;
    private OptionalNull<List<InfoItem>> infoItems;
    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> invoiceNumber;

    /*
    @EncodingAnnotation(pattern="yyyy-MM-dd HH:mm:ss")
    private OptionalNull<Date> invoiceDate;
    */

    @EncodingAnnotation(Encoding.STRING_INT)
    private OptionalNull<Boolean> allowChannelUpdate;

    // when it is a master booking, this will contain a list of the slave bookings.
    // private OptionalNull<List<Long>> group;

    public OptionalNull<Long> getBookId() {
        return bookId;
    }

    public void setBookId(OptionalNull<Long> bookId) {
        this.bookId = bookId;
    }

    public OptionalNull<Long> getRoomId() {
        return roomId;
    }

    public void setRoomId(OptionalNull<Long> roomId) {
        this.roomId = roomId;
    }

    public OptionalNull<Long> getUnitId() {
        return unitId;
    }

    public void setUnitId(OptionalNull<Long> unitId) {
        this.unitId = unitId;
    }

    public OptionalNull<Long> getRoomQty() {
        return roomQty;
    }

    public void setRoomQty(OptionalNull<Long> roomQty) {
        this.roomQty = roomQty;
    }

    public OptionalNull<BookingStatus> getStatus() {
        return status;
    }

    public void setStatus(OptionalNull<BookingStatus> status) {
        this.status = status;
    }

    public OptionalNull<Date> getFirstNight() {
        return firstNight;
    }

    public void setFirstNight(OptionalNull<Date> firstNight) {
        this.firstNight = firstNight;
    }

    public OptionalNull<Date> getLastNight() {
        return lastNight;
    }

    public void setLastNight(OptionalNull<Date> lastNight) {
        this.lastNight = lastNight;
    }

    public OptionalNull<Long> getNumAdult() {
        return numAdult;
    }

    public void setNumAdult(OptionalNull<Long> numAdult) {
        this.numAdult = numAdult;
    }

    public OptionalNull<Long> getNumChild() {
        return numChild;
    }

    public void setNumChild(OptionalNull<Long> numChild) {
        this.numChild = numChild;
    }

    public OptionalNull<String> getGuestTitle() {
        return guestTitle;
    }

    public void setGuestTitle(OptionalNull<String> guestTitle) {
        this.guestTitle = guestTitle;
    }

    public OptionalNull<String> getGuestName() {
        return guestName;
    }

    public void setGuestName(OptionalNull<String> guestName) {
        this.guestName = guestName;
    }

    public OptionalNull<String> getGuestFirstName() {
        return guestFirstName;
    }

    public void setGuestFirstName(OptionalNull<String> guestFirstName) {
        this.guestFirstName = guestFirstName;
    }

    public OptionalNull<String> getGuestEmail() {
        return guestEmail;
    }

    public void setGuestEmail(OptionalNull<String> guestEmail) {
        this.guestEmail = guestEmail;
    }

    public OptionalNull<String> getGuestPhone() {
        return guestPhone;
    }

    public void setGuestPhone(OptionalNull<String> guestPhone) {
        this.guestPhone = guestPhone;
    }

    public OptionalNull<String> getGuestMobile() {
        return guestMobile;
    }

    public void setGuestMobile(OptionalNull<String> guestMobile) {
        this.guestMobile = guestMobile;
    }

    public OptionalNull<String> getGuestFax() {
        return guestFax;
    }

    public void setGuestFax(OptionalNull<String> guestFax) {
        this.guestFax = guestFax;
    }

    public OptionalNull<String> getGuestAddress() {
        return guestAddress;
    }

    public void setGuestAddress(OptionalNull<String> guestAddress) {
        this.guestAddress = guestAddress;
    }

    public OptionalNull<String> getGuestCity() {
        return guestCity;
    }

    public void setGuestCity(OptionalNull<String> guestCity) {
        this.guestCity = guestCity;
    }

    public OptionalNull<String> getGuestPostcode() {
        return guestPostcode;
    }

    public void setGuestPostcode(OptionalNull<String> guestPostcode) {
        this.guestPostcode = guestPostcode;
    }

    public OptionalNull<String> getGuestCountry() {
        return guestCountry;
    }

    public void setGuestCountry(OptionalNull<String> guestCountry) {
        this.guestCountry = guestCountry;
    }

    public OptionalNull<String> getGuestCountry2() {
        return guestCountry2;
    }

    public void setGuestCountry2(OptionalNull<String> guestCountry2) {
        this.guestCountry2 = guestCountry2;
    }

    public OptionalNull<String> getGuestArrivalTime() {
        return guestArrivalTime;
    }

    public void setGuestArrivalTime(OptionalNull<String> guestArrivalTime) {
        this.guestArrivalTime = guestArrivalTime;
    }

    public OptionalNull<String> getGuestVoucher() {
        return guestVoucher;
    }

    public void setGuestVoucher(OptionalNull<String> guestVoucher) {
        this.guestVoucher = guestVoucher;
    }

    public OptionalNull<String> getGuestComments() {
        return guestComments;
    }

    public void setGuestComments(OptionalNull<String> guestComments) {
        this.guestComments = guestComments;
    }

    public OptionalNull<String> getNotes() {
        return notes;
    }

    public void setNotes(OptionalNull<String> notes) {
        this.notes = notes;
    }

    public OptionalNull<String> getMessage() {
        return message;
    }

    public void setMessage(OptionalNull<String> message) {
        this.message = message;
    }

    public OptionalNull<String> getCustom1() {
        return custom1;
    }

    public void setCustom1(OptionalNull<String> custom1) {
        this.custom1 = custom1;
    }

    public OptionalNull<String> getCustom2() {
        return custom2;
    }

    public void setCustom2(OptionalNull<String> custom2) {
        this.custom2 = custom2;
    }

    public OptionalNull<String> getCustom3() {
        return custom3;
    }

    public void setCustom3(OptionalNull<String> custom3) {
        this.custom3 = custom3;
    }

    public OptionalNull<String> getCustom4() {
        return custom4;
    }

    public void setCustom4(OptionalNull<String> custom4) {
        this.custom4 = custom4;
    }

    public OptionalNull<String> getCustom5() {
        return custom5;
    }

    public void setCustom5(OptionalNull<String> custom5) {
        this.custom5 = custom5;
    }

    public OptionalNull<String> getCustom6() {
        return custom6;
    }

    public void setCustom6(OptionalNull<String> custom6) {
        this.custom6 = custom6;
    }

    public OptionalNull<String> getCustom7() {
        return custom7;
    }

    public void setCustom7(OptionalNull<String> custom7) {
        this.custom7 = custom7;
    }

    public OptionalNull<String> getCustom8() {
        return custom8;
    }

    public void setCustom8(OptionalNull<String> custom8) {
        this.custom8 = custom8;
    }

    public OptionalNull<String> getCustom9() {
        return custom9;
    }

    public void setCustom9(OptionalNull<String> custom9) {
        this.custom9 = custom9;
    }

    public OptionalNull<String> getCustom10() {
        return custom10;
    }

    public void setCustom10(OptionalNull<String> custom10) {
        this.custom10 = custom10;
    }

    public OptionalNull<String> getFlagColor() {
        return flagColor;
    }

    public void setFlagColor(OptionalNull<String> flagColor) {
        this.flagColor = flagColor;
    }

    public OptionalNull<String> getFlagText() {
        return flagText;
    }

    public void setFlagText(OptionalNull<String> flagText) {
        this.flagText = flagText;
    }

    public OptionalNull<Long> getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(OptionalNull<Long> statusCode) {
        this.statusCode = statusCode;
    }

    public OptionalNull<String> getLang() {
        return lang;
    }

    public void setLang(OptionalNull<String> lang) {
        this.lang = lang;
    }

    public OptionalNull<BigDecimal> getPrice() {
        return price;
    }

    public void setPrice(OptionalNull<BigDecimal> price) {
        this.price = price;
    }

    public OptionalNull<BigDecimal> getDeposit() {
        return deposit;
    }

    public void setDeposit(OptionalNull<BigDecimal> deposit) {
        this.deposit = deposit;
    }

    public OptionalNull<BigDecimal> getTax() {
        return tax;
    }

    public void setTax(OptionalNull<BigDecimal> tax) {
        this.tax = tax;
    }

    public OptionalNull<BigDecimal> getCommission() {
        return commission;
    }

    public void setCommission(OptionalNull<BigDecimal> commission) {
        this.commission = commission;
    }

    public OptionalNull<String> getCurrency() {
        return currency;
    }

    public void setCurrency(OptionalNull<String> currency) {
        this.currency = currency;
    }

    public OptionalNull<Long> getOfferId() {
        return offerId;
    }

    public void setOfferId(OptionalNull<Long> offerId) {
        this.offerId = offerId;
    }

    public OptionalNull<String> getRateDescription() {
        return rateDescription;
    }

    public void setRateDescription(OptionalNull<String> rateDescription) {
        this.rateDescription = rateDescription;
    }

    public OptionalNull<String> getReferer() {
        return referer;
    }

    public void setReferer(OptionalNull<String> referer) {
        this.referer = referer;
    }

    public OptionalNull<String> getRefererEditable() {
        return refererEditable;
    }

    public void setRefererEditable(OptionalNull<String> refererEditable) {
        this.refererEditable = refererEditable;
    }

    public OptionalNull<String> getReference() {
        return reference;
    }

    public void setReference(OptionalNull<String> reference) {
        this.reference = reference;
    }

    public OptionalNull<Channel> getApiSource() {
        return apiSource;
    }

    public void setApiSource(OptionalNull<Channel> apiSource) {
        this.apiSource = apiSource;
    }

    public OptionalNull<String> getApiReference() {
        return apiReference;
    }

    public void setApiReference(OptionalNull<String> apiReference) {
        this.apiReference = apiReference;
    }

    public OptionalNull<String> getApiMessage() {
        return apiMessage;
    }

    public void setApiMessage(OptionalNull<String> apiMessage) {
        this.apiMessage = apiMessage;
    }

    public OptionalNull<Long> getMasterId() {
        return masterId;
    }

    public void setMasterId(OptionalNull<Long> masterId) {
        this.masterId = masterId;
    }

    public OptionalNull<Date> getBookingTime() {
        return bookingTime;
    }

    public void setBookingTime(OptionalNull<Date> bookingTime) {
        this.bookingTime = bookingTime;
    }

    public OptionalNull<Date> getModified() {
        return modified;
    }

    public void setModified(OptionalNull<Date> modified) {
        this.modified = modified;
    }

    public OptionalNull<List<InvoiceLine>> getInvoice() {
        return invoice;
    }

    public void setInvoice(OptionalNull<List<InvoiceLine>> invoice) {
        this.invoice = invoice;
    }

    public OptionalNull<List<InfoItem>> getInfoItems() {
        return infoItems;
    }

    public void setInfoItems(OptionalNull<List<InfoItem>> infoItems) {
        this.infoItems = infoItems;
    }

    public OptionalNull<Long> getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(OptionalNull<Long> invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }
    /*
        public OptionalNull<Date> getInvoiceDate() {
            return invoiceDate;
        }
        public void setInvoiceDate(OptionalNull<Date> invoiceDate) {
            this.invoiceDate = invoiceDate;
        }
    */
    public OptionalNull<Boolean> getAllowChannelUpdate() {
        return allowChannelUpdate;
    }

    public void setAllowChannelUpdate(OptionalNull<Boolean> allowChannelUpdate) {
        this.allowChannelUpdate = allowChannelUpdate;
    }
/*
    public OptionalNull<List<Long>> getGroup() {
        return group;
    }
*/
    /*
    public void setGroup(OptionalNull<List<Long>> group) {
        this.group = group;
    }
    */

    @Override
    public String toString() {
        String result = "Book ID: " + bookId + ", Guest name: " + guestName;

        if( getInvoice().get() != null ) {
            List<InvoiceLine> list = getInvoice().get();
            result += "\nInvoice lines:\n";
            for( InvoiceLine l : list )
                result += l.toString() + "\n";
        }

        return result;
    }

    public HashMap<Long, List<InvoiceLine>> getInvoiceeHash( Long cashCustomerId ) {
        HashMap<Long,List<InvoiceLine>> hash = new HashMap<>();

        if( invoice.get() != null ) {
            List<InvoiceLine> lines
                    = invoice.get().stream().filter( i -> i.getType().get().isCharge()).collect(Collectors.toList());

            for( InvoiceLine il : lines) {
                Long id = il.getInvoiceeId().get();
                if( id == null && cashCustomerId != null ) {
                    id = cashCustomerId;
                }
                if( id != null ) {
                    if (hash.containsKey(id) ) {
                        List<InvoiceLine> inv = hash.get(id);
                        inv.add(il);
                        hash.put(id, inv);
                    } else {
                        List<InvoiceLine> inv = new ArrayList<>();
                        inv.add(il);
                        hash.put(id, inv);
                    }
                }
            }
        }

        return hash;
    }

}
