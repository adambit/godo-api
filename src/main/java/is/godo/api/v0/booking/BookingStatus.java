package is.godo.api.v0.booking;


import is.godo.api.v0.json.NumericEnum;

/**
 * BookingStatus represents the status of a booking in godo.
 */
public class BookingStatus extends NumericEnum {
    public static final BookingStatus CANCELLED = new BookingStatus(0L);
    public static final BookingStatus CONFIRMED = new BookingStatus(1L);
    public static final BookingStatus NEW = new BookingStatus(2L);
    public static final BookingStatus REQUEST = new BookingStatus(3L);

    public BookingStatus(Long id) {
        super(id);
    }

    public BookingStatus(String id) {
        super(id);
    }


    public Boolean equals(BookingStatus other) {
        return super.equals(other);
    }
}