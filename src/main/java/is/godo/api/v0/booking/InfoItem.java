package is.godo.api.v0.booking;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import is.godo.api.v0.json.EncodingAnnotation;
import is.godo.api.v0.json.OptionalNull;

import java.util.Date;


/**
 * InfoItem represents a key/pair value associated with a booking in godo;
 *
 * InfoItems are used to add misc. info to the booking like when guest checked in or out.
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class InfoItem {

    private OptionalNull<Long> infoItemId;

    @EncodingAnnotation(pattern="yyyy-MM-dd HH:mm:ss")
    private OptionalNull<Date> time;
    private OptionalNull<String> code;
    private OptionalNull<String> text;

    public OptionalNull<Long> getInfoItemId() {
        return infoItemId;
    }

    public void setInfoItemId(OptionalNull<Long> infoItemId) {
        this.infoItemId = infoItemId;
    }

    public OptionalNull<Date> getTime() {
        return time;
    }

    public void setTime(OptionalNull<Date> time) {
        this.time = time;
    }

    public OptionalNull<String> getCode() {
        return code;
    }

    public void setCode(OptionalNull<String> code) {
        this.code = code;
    }

    public OptionalNull<String> getText() {
        return text;
    }

    public void setText(OptionalNull<String> text) {
        this.text = text;
    }
}
