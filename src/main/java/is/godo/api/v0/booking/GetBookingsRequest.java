package is.godo.api.v0.booking;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import is.godo.api.v0.Authentication;
import is.godo.api.v0.json.Encoding;
import is.godo.api.v0.json.EncodingAnnotation;
import is.godo.api.v0.json.OptionalNull;

import java.util.Date;

/**
 *
 * The getBookings parameters roomId, bookId, arrivalFrom, arrivalTo, departureFrom, departureTo and modifiedSince are
 * all optional and can be used to limit the returned bookings to the specified values.
 * If not specified, arrivalFrom defaults to yesterday and arrivalTo defaults to plus one year from the current day.
 * Credit card information is not available.
 * Bookings will have a status field containing an integer with the following meanings
 *   0 = Cancelled
 *   1 = Confirmed
 *   2 = New (same as confirmed but unread)
 *   3 = Request
 *
 *
 *  Example Data
 *  All bookings in property
 *  When not otherwise specified the date range is for arrivals from yesterday to one year from now.
 *      {
 *      "authentication":
 *          {
 *          "apiKey": "apiKeyAsSetInAccountSettings",
 *          "propKey": "propKeyAsSetForTheProperty"
 *          },
 *      "includeInvoice": false,
 *      "includeInfoItems": false
 *      }
 *
 * Specific bookings
 * Using Optional parameters, returned bookings will match every optional parameter value
 *      {
 *      "authentication":
 *          {
 *          "apiKey": "apiKeyAsSetInAccountSettings",
 *          "propKey": "propKeyAsSetForTheProperty"
 *          },
 *      "roomId": "12345",
 *      "bookId": "12345678",
 *      "masterId": "12345678",
 *      "arrivalFrom": "20141001",
 *      "arrivalTo": "20151001",
 *      "departureFrom": "20141001",
 *      "departureTo": "20151201",
 *      "modifiedSince": "20131001 12:30:00",
 *      "includeInvoice": false,
 *      "includeInfoItems": false
 *      }
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetBookingsRequest {

    private Authentication authentication;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> roomId;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> bookId;

    @EncodingAnnotation(Encoding.STRING)
    private OptionalNull<Long> masterId;

    @EncodingAnnotation(pattern="yyyyMMdd")
    private OptionalNull<Date> arrivalFrom;

    @EncodingAnnotation(pattern="yyyyMMdd")
    private OptionalNull<Date> arrivalTo;

    @EncodingAnnotation(pattern="yyyyMMdd")
    private OptionalNull<Date> departureFrom;

    @EncodingAnnotation(pattern="yyyyMMdd")
    private OptionalNull<Date> departureTo;

    @EncodingAnnotation(pattern="yyyyMMdd HH:mm:ss")
    private OptionalNull<Date> modifiedSince;

    private OptionalNull<Boolean> includeInvoice = new OptionalNull<>(true);
    private OptionalNull<Boolean> includeInfoItems =new OptionalNull<>(true);


    public Authentication getAuthentication() {
        return authentication;
    }

    public void setAuthentication(Authentication authentication) {
        this.authentication = authentication;
    }

    public OptionalNull<Long> getRoomId() {
        return roomId;
    }

    public void setRoomId(OptionalNull<Long> roomId) {
        this.roomId = roomId;
    }

    public OptionalNull<Long> getBookId() {
        return bookId;
    }

    public void setBookId(OptionalNull<Long> bookId) {
        this.bookId = bookId;
    }

    public OptionalNull<Long> getMasterId() {
        return masterId;
    }

    public void setMasterId(OptionalNull<Long> masterId) {
        this.masterId = masterId;
    }

    public OptionalNull<Date> getArrivalFrom() {
        return arrivalFrom;
    }

    public void setArrivalFrom(OptionalNull<Date> arrivalFrom) {
        this.arrivalFrom = arrivalFrom;
    }

    public OptionalNull<Date> getArrivalTo() {
        return arrivalTo;
    }

    public void setArrivalTo(OptionalNull<Date> arrivalTo) {
        this.arrivalTo = arrivalTo;
    }

    public OptionalNull<Date> getDepartureFrom() {
        return departureFrom;
    }

    public void setDepartureFrom(OptionalNull<Date> departureFrom) {
        this.departureFrom = departureFrom;
    }

    public OptionalNull<Date> getDepartureTo() {
        return departureTo;
    }

    public void setDepartureTo(OptionalNull<Date> departureTo) {
        this.departureTo = departureTo;
    }

    public OptionalNull<Date> getModifiedSince() {
        return modifiedSince;
    }

    public void setModifiedSince(OptionalNull<Date> modifiedSince) {
        this.modifiedSince = modifiedSince;
    }

    public OptionalNull<Boolean> getIncludeInvoice() {
        return includeInvoice;
    }

    public void setIncludeInvoice(OptionalNull<Boolean> includeInvoice) {
        this.includeInvoice = includeInvoice;
    }

    public OptionalNull<Boolean> getIncludeInfoItems() {
        return includeInfoItems;
    }

    public void setIncludeInfoItems(OptionalNull<Boolean> includeInfoItems) {
        this.includeInfoItems = includeInfoItems;
    }
}
