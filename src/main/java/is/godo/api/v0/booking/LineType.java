package is.godo.api.v0.booking;


import is.godo.api.v0.json.NumericEnum;

/**
 * LineType represents the type of an invoice line in godo.
 */
public class LineType extends NumericEnum {

    public static final LineType CHARGE = new LineType(0L);
    public static final LineType OTA_1 = new LineType(8L);
    public static final LineType OTA_2 = new LineType(9L);
    public static final LineType OTA_3 = new LineType(10L);
    public static final LineType OTA_4 = new LineType(11L);
    public static final LineType PAYMENT = new LineType(200L);

    public LineType(Long id) {
        super(id);
    }

    public LineType(String id) {
        super(id);
    }


    public Boolean equals(LineType other) {
        return super.equals(other);
    }

    public boolean isCharge() {
        return getId() < 200;
    }
}
