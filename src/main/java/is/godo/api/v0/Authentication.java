package is.godo.api.v0;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Authentication {
    private String apiKey;
    private String propKey;


    public Authentication() {
    }

    public Authentication(String apiKey) {
        this.apiKey = apiKey;
    }

    public Authentication(String apiKey, String propKey) {
        this.apiKey = apiKey;
        this.propKey = propKey;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getPropKey() {
        return propKey;
    }

    public void setPropKey(String propKey) {
        this.propKey = propKey;
    }
}
