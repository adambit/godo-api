package is.godo.api.v0.booking;

public class BookingStatusTestClass {

    private BookingStatus test;

    public BookingStatusTestClass() {
    }

    public BookingStatus getTest() {
        return test;
    }

    public void setTest(BookingStatus test) {
        this.test = test;
    }
}
