package is.godo.api.v0.booking;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import is.godo.api.v0.json.EncodingAnnotationApplier;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class GetBookingsResponseTest {

    private static ObjectMapper om;

    @BeforeClass
    public static void setup(){
        om = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.setSerializerModifier(new EncodingAnnotationApplier());
        om.registerModule(module);
    }

    private static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    private static final SimpleDateFormat FORMAT_TIME = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Ignore
    public void testEmptyFieldsSerialize() throws JsonProcessingException {

        GetBookingsResponse r = new GetBookingsResponse();
        String json = om.writeValueAsString(r);
        assertEquals("{}", json);
    }

    @Test
    public void testDeserialize() throws Exception {
        String text = new Scanner(GetBookingsResponse.class.getResourceAsStream("/booking/get_bookings_response.json"), "UTF-8").useDelimiter("\\A").next();

        GetBookingsResponse r = om.readValue(text, GetBookingsResponse.class);
        assertEquals(1, r.getBookings().size());

        Booking b = r.getBookings().get(0);
        assert b.getBookId().get().equals(4400966L);
        assert b.getRoomId().get().equals(54488L);
        assert b.getUnitId().get().equals(1L);
        assert b.getRoomQty().get().equals(1L);
        assert b.getStatus().get().equals(BookingStatus.CONFIRMED);
        assert b.getFirstNight().get().equals(FORMAT.parse("2017-10-07"));
        assert b.getLastNight().get().equals(FORMAT.parse("2017-10-07"));
        assert b.getNumAdult().get().equals(1L);
        assert b.getNumChild().get().equals(0L);
        assert b.getGuestTitle().get().equals("");
        assert b.getGuestFirstName().get().equals("asdf");
        assert b.getGuestName().get().equals("asdf");
        assert b.getGuestEmail().get().equals("asdf");
        assert b.getGuestPhone().get().equals("asdf");
        assert b.getGuestFax().get().equals("");
        assert b.getGuestAddress().get().equals("");
        assert b.getGuestCity().get().equals("");
        assert b.getGuestPostcode().get().equals("");
        assert b.getGuestCountry().get().equals("");
        assert b.getGuestArrivalTime().get().equals("");
        assert b.getGuestVoucher().get().equals("");
        assert b.getGuestComments().get().equals("");
        assert b.getNotes().get().equals("");
        assert b.getMessage().get().equals("");
        assert b.getCustom1().get().equals("");
        assert b.getCustom2().get().equals("");
        assert b.getCustom3().get().equals("");
        assert b.getCustom4().get().equals("");
        assert b.getCustom5().get().equals("");
        assert b.getCustom6().get().equals("");
        assert b.getCustom7().get().equals("");
        assert b.getCustom8().get().equals("");
        assert b.getCustom9().get().equals("");
        assert b.getCustom10().get().equals("");
        assert b.getCustom1().get().equals("");
        assert b.getFlagColor().get().equals("");
        assert b.getFlagText().get().equals("");
        assert b.getStatusCode().get().equals(0L);
        assert b.getLang().get().equals("");
        assert b.getPrice().get().equals(new BigDecimal("123.00"));
        assert b.getDeposit().get().equals(new BigDecimal("0.00"));
        assert b.getTax().get().equals(new BigDecimal("0.00"));
        assert b.getCommission().get().equals(new BigDecimal("0.00"));
        assert b.getCurrency().get().equals("EUR");
        assert b.getOfferId().get().equals(0L);
        assert b.getReferer().get().equals("godotest");
        assert b.getRefererEditable().get().equals("godotest");
        assert b.getReference().get().equals("");
        assert b.getApiSource().get().equals(Channel.GODO_PROPERTY);
        assert b.getApiReference().get().equals("");
        assert b.getAllowChannelUpdate().get().equals(true);
        assert b.getBookingTime().get().equals(FORMAT_TIME.parse("2017-10-04 12:56:41"));
        assert b.getModified().get().equals(FORMAT_TIME.parse("2017-10-04 12:56:53"));
        assert b.getMasterId().isPresent();
        assert b.getMasterId().get() == null ;
    }
}
