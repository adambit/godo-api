package is.godo.api.v0.booking;



import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import is.godo.api.v0.json.Encoding;
import is.godo.api.v0.json.EncodingAnnotation;
import is.godo.api.v0.json.EncodingAnnotationApplier;
import is.godo.api.v0.json.OptionalNull;


import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LineTypeTest {

    private static ObjectMapper om;

    @BeforeClass
    public static void setup(){
         om = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.setSerializerModifier(new EncodingAnnotationApplier());
        om.registerModule(module);
    }

    @Test
    public void testSerializeation() throws Exception {
        LineTypeTestClass test = new LineTypeTestClass();
        test.setTest( LineType.CHARGE);

        String json = om.writeValueAsString(test);
        assertEquals("{\"test\":0}", json);
    }

    @Test
    public void testDeserialization() throws  Exception{


        LineTypeTestClass test = om.readValue("{\"test\":0}", LineTypeTestClass.class);
        assertEquals(true, LineType.CHARGE.equals(test.getTest()));


        test = om.readValue("{\"test\":\"0\"}", LineTypeTestClass.class);
        assertEquals(true, LineType.CHARGE.equals(test.getTest()));



    }

    @Test
    public void testOptionalNullSerializeation() throws Exception {
        class OptionalNullTestClass {
            private OptionalNull<LineType> test;

            public OptionalNull<LineType> getTest() {
                return test;
            }

            public void setTest(OptionalNull<LineType> test) {
                this.test = test;
            }
        }

        OptionalNullTestClass test = new OptionalNullTestClass();
        test.test = new OptionalNull<>(LineType.CHARGE);

        String json = om.writeValueAsString(test);
        assertEquals("{\"test\":0}", json);
    }



    public class OptionalNullEncodingTestClass {
        @EncodingAnnotation(Encoding.STRING)
        private OptionalNull<LineType> test;

        public OptionalNull<LineType> getTest() {
            return test;
        }

        public void setTest(OptionalNull<LineType> test) {
            this.test = test;
        }
    }

    @Test
    public void testOptionalNullEncodingSerializeation() throws Exception {


        OptionalNullEncodingTestClass test = new OptionalNullEncodingTestClass();
        test.test = new OptionalNull<>(LineType.CHARGE);


        String json = om.writeValueAsString(test);
        assertEquals("{\"test\":\"0\"}", json);
    }

}


