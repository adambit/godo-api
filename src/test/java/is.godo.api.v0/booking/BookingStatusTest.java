package is.godo.api.v0.booking;





import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import is.godo.api.v0.json.Encoding;
import is.godo.api.v0.json.EncodingAnnotationApplier;
import is.godo.api.v0.json.OptionalNull;
import is.godo.api.v0.json.EncodingAnnotation;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BookingStatusTest {

    private static ObjectMapper om;

    @BeforeClass
    public static void setup(){
        om = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.setSerializerModifier(new EncodingAnnotationApplier());
        om.registerModule(module);
    }

    @Test
    public void testSerializeation() throws Exception {


        BookingStatusTestClass test = new BookingStatusTestClass();
        test.setTest( BookingStatus.REQUEST);


        String json = om.writeValueAsString(test);
        assertEquals("{\"test\":3}", json);
    }

    @Test
    public void testDeserialization() throws  Exception{



        BookingStatusTestClass test = om.readValue("{\"test\":3}", BookingStatusTestClass.class);
        assertEquals(true, BookingStatus.REQUEST.equals(test.getTest()));



        test = om.readValue("{\"test\":\"3\"}", BookingStatusTestClass.class);
        assertEquals(true, BookingStatus.REQUEST.equals(test.getTest()));



    }

    @Test
    public void testOptionalNullSerializeation() throws Exception {
        class OptionalNullTestClass {
            OptionalNull<BookingStatus> test;

            public OptionalNull<BookingStatus> getTest() {
                return test;
            }

            public void setTest(OptionalNull<BookingStatus> test) {
                this.test = test;
            }
        }

        OptionalNullTestClass test = new OptionalNullTestClass();
        test.test = new OptionalNull<>(BookingStatus.REQUEST);


        String json = om.writeValueAsString(test);
        assertEquals("{\"test\":3}", json);
    }


    public class OptionalNullEncodingTestClass {
        @EncodingAnnotation(Encoding.STRING)
        private OptionalNull<BookingStatus> test;

        public OptionalNull<BookingStatus> getTest() {
            return test;
        }

        public void setTest(OptionalNull<BookingStatus> test) {
            this.test = test;
        }
    }

    @Test
    public void testOptionalNullEncodingSerializeation() throws Exception {


        OptionalNullEncodingTestClass test = new OptionalNullEncodingTestClass();
        test.test = new OptionalNull<>(BookingStatus.REQUEST);


        String json = om.writeValueAsString(test);
        assertEquals("{\"test\":\"3\"}", json);
    }

}


