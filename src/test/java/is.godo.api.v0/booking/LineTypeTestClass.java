package is.godo.api.v0.booking;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class LineTypeTestClass {
    @JsonProperty
    private LineType test;


    public LineType getTest() {
        return test;
    }


    public void setTest(LineType test) {
        this.test = test;
    }

    @JsonCreator
    public LineTypeTestClass() {
        this.test = test;
    }
}