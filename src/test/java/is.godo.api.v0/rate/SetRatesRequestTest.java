package is.godo.api.v0.rate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import is.godo.api.v0.Action;
import is.godo.api.v0.inventory.SetRoomDatesRequestTest;
import is.godo.api.v0.json.EncodingAnnotationApplier;
import is.godo.api.v0.json.OptionalNull;
import is.godo.api.v0.Authentication;
import org.junit.BeforeClass;
import org.junit.Test;


import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class SetRatesRequestTest {
    private static ObjectMapper om;
    private static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    @BeforeClass
    public static void setup(){
        om = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.setSerializerModifier(new EncodingAnnotationApplier());
        om.registerModule(module);
    }

    @Test
    public void testEmptySerialization() throws  Exception{
        SetRatesRequest req = new SetRatesRequest();
        String json = om.writeValueAsString(req);
        assertEquals("{}", json);
    }

    @Test
    public void testComplexSerialization() throws Exception {
        SetRatesRequest req = new SetRatesRequest();
        Authentication auth = new Authentication("apiKeyAsSetInAccountSettings","propKeyAsSetForTheProperty");

        req.setAuthentication(auth);

        List<Rate> rates = new ArrayList<>();
        Rate rate1 = new Rate();
        rate1.setAction(Action.MODIFY);
        rate1.setRateId(new OptionalNull<>(123456L));
        rate1.setRoomId(new OptionalNull<>(12345L));
        rate1.setFirstNight(new OptionalNull<>(FORMAT.parse("2014-10-01")));
        rate1.setLastNight(new OptionalNull<>( FORMAT.parse("2016-10-01")));
        rate1.setName(new OptionalNull<>("Rate 1"));
        rate1.setMinNights(new OptionalNull<>(1L));
        rate1.setMaxNights(new OptionalNull<>(30L));
        rate1.setMinAdvance(new OptionalNull<>(0L));
        rate1.setMaxAdvance(new OptionalNull<>(365L));
        rate1.setStrategy(new OptionalNull<>(RateStrategy.ALLOW_LOWER_PRICES));
        rate1.setRoomPrice(new OptionalNull<>(new BigDecimal("100")));
        rate1.setRoomPriceEnable(new OptionalNull<>(true));
        rate1.setRoomPriceGuests(new OptionalNull<Long>(0L));
        rate1.setPrice1p(new OptionalNull<>(new BigDecimal("60.00")));
        rate1.setPrice1pEnable(new OptionalNull<>(true));
        rate1.setPrice2p(new OptionalNull<>(new BigDecimal("85.50")));
        rate1.setPrice2pEnable(new OptionalNull<>(true));
        rate1.setExtraPersonPrice(new OptionalNull<>(new BigDecimal("20.00")));
        rate1.setExtraPersonPriceEnable(new OptionalNull<>(true));
        rate1.setExtraChildPrice(new OptionalNull<>(new BigDecimal("0")));
        rate1.setExtraChildPriceEnable(new OptionalNull<>(false));
        rate1.setBookingcomRateCode(new OptionalNull<>(""));
        rate1.setExpediacomRateCode(new OptionalNull<>(""));
        rate1.setFeratelRateCode(new OptionalNull<>(""));
        rate1.setLastminutecomRateCode(new OptionalNull<>(""));
        rate1.setTablethotelscomRateCode(new OptionalNull<>(""));
        rate1.setTravelocitycomRateCode(new OptionalNull<>(""));

        Rate rate2 = new Rate();
        rate2.setAction(Action.DELETE);
        rate2.setRateId(new OptionalNull<>(123457L));
        rate2.setRoomId(new OptionalNull<>(12346L));

        rates.add(rate1);
        rates.add(rate2);

        req.setSetRates(rates);

        String expected = new Scanner(SetRoomDatesRequestTest.class.getResourceAsStream("/rate/set_rates_request.json"), "UTF-8").useDelimiter("\\A").next();
        String json = om.writerWithDefaultPrettyPrinter().writeValueAsString(req);

        assertEquals(expected, json);
    }


    @Test
    public void testAll() throws  Exception{
        SetRatesRequest req = new SetRatesRequest();
        Authentication auth = new Authentication("fdsa","asdf");

        req.setAuthentication(auth);

        List<Rate> rateList = new ArrayList<>();

        Rate create = new Rate();
        create.setAction(Action.NEW);

        Rate modify = new Rate();
        modify.setAction(Action.MODIFY);

        Rate delete = new Rate();
        delete.setAction(Action.DELETE);

        rateList.add(create);
        rateList.add(modify);
        rateList.add(delete);
        req.setSetRates(rateList);


        // Encoding
        String json = om.writeValueAsString(req);
        assertEquals("{\"authentication\":{\"apiKey\":\"fdsa\",\"propKey\":\"asdf\"},\"setRates\":[{\"action\":\"new\"},{\"action\":\"modify\"},{\"action\":\"delete\"}]}", json);
    }
}
