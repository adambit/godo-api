package is.godo.api.v0.rate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import is.godo.api.v0.json.EncodingAnnotationApplier;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SetRatesResponseTest {

    private static ObjectMapper om;

    @BeforeClass
    public static void setup(){
        om = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.setSerializerModifier(new EncodingAnnotationApplier());
        om.registerModule(module);
    }

    @Test
    public void testEmptyDeserialization() throws Exception{

        SetRatesResponse resp = om.readValue("{}", SetRatesResponse.class);

    }

    @Test
    public void testEmptyListDeserialization() throws Exception{

        SetRatesResponse resp = om.readValue("[]", SetRatesResponse.class);

    }

    @Test
    public void testDeserialization() throws Exception{

        String json = "[" +
                "{\"rateId\": \"123\" },"+
                "{\"rateId\": \"124\" },"+
                "{\"rateId\": \"125\" }"+
                "]";

        SetRatesResponse resp = om.readValue(json, SetRatesResponse.class);

        assertEquals(3, resp.getResp().size());

    }
}
