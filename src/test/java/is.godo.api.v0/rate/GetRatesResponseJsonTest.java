package is.godo.api.v0.rate;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.fasterxml.jackson.databind.module.SimpleModule;
import is.godo.api.v0.inventory.SetRoomDatesRequestTest;
import is.godo.api.v0.json.EncodingAnnotationApplier;
import is.godo.api.v0.json.OptionalNull;
import is.godo.api.v0.booking.Channel;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;


public class GetRatesResponseJsonTest {


    //private static final Logger log = LoggerFactory.getLogger(RoomTypeJsonTest.class);
    private static ObjectMapper om;

    @BeforeClass
    public static void setup() {
        om = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.setSerializerModifier(new EncodingAnnotationApplier());
        om.registerModule(module);
    }

    @Test
    public void testEmpty() throws Exception {

    }

    private static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    @Test
    public void testGetRatesResponseSerializer() throws Exception {
        GetRatesResponse r = new GetRatesResponse();
        List<Rate> rates = new ArrayList<>();

        Date firstNight = FORMAT.parse("2016-10-17");
        Date lastNight = FORMAT.parse("2018-10-17");


        Rate rate1 = new Rate();
        rate1.setRateId(new OptionalNull<>(449783L));
        rate1.setRoomId(new OptionalNull<>(54488L));
        rate1.setOfferId(new OptionalNull<>(1L));
        rate1.setFirstNight(new OptionalNull<>(firstNight));
        rate1.setLastNight(new OptionalNull<>(lastNight));
        rate1.setName(new OptionalNull<>("Rate"));

        rate1.setMinNights(new OptionalNull<>(1L));
        rate1.setMaxNights(new OptionalNull<>(365L));
        rate1.setMinAdvance(new OptionalNull<>(0L));
        rate1.setMaxAdvance(new OptionalNull<>(365L));

        rate1.setStrategy(new OptionalNull<>(RateStrategy.ALLOW_LOWER_PRICES));
        rate1.setRoomPrice(new OptionalNull<>(new BigDecimal("100.00")));
        rate1.setRoomPriceEnable(new OptionalNull<>(true));
        rate1.setRoomPriceGuests(new OptionalNull<Long>(0L));

        rate1.setPrice1p(new OptionalNull<>(new BigDecimal("0.00")));
        rate1.setPrice1pEnable(new OptionalNull<>(false));

        rate1.setPrice2p(new OptionalNull<>(new BigDecimal("0.00")));
        rate1.setPrice2pEnable(new OptionalNull<>(false));

        rate1.setExtraPersonPrice(new OptionalNull<>(new BigDecimal("0.00")));
        rate1.setExtraPersonPriceEnable(new OptionalNull<>(false));

        rate1.setExtraChildPrice(new OptionalNull<>(new BigDecimal("0.00")));
        rate1.setExtraChildPriceEnable(new OptionalNull<>(false));

        rate1.setAgodacomRateCode(new OptionalNull<>(""));
        rate1.setBudgetplacescomRateCode(new OptionalNull<>(""));
        rate1.setBookingcomRateCode(new OptionalNull<>(""));
        rate1.setExpediacomRateCode(new OptionalNull<>(""));
        rate1.setFeratelRateCode(new OptionalNull<>(""));
        rate1.setLastminutecomRateCode(new OptionalNull<>(""));
        rate1.setLateroomscomRateCode(new OptionalNull<>(""));
        rate1.setTablethotelscomRateCode(new OptionalNull<>(""));
        rate1.setTravelocitycomRateCode(new OptionalNull<>(""));

        rate1.setCanInMon(new OptionalNull<>(true));
        rate1.setCanInTue(new OptionalNull<>(true));
        rate1.setCanInWed(new OptionalNull<>(true));
        rate1.setCanInThu(new OptionalNull<>(true));
        rate1.setCanInFri(new OptionalNull<>(true));
        rate1.setCanInSat(new OptionalNull<>(true));
        rate1.setCanInSun(new OptionalNull<>(true));


        List<ChannelEnabled> enabled1 = new ArrayList<>();
        enabled1.add(new ChannelEnabled(new Channel(0L),true));
        enabled1.add(new ChannelEnabled(new Channel(2L),true));
        enabled1.add(new ChannelEnabled(new Channel(8L),true));
        enabled1.add(new ChannelEnabled(new Channel(12L),true));
        enabled1.add(new ChannelEnabled(new Channel(13L),true));
        enabled1.add(new ChannelEnabled(new Channel(14L),true));
        enabled1.add(new ChannelEnabled(new Channel(15L),true));
        enabled1.add(new ChannelEnabled(new Channel(17L),true));
        enabled1.add(new ChannelEnabled(new Channel(18L),true));
        enabled1.add(new ChannelEnabled(new Channel(19L),true));
        enabled1.add(new ChannelEnabled(new Channel(20L),true));
        enabled1.add(new ChannelEnabled(new Channel(22L),true));
        enabled1.add(new ChannelEnabled(new Channel(23L),true));
        enabled1.add(new ChannelEnabled(new Channel(24L),true));
        enabled1.add(new ChannelEnabled(new Channel(27L),true));
        enabled1.add(new ChannelEnabled(new Channel(30L),true));
        enabled1.add(new ChannelEnabled(new Channel(31L),true));
        enabled1.add(new ChannelEnabled(new Channel(32L),true));
        enabled1.add(new ChannelEnabled(new Channel(33L),true));
        enabled1.add(new ChannelEnabled(new Channel(34L),true));
        enabled1.add(new ChannelEnabled(new Channel(35L),true));
        enabled1.add(new ChannelEnabled(new Channel(36L),true));
        enabled1.add(new ChannelEnabled(new Channel(38L),true));
        enabled1.add(new ChannelEnabled(new Channel(41L),true));
        enabled1.add(new ChannelEnabled(new Channel(42L),true));
        enabled1.add(new ChannelEnabled(new Channel(43L),true));
        enabled1.add(new ChannelEnabled(new Channel(44L),true));
        enabled1.add(new ChannelEnabled(new Channel(46L),true));
        enabled1.add(new ChannelEnabled(new Channel(50L),true));
        enabled1.add(new ChannelEnabled(new Channel(51L),true));
        enabled1.add(new ChannelEnabled(new Channel(52L),true));
        enabled1.add(new ChannelEnabled(new Channel(53L),true));
        enabled1.add(new ChannelEnabled(new Channel(54L),true));
        enabled1.add(new ChannelEnabled(new Channel(55L),true));
        enabled1.add(new ChannelEnabled(new Channel(999L),true));
        rate1.setChannelEnabledList(enabled1);
        rates.add(rate1);



        Rate rate2 = new Rate();
        rate2.setRateId(new OptionalNull<>(449784L));
        rate2.setRoomId(new OptionalNull<>( 54489L));
        rate2.setOfferId(new OptionalNull<>( 1L));
        rate2.setFirstNight(new OptionalNull<>( FORMAT.parse("2016-10-17")));
        rate2.setLastNight(new OptionalNull<>( FORMAT.parse("2018-10-17")));
        rate2.setName(new OptionalNull<>( "Rate"));
        rate2.setMinNights(new OptionalNull<>( 1L));
        rate2.setMaxNights(new OptionalNull<>( 365L));
        rate2.setMinAdvance(new OptionalNull<>( 0L));
        rate2.setMaxAdvance(new OptionalNull<>( 365L));
        rate2.setStrategy(new OptionalNull<>( RateStrategy.ALLOW_LOWER_PRICES));
        rate2.setRoomPrice(new OptionalNull<>( new BigDecimal("150.00")));
        rate2.setRoomPriceEnable(new OptionalNull<>( true));
        rate2.setRoomPriceGuests(new OptionalNull<Long>( 0L));
        rate2.setPrice1p(new OptionalNull<>( new BigDecimal("0.00")));
        rate2.setPrice1pEnable(new OptionalNull<>( false));
        rate2.setPrice2p(new OptionalNull<>( new BigDecimal("0.00")));
        rate2.setPrice2pEnable(new OptionalNull<>(  true));
        rate2.setExtraPersonPrice(new OptionalNull<>( new BigDecimal("0.00")));
        rate2.setExtraPersonPriceEnable(new OptionalNull<>( false));
        rate2.setExtraChildPrice(new OptionalNull<>( new BigDecimal("0.00")));
        rate2.setExtraChildPriceEnable(new OptionalNull<>( false));
        rate2.setCanInMon(new OptionalNull<>( true));
        rate2.setCanInTue(new OptionalNull<>( true));
        rate2.setCanInWed(new OptionalNull<>( true));
        rate2.setCanInThu(new OptionalNull<>( true));
        rate2.setCanInFri(new OptionalNull<>( true));
        rate2.setCanInSat(new OptionalNull<>( true));
        rate2.setCanInSun(new OptionalNull<>( true));
        List<ChannelEnabled> enabled2 = new ArrayList<>();
        enabled2.add(new ChannelEnabled(new Channel(0L),true));
        enabled2.add(new ChannelEnabled(new Channel(2L),true));
        enabled2.add(new ChannelEnabled(new Channel(8L),true));
        enabled2.add(new ChannelEnabled(new Channel(12L),true));
        enabled2.add(new ChannelEnabled(new Channel(13L),true));
        enabled2.add(new ChannelEnabled(new Channel(14L),true));
        enabled2.add(new ChannelEnabled(new Channel(15L),true));
        enabled2.add(new ChannelEnabled(new Channel(17L),true));
        enabled2.add(new ChannelEnabled(new Channel(18L),true));
        enabled2.add(new ChannelEnabled(new Channel(19L),true));
        enabled2.add(new ChannelEnabled(new Channel(20L),true));
        enabled2.add(new ChannelEnabled(new Channel(22L),true));
        enabled2.add(new ChannelEnabled(new Channel(23L),true));
        enabled2.add(new ChannelEnabled(new Channel(24L),true));
        enabled2.add(new ChannelEnabled(new Channel(27L),true));
        enabled2.add(new ChannelEnabled(new Channel(30L),true));
        enabled2.add(new ChannelEnabled(new Channel(31L),true));
        enabled2.add(new ChannelEnabled(new Channel(32L),true));
        enabled2.add(new ChannelEnabled(new Channel(33L),true));
        enabled2.add(new ChannelEnabled(new Channel(34L),true));
        enabled2.add(new ChannelEnabled(new Channel(35L),true));
        enabled2.add(new ChannelEnabled(new Channel(36L),true));
        enabled2.add(new ChannelEnabled(new Channel(38L),true));
        enabled2.add(new ChannelEnabled(new Channel(41L),true));
        enabled2.add(new ChannelEnabled(new Channel(42L),true));
        enabled2.add(new ChannelEnabled(new Channel(43L),true));
        enabled2.add(new ChannelEnabled(new Channel(44L),true));
        enabled2.add(new ChannelEnabled(new Channel(46L),true));
        enabled2.add(new ChannelEnabled(new Channel(50L),true));
        enabled2.add(new ChannelEnabled(new Channel(51L),true));
        enabled2.add(new ChannelEnabled(new Channel(52L),true));
        enabled2.add(new ChannelEnabled(new Channel(53L),true));
        enabled2.add(new ChannelEnabled(new Channel(54L),true));
        enabled2.add(new ChannelEnabled(new Channel(55L),true));
        enabled2.add(new ChannelEnabled(new Channel(999L),true));
        rate2.setChannelEnabledList(enabled2);
        rate2.setAgodacomRateCode(new OptionalNull<>(""));
        rate2.setBookingcomRateCode(new OptionalNull<>(""));
        rate2.setBudgetplacescomRateCode(new OptionalNull<>(""));
        rate2.setExpediacomRateCode(new OptionalNull<>(""));
        rate2.setFeratelRateCode(new OptionalNull<>(""));
        rate2.setHotelbedscomRateCode(new OptionalNull<>(""));
        rate2.setLastminutecomRateCode(new OptionalNull<>(""));
        rate2.setLateroomscomRateCode(new OptionalNull<>(""));
        rate2.setOtaRateCode(new OptionalNull<>(""));
        rate2.setTablethotelscomRateCode(new OptionalNull<>(""));
        rate2.setTravelocitycomRateCode(new OptionalNull<>(""));

        rates.add(rate2);

        r.setGetRates(rates);

        String json = om.writerWithDefaultPrettyPrinter().writeValueAsString(r);
        String expected = new Scanner(SetRoomDatesRequestTest.class.getResourceAsStream("/rate/get_rates_response.json"), "UTF-8").useDelimiter("\\A").next();
        assertEquals(expected, json);


    }

    @Test
    public void testChannels() throws Exception {
        Rate r = new Rate();
        List<ChannelEnabled> channels = new ArrayList<>();
        ChannelEnabled channel = new ChannelEnabled(Channel.AGODA_API, true);
        channels.add(channel);
        r.setChannelEnabledList(channels);

        System.out.println(om.writerWithDefaultPrettyPrinter().writeValueAsString(r));
    }

    @Test
    public void testGetRatesResponseDeserializer() throws Exception {

        GetRatesResponse expected = new GetRatesResponse();
        List<Rate> rates = new ArrayList<>();

        Date firstNight = new Date();
        firstNight.setTime(0);
        firstNight.setYear(2014);
        firstNight.setMonth(10);
        firstNight.setDate(1);

        Date lastNight = new Date();
        lastNight.setTime(0);
        lastNight.setYear(2014);
        lastNight.setMonth(10);
        lastNight.setDate(1);

        Rate rate1 = new Rate();
        rate1.setRateId(new OptionalNull<>(449783L));
        rate1.setRoomId(new OptionalNull<>(254488L));
        rate1.setFirstNight(new OptionalNull<>(firstNight));
        rate1.setLastNight(new OptionalNull<>(lastNight));
        rate1.setName(new OptionalNull<>("Rate 1"));
        rate1.setMinNights(new OptionalNull<>(1L));
        rate1.setMaxNights(new OptionalNull<>(30L));
        rate1.setMinAdvance(new OptionalNull<>(0L));
        rate1.setMaxAdvance(new OptionalNull<>(365L));
        rate1.setStrategy(new OptionalNull<>(RateStrategy.ALLOW_LOWER_PRICES));
        rate1.setRoomPrice(new OptionalNull<>(new BigDecimal("100")));
        rate1.setRoomPriceEnable(new OptionalNull<>(true));
        rate1.setRoomPriceGuests(new OptionalNull<Long>(0L));
        rate1.setPrice1p(new OptionalNull<>(new BigDecimal("60.00")));
        rate1.setPrice1pEnable(new OptionalNull<>(true));
        rate1.setPrice2p(new OptionalNull<>(new BigDecimal("85.50")));
        rate1.setPrice2pEnable(new OptionalNull<>(true));
        rate1.setExtraPersonPrice(new OptionalNull<>(new BigDecimal("20.00")));
        rate1.setExtraPersonPriceEnable(new OptionalNull<>(true));
        rate1.setExtraChildPrice(new OptionalNull<>(new BigDecimal("0")));
        rate1.setExtraChildPriceEnable(new OptionalNull<>(false));
        rate1.setBookingcomRateCode(new OptionalNull<>(""));
        rate1.setExpediacomRateCode(new OptionalNull<>(""));
        rate1.setFeratelRateCode(new OptionalNull<>(""));
        rate1.setLastminutecomRateCode(new OptionalNull<>(""));
        rate1.setTablethotelscomRateCode(new OptionalNull<>(""));
        rate1.setTravelocitycomRateCode(new OptionalNull<>(""));
        rates.add(rate1);
        expected.setGetRates(rates);

        String json = new Scanner(GetRatesResponseJsonTest.class.getResourceAsStream("/rate/get_rates_response.json"), "UTF-8").useDelimiter("\\A").next();
        GetRatesResponse actual = om.readValue(json, GetRatesResponse.class);
    }


}

