package is.godo.api.v0.rate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import is.godo.api.v0.json.EncodingAnnotationApplier;
import is.godo.api.v0.json.OptionalNull;
import is.godo.api.v0.booking.Channel;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class RateJsonTest {
    private static ObjectMapper om;

    @BeforeClass
    public static void setup() {
        om = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.setSerializerModifier(new EncodingAnnotationApplier());
        om.registerModule(module);
    }


    @Test
    public void testEmpty() throws Exception{
        String json = om.writeValueAsString(new Rate());
        assertEquals("{}", json);
    }

    @Test
    public void testSimpleSerialization() throws Exception{
        Rate rate = new Rate();
        rate.setPrice1pEnable(new OptionalNull<>(true));
        rate.setPrice1p(new OptionalNull<>(new BigDecimal("123.24")));

        String json = om.writerWithDefaultPrettyPrinter().writeValueAsString(rate);
        assertEquals("{\n" +
                "  \"1pPrice\" : \"123.24\",\n" +
                "  \"1pPriceEnable\" : \"1\"\n" +
                "}", json);
    }

    @Test
    public void testChannelEnabledDeserialization() throws Exception {


        String json = "{\"channel000\": \"1\", \"channel912\": \"true\", \"channel432\": true, \"channel012\": \"0\", \"channel942\": \"false\", \"channel454\": false }";
        Rate r = om.readValue(json, Rate.class);
        assertNotEquals(null, r);
        assertEquals(6, r.getChannelEnabledList().size());

        for(ChannelEnabled c: r.getChannelEnabledList()){
            Long id = c.getChannel().getId();

            if(id.equals(0L)){
                assertEquals(true, c.getEnabled());
                continue;
            }

            if(id.equals(912L)){
                assertEquals(true, c.getEnabled());
                continue;
            }

            if(id.equals(432L)){
                assertEquals(true, c.getEnabled());
                continue;
            }

            if(id.equals(12L)){
                assertEquals(false, c.getEnabled());
                continue;
            }

            if(id.equals(942L)){
                assertEquals(false, c.getEnabled());
                continue;
            }

            if(id.equals(454L)){
                assertEquals(false, c.getEnabled());
                continue;
            }
        }
    }

    @Test
    public void testChannelEnabledSerialization() throws Exception {


        String expected = "{\"channel000\":\"1\",\"channel912\":\"1\",\"channel432\":\"1\",\"channel012\":\"0\",\"channel942\":\"0\",\"channel454\":\"0\"}";


        List<ChannelEnabled> channelEnabledList = new ArrayList<>();
        channelEnabledList.add(new ChannelEnabled(new Channel(0L), true));
        channelEnabledList.add(new ChannelEnabled(new Channel(912L), true));
        channelEnabledList.add(new ChannelEnabled(new Channel(432L), true));
        channelEnabledList.add(new ChannelEnabled(new Channel(12L), false));
        channelEnabledList.add(new ChannelEnabled(new Channel(942L), false));
        channelEnabledList.add(new ChannelEnabled(new Channel(454L), false));

        Rate r = new Rate();
        r.setChannelEnabledList(channelEnabledList);

        String json = om.writeValueAsString(r);

        assertEquals(expected, json);
    }
}
