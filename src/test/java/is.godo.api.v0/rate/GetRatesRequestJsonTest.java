package is.godo.api.v0.rate;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import is.godo.api.v0.json.EncodingAnnotationApplier;
import is.godo.api.v0.json.OptionalNull;
import is.godo.api.v0.Authentication;


import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GetRatesRequestJsonTest {
    private static ObjectMapper om;

    @BeforeClass
    public static void setup(){
        om = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.setSerializerModifier(new EncodingAnnotationApplier());
        om.registerModule(module);
    }
    @Test
    public void testEmptyFieldsSerializer() throws JsonProcessingException {
        GetRatesRequest r = new GetRatesRequest();
        String json = om.writeValueAsString(r);
        assertEquals("{}", json);
    }

    @Test
    public void testGetRatesRequestSerializer() throws JsonProcessingException {
        GetRatesRequest r = new GetRatesRequest();

        Authentication auth = new Authentication("apiKeyAsSetInAccountSettings","propKeyAsSetForTheProperty");

        r.setAuthentication(auth);

        String json = om.writeValueAsString(r);
        assertEquals("{\"authentication\":{\"apiKey\":\"apiKeyAsSetInAccountSettings\",\"propKey\":\"propKeyAsSetForTheProperty\"}}", json);
    }

    @Test
    public void testGetRatesRequestWithRoomIDSerializer() throws JsonProcessingException {
        GetRatesRequest r = new GetRatesRequest();
        r.setRoomId(new OptionalNull<>(12345L));

        Authentication auth = new Authentication("apiKeyAsSetInAccountSettings","propKeyAsSetForTheProperty");

        r.setAuthentication(auth);

        String json = om.writeValueAsString(r);
        assertEquals("{\"authentication\":{\"apiKey\":\"apiKeyAsSetInAccountSettings\",\"propKey\":\"propKeyAsSetForTheProperty\"},\"roomId\":12345}", json);
    }


    @Test
    public void testEmptyFieldsDeSerializer() throws JsonProcessingException {
        GetRatesRequest r = new GetRatesRequest();
        String json = om.writeValueAsString(r);
        assertEquals("{}", json);
    }

    @Test
    public void testGetRatesRequestDeSerializer() throws JsonProcessingException {
        GetRatesRequest r = new GetRatesRequest();

        Authentication auth = new Authentication("apiKeyAsSetInAccountSettings","propKeyAsSetForTheProperty");

        r.setAuthentication(auth);

        String json = om.writeValueAsString(r);
        assertEquals("{\"authentication\":{\"apiKey\":\"apiKeyAsSetInAccountSettings\",\"propKey\":\"propKeyAsSetForTheProperty\"}}", json);
    }

    @Test
    public void testGetRatesRequestWithRoomIDDeSerializer() throws JsonProcessingException {
        GetRatesRequest r = new GetRatesRequest();
        r.setRoomId(new OptionalNull<>(12345L));

        Authentication auth = new Authentication("apiKeyAsSetInAccountSettings","propKeyAsSetForTheProperty");

        r.setAuthentication(auth);

        String json = om.writeValueAsString(r);
        assertEquals("{\"authentication\":{\"apiKey\":\"apiKeyAsSetInAccountSettings\",\"propKey\":\"propKeyAsSetForTheProperty\"},\"roomId\":12345}", json);
    }
}
