package is.godo.api.v0;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import is.godo.api.v0.booking.*;
import is.godo.api.v0.inventory.*;
import is.godo.api.v0.json.ErrorCode;

import is.godo.api.v0.json.OptionalNull;
import is.godo.api.v0.property.*;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.hamcrest.MatcherAssert.assertThat;

import static org.junit.Assert.assertEquals;


public class GodoPropertyClientTest {
    final static Logger log = LoggerFactory.getLogger(GodoPropertyClientTest.class);

    private static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    private static final SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
    private static final SimpleDateFormat FORMAT_TIME = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private GodoPropertyClient client;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(wireMockConfig().port(8089));//.httpsPort(8443));

    @Before
    public void setup() throws Exception {
        //client = new GodoPropertyClient("http://localhost:8089");
        client = new GodoPropertyClient();
    }


    @Ignore("Not working")
    public void testGetBookingValids() throws GodoPropertyException, ParseException {
        Authentication auth = new Authentication("apiKey","propKey");


        GetBookingsRequest req = new GetBookingsRequest();
        req.setAuthentication(auth);
        List<Booking> resp;

        String jsonResp = new Scanner( GodoPropertyClientTest.class.getResourceAsStream("/booking/get_bookings_response.json"), "UTF-8").useDelimiter("\\A").next();

        stubFor(post(urlEqualTo("/json/getBookings"))
                .willReturn(aResponse()
                        .withHeader("Content-Type","application/json")
                        .withBody(jsonResp)));



        resp = client.getBookings(req);

        Booking b = resp.get(0);
        assert b.getBookId().get().equals(4400966L);
        assert b.getRoomId().get().equals(54488L);
        assert b.getUnitId().get().equals(1L);
        assert b.getRoomQty().get().equals(1L);
        assert b.getStatus().get().equals(BookingStatus.CONFIRMED);
        assert b.getFirstNight().get().equals(FORMAT.parse("2017-10-07"));
        assert b.getLastNight().get().equals(FORMAT.parse("2017-10-07"));
        assert b.getNumAdult().get().equals(1L);
        assert b.getNumChild().get().equals(0L);
        assert b.getGuestTitle().get().equals("");
        assert b.getGuestFirstName().get().equals("asdf");
        assert b.getGuestName().get().equals("asdf");
        assert b.getGuestEmail().get().equals("asdf");
        assert b.getGuestPhone().get().equals("asdf");
        assert b.getGuestFax().get().equals("");
        assert b.getGuestAddress().get().equals("");
        assert b.getGuestCity().get().equals("");
        assert b.getGuestPostcode().get().equals("");
        assert b.getGuestCountry().get().equals("");
        assert b.getGuestArrivalTime().get().equals("");
        assert b.getGuestVoucher().get().equals("");
        assert b.getGuestComments().get().equals("");
        assert b.getNotes().get().equals("");
        assert b.getMessage().get().equals("");
        assert b.getCustom1().get().equals("");
        assert b.getCustom2().get().equals("");
        assert b.getCustom3().get().equals("");
        assert b.getCustom4().get().equals("");
        assert b.getCustom5().get().equals("");
        assert b.getCustom6().get().equals("");
        assert b.getCustom7().get().equals("");
        assert b.getCustom8().get().equals("");
        assert b.getCustom9().get().equals("");
        assert b.getCustom10().get().equals("");
        assert b.getCustom1().get().equals("");
        assert b.getFlagColor().get().equals("");
        assert b.getFlagText().get().equals("");
        assert b.getStatusCode().get().equals(0L);
        assert b.getLang().get().equals("");
        assert b.getPrice().get().equals(new BigDecimal("123.00"));
        assert b.getDeposit().get().equals(new BigDecimal("0.00"));
        assert b.getTax().get().equals(new BigDecimal("0.00"));
        assert b.getCommission().get().equals(new BigDecimal("0.00"));
        assert b.getCurrency().get().equals("EUR");
        assert b.getOfferId().get().equals(0L);
        assert b.getReferer().get().equals("godotest");
        assert b.getRefererEditable().get().equals("godotest");
        assert b.getReference().get().equals("");
        assert b.getApiSource().get().equals(Channel.GODO_PROPERTY);
        assert b.getApiReference().get().equals("");
        assert b.getAllowChannelUpdate().get().equals(true);
        assert b.getBookingTime().get().equals(FORMAT_TIME.parse("2017-10-04 12:56:41"));
        assert b.getModified().get().equals(FORMAT_TIME.parse("2017-10-04 12:56:53"));
        assert b.getMasterId().isPresent();
        assert b.getMasterId().get() == null ;


    }

    @Ignore
    public void testGetPropertiesAuthenticationError() {
        String jsonResp = "{\"error\":\"Unauthorized\",\"errorCode\":\"1002\"}";

        stubFor(post(urlEqualTo("/json/getProperties"))
        .willReturn(aResponse()
                .withHeader("Content-Type","application/json")
                .withBody(jsonResp)));

        try {
            client.getProperties("test");

        } catch(GodoPropertyException e){

            assertEquals("Unauthorized", e.getMessage());
            assert ErrorCode.AUTH_ERROR2.equals(e.getErrorCode());
            return;
        }
        // should not be reached
        assert false;
    }

    @Ignore
    public void testGetBookingsAuthenticationError() {
        String jsonResp = "{\"error\":\"Unauthorized\",\"errorCode\":\"1002\"}";

        stubFor(post(urlEqualTo("/json/getBookings"))
                .willReturn(aResponse()
                        .withHeader("Content-Type","application/json")
                        .withBody(jsonResp)));

        try {
            GetBookingsRequest req = new GetBookingsRequest();
            req.setAuthentication(new Authentication("apiKey"));
            client.getBookings(req);

        } catch(GodoPropertyException e){

            assertEquals("Unauthorized", e.getMessage());
            assert ErrorCode.AUTH_ERROR2.equals(e.getErrorCode());
            return;
        }
        // should not be reached
        assert false;
    }


    @Ignore
    public void testSanity(){
        assert ErrorCode.AUTH_ERROR2.equals(ErrorCode.AUTH_ERROR2);
        assert ErrorCode.AUTH_ERROR.equals(new ErrorCode("1000"));
      //  assertEquals(ErrorCode.AUTH_ERROR2, ErrorCode.AUTH_ERROR2);
       // assertEquals(ErrorCode.AUTH_ERROR2, new ErrorCode("1002"));
    }

    @Ignore("working but takes too long")
    public void getRoomDates()
        throws GodoPropertyException, ParseException {
        log.info("getRoomDates test");

        SimpleDateFormat Format = new SimpleDateFormat("yyyyMMdd");

        String apiKey = "Zv3LW633K5UHCqKJ";
        GodoPropertyClient client = new GodoPropertyClient();

        List<PropertyListItem> properties = client.getProperties(apiKey);

        for (PropertyListItem property : properties) {
            for (RoomType roomType : property.getRoomTypes()) {
                GetRoomDatesRequest getRoomDatesRequest = new GetRoomDatesRequest();
                getRoomDatesRequest.setFrom(new OptionalNull<Date>(Format.parse("20180501")));
                getRoomDatesRequest.setTo(new OptionalNull<Date>(Format.parse("20180503")));
                getRoomDatesRequest.setRoomId(roomType.getRoomId());

                Map<Date, RoomDate> dates = client.getRoomDates(apiKey, property.getPropKey(), getRoomDatesRequest);
                if (dates != null) {
                    log.info("Available room: " + roomType.getRoomId().get());
                    log.info( dates.toString());
                } else {
                    log.info("Unavailable room: " + roomType.getRoomId().get());
                }
            }
        }

    }

    @Test
    public void getAvailability() throws GodoPropertyException {
        log.info("=== getAvailability ===");
        GetAvailabilityRequest av = new GetAvailabilityRequest();

        try {
            av.setCheckIn(new OptionalNull<>(format.parse("20180501")));
            av.setCheckOut(new OptionalNull<>(format.parse("20180508")));
            av.setPropId(new OptionalNull<>(40282L));
            av.setRoomId(new OptionalNull<>(92957L));
            av.setNumAdult(new OptionalNull<>(2L));
        }
        catch( java.text.ParseException pe ) {
        }

        Map<Long, Availability> resp = client.getAvailabilities(av, true);

        for( Long room : resp.keySet() ) {
            log.info("Check in: " + av.getCheckIn());
            log.info("Check out: " + av.getCheckOut());
            log.info(" Room: " + room );
            log.info("   Availability: " + resp.get(room) );
        }
    }

    @Ignore
    public void getDailyAvailability() throws GodoPropertyException {
        log.info("=== getDailyAvailability ===");
        GetDailyAvailabilityRequest da = new GetDailyAvailabilityRequest();

        try {
            da.setCheckIn(new OptionalNull<>(format.parse("20180501")));
            da.setCheckOut(new OptionalNull<>(format.parse("20180508")));
            da.setQtyAvailable(new OptionalNull<>(true));
            ArrayList<Long> list = new ArrayList<>();
            list.add(34500L);
            list.add(34501L);
            da.setRoomIds(new OptionalNull<>(list));
        }
        catch( java.text.ParseException pe ) {
        }

        Map<Date, DailyAvailability> resp = client.getDailyAvailabilities(da);

        for( Date date : resp.keySet() ) {
            log.info(" Date: " + date );
            log.info("   Availability: " + resp.get(date) );
        }
    }

    @Ignore
    public void getBookings() throws GodoPropertyException {
        log.info("=== getBookings ===");
        GodoPropertyClient client = new GodoPropertyClient();
        GetBookingsRequest br = new GetBookingsRequest();
        br.setIncludeInvoice(new OptionalNull<>(true));
        Authentication auth = new Authentication("NpTYF4VKUBYyWfAf6Y53","EGxBEdczrdr3kFacDnck");
        br.setAuthentication(auth);
        List<Booking> bookings = client.getBookings(br, true);
        for( Booking b : bookings ) {
            log.info(b.toString());
            log.info(b.getInvoiceeHash(null).toString() );
        }
    }

    @Ignore
    public void getBookingsArnarstapi() throws GodoPropertyException {
        log.info("=== getBooking for Arnarstapi ===");
        GodoPropertyClient client = new GodoPropertyClient();
        GetBookingsRequest br = new GetBookingsRequest();
        Authentication auth = new Authentication("OKUUXQFrd37SRfHD","QqkwjMi6bXstLCX6");
        br.setAuthentication(auth);
        //br.setBookId(new OptionalNull<Long>(6095465L)); //Virkar ekki
        br.setBookId(new OptionalNull<Long>(4361155L));
        List<Booking> bookings = client.getBookings(br);
        for( Booking b : bookings ) {
            log.info(b.toString());
            log.info(b.getInvoiceeHash(null).toString() );
        }
    }

    @Ignore
    public void getBookingsByKey()  throws GodoPropertyException {
        log.info("== getBookingsBykey ==");
        List<Booking> bookings = client.getBookings("Zv3LW633K5UHCqKJ", "W2XdbjXRz2ZWJjmT", 5852665L);
        for( Booking b : bookings ) {
            log.info(b.toString());
            for( InvoiceLine il : b.getInvoice().get() ) {
                log.info(" --- " + il.toString());
            }
        }
    }

    @Ignore
    public void getInvoicees() throws GodoPropertyException {
        log.info( "== getInvoicees for nordurnes ==");
        GodoPropertyClient client = new GodoPropertyClient();
        GetInvoiceesRequest ir = new GetInvoiceesRequest();
        Authentication auth = new Authentication("Zv3LW633K5UHCqKJ","");
        ir.setAuthentication(auth);
        List<Invoicee> invoicees = client.getInvoicees(ir);
        for( Invoicee i : invoicees ) {
            log.info(i.toString());
        }
    }

    @Ignore
    public void getInvoicesForApiKey() throws GodoPropertyException {
        log.info( "== getInvoiceesForApiKey ==");
        GodoPropertyClient client = new GodoPropertyClient();
        HashMap<Long,Invoicee> invoicees = client.getInvoiceesForApiKey("Zv3LW633K5UHCqKJ");
        for( Map.Entry i : invoicees.entrySet() ) {
            log.info(i.getKey() + " - " + i.getValue());
        }
    }

    @Test
    public void setBooking() throws GodoPropertyException {
        log.info("setBooking test");
        GodoPropertyClient client = new GodoPropertyClient();
        SetBookingRequest req = new SetBookingRequest();
        String apiKey = "mysuperlongveryownkey";
        String propKey = "thisisthekeytohotelgillimann";
        Authentication auth = new Authentication(apiKey,propKey);
        req.setAuthentication(auth);
        /*
        Authentication auth = new Authentication("OKUUXQFrd37SRfHD","QqkwjMi6bXstLCX6");
        req.setAuthentication(auth);
        req.setBookId(new OptionalNull<>(6186301L ));
        req.setMessage(new OptionalNull<>("My message"));
        req.setCustom10(new OptionalNull<>("My custom message"));
        req.setNotifyHost(new OptionalNull<>("xx@example.com"));
    */

        try {
            //req.setRoomId(new OptionalNull<>(45243L));
            req.setFirstNight(new OptionalNull<>(format.parse("20180901")));
            req.setLastNight(new OptionalNull<>(format.parse("20180902")));
            req.setGuestName(new OptionalNull<>("Foxy Cleopatra"));
            req.setPrice(new OptionalNull<>(10L));
            req.setNumAdult(new OptionalNull<>(2L));
            req.setRoomId(new OptionalNull<>(97447L));
            req.setRoomQty(new OptionalNull<>(2L));

            InvoiceLine il = new InvoiceLine();
            il.setDescription(new OptionalNull<>("Goodies"));
            il.setPrice(new OptionalNull<BigDecimal>(new BigDecimal(12345)));
            List<InvoiceLine> list = new ArrayList<>();
            list.add(il);
            OptionalNull<List<InvoiceLine>> listarg = new OptionalNull<>(list);
            req.setInvoice(listarg);
            req.setStatus(new OptionalNull<>(BookingStatus.CONFIRMED));
            Long bookId = client.setBooking(req);
            log.info("set booking: bookId: " + bookId );
        }
        catch( ParseException pex ) {
            log.info("Wrong date format ???");
        }

    }

    @Ignore
    public void setMessageForBooking() throws GodoPropertyException {
        log.info("setMessageForBooking test");
        GodoPropertyClient client = new GodoPropertyClient();
        client.setMessageForBooking("OKUUXQFrd37SRfHD", "QqkwjMi6bXstLCX6",
                4361155L, "My special message");
    }

    @Ignore
    public void getProperty() throws GodoPropertyException {
        log.info("getProperty test");
        GetPropertyRequest req = new GetPropertyRequest();
        GodoPropertyClient client = new GodoPropertyClient();
        // new: LiGguubHtSbnuz97TvAv older: 7GlevjEz8yyzsKMlBNMf oldest: OKUUXQFrd37SRfHD
        // new: bFI7GhA7vedesQZh75QUVmztJyTXfoDj older: lPmReXywQN8EawNiR8Nb oldest: QqkwjMi6bXstLCX6
        Authentication auth = new Authentication("LiGguubHtSbnuz97TvAv","bFI7GhA7vedesQZh75QUVmztJyTXfoDj");
        req.setAuthentication(auth);
        Property p = client.getProperty(req);

        log.info("Property: " + p.toString());
    }

    @Ignore
    public void getPropertyByApiKeyAndPropKey() throws GodoPropertyException {
        log.info("getPropertyByApiAndProp test");
        GodoPropertyClient client = new GodoPropertyClient();
        Property p = client.getProperty("OKUUXQFrd37SRfHD", "QqkwjMi6bXstLCX6");
        log.info("PropertyByApiKey: " + p.toString());
    }

    @Ignore
    public void cancelBooking() throws GodoPropertyException {
        log.info("cancelBooking test");
        GodoPropertyClient client = new GodoPropertyClient();
        SetBookingRequest req = new SetBookingRequest();
        String apiKey = "Zv3LW633K5UHCqKJ";
        String propKey = "W2XdbjXRz2ZWJjmT";
        Authentication auth = new Authentication(apiKey,propKey);
        req.setAuthentication(auth);

        //List<PropertyListItem> properties = client.getProperties(apiKey);

        try {
            req.setBookId(new OptionalNull<>(5685750L));
            //req.setRoomId(new OptionalNull<>(45243L));
            //req.setFirstNight(new OptionalNull<>(format.parse("20180501")));
            //req.setLastNight(new OptionalNull<>(format.parse("20180502")));
            //req.setGuestName(new OptionalNull<>("Ronald Pluck"));
            //req.setPrice(new OptionalNull<>(10L));
            //req.setNumAdult(new OptionalNull<>(2L));
            //req.setRoomQty(new OptionalNull<>(2L));
            req.setStatus(new OptionalNull<>(BookingStatus.CANCELLED));
        }
        catch( Exception ex ) {
            log.info("Something went wrong :( " + ex.getMessage());
        }

        Long bookId = client.setBooking(req);
        log.info( "cancel booking, book ID: " + bookId );
    }

    @Ignore
    public void getProperties() throws GodoPropertyException {
        GodoPropertyClient client = new GodoPropertyClient();
        // Old key Zv3LW633K5UHCqKJ


        List<PropertyListItem> properties = client.getProperties("7GlevjEz8yyzsKMlBNMf");
        for( PropertyListItem p : properties ) {
            log.info(p.toString());
        }
    }

    @Ignore
    public void getAvailabilityDebug() throws GodoPropertyException {
        log.info("=== getAvailabilityDebug ===");
        GetAvailabilityRequest av = new GetAvailabilityRequest();

        try {
            av.setCheckIn(new OptionalNull<>(format.parse("20180601")));
            av.setCheckOut(new OptionalNull<>(format.parse("20180610")));
            av.setPropId(new OptionalNull<>(26108L));
            av.setRoomId(new OptionalNull<>(null));
            // 61993L
            av.setNumAdult(new OptionalNull<>(2L));
        }
        catch( java.text.ParseException pe ) {
            log.error("Failed to parse: ", pe);
        }

        Map<Long, Availability> resp = client.getAvailabilities(av, true);

        log.error("***************About to log response***************");
        for( Long room : resp.keySet() ) {
            log.info("Check in: " + av.getCheckIn());
            log.info("Check out: " + av.getCheckOut());
            log.info(" Room: " + room );
            log.info("   Availability: " + resp.get(room) );
        }
    }

    // @Ignore
    @Test
    public void getDailyAvailabilityDebug() throws GodoPropertyException {
        log.info("=== getDailyAvailabilityDebug ===");
        GetDailyAvailabilityRequest da = new GetDailyAvailabilityRequest();

        try {
            da.setCheckIn(new OptionalNull<>(format.parse("20180829")));
            da.setCheckOut(new OptionalNull<>(format.parse("20180831")));
            da.setQtyAvailable(new OptionalNull<>(true));
            ArrayList<Long> list = new ArrayList<>();
            list.add(61996L);
            list.add(61994L);
            da.setRoomIds(new OptionalNull<>(list));
        }
        catch( java.text.ParseException pe ) {
            log.error("Got this parse exception: ", pe);
        }

        Map<Date, DailyAvailability> resp = client.getDailyAvailabilities(da);

        try {
            resp = client.getDailyAvailabilities(da);

        } catch(Exception e) {
            log.error("client call failed: ", e);
        }

        for( Date date : resp.keySet() ) {
            log.info(" Date: " + date );
            log.info("   Availability: " + resp.get(date) );
        }
    }

    @Ignore("working but takes too long")
    public void getRoomDatesDebug()
            throws GodoPropertyException, ParseException {
        log.info("getRoomDates test");

        SimpleDateFormat Format = new SimpleDateFormat("yyyyMMdd");

        String apiKey = "Zv3LW633K5UHCqKJ";
        GodoPropertyClient client = new GodoPropertyClient();

        List<PropertyListItem> properties = client.getProperties(apiKey);

        for (PropertyListItem property : properties) {
            for (RoomType roomType : property.getRoomTypes()) {
                GetRoomDatesRequest getRoomDatesRequest = new GetRoomDatesRequest();
                getRoomDatesRequest.setFrom(new OptionalNull<Date>(Format.parse("20180615")));
                getRoomDatesRequest.setTo(new OptionalNull<Date>(Format.parse("20180620")));
                getRoomDatesRequest.setRoomId(roomType.getRoomId());

                Map<Date, RoomDate> dates = client.getRoomDates(apiKey, property.getPropKey(), getRoomDatesRequest);
                if (dates != null) {
                    log.info("Available room: " + roomType.getRoomId().get());
                    log.info( dates.toString());
                } else {
                    log.info("Unavailable room: " + roomType.getRoomId().get());
                }
            }
        }

    }

    @Ignore
    public void modifyProperty() throws GodoPropertyException {
        log.info("modifyProperty test");
        String apiKey = "mysuperlongveryownkey";
        String propKey = "thisisthekeytohotelgillimann";
        Property p = new Property();
        //p.setAction(new OptionalNull<>("modify"));
        p.setNotifyUrl(new OptionalNull<>("https://changed.with.test.com"));

        HashMap<String,String> result = client.modifyProperty(apiKey, propKey, p);

        if( result != null ) {
            //log.info(map.toString());
            for( String s : result.keySet() ) {
                log.info("Key: " + s );
                log.info( "Value: " + result.get(s) );
            }
        }
    }
}
