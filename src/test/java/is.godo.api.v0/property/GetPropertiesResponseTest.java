package is.godo.api.v0.property;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import is.godo.api.v0.json.EncodingAnnotationApplier;
import is.godo.api.v0.json.OptionalNull;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class GetPropertiesResponseTest {
    private static ObjectMapper om;

    @BeforeClass
    public static void setup(){
        om = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.setSerializerModifier(new EncodingAnnotationApplier());
        om.registerModule(module);
    }

    @Test
    public void testEmptyFields() throws JsonProcessingException {
        GetPropertiesResponse rt = new GetPropertiesResponse();
        String json = om.writeValueAsString(rt);
        assertEquals("{}", json);
    }

    @Test
    public void testEmptyList() throws JsonProcessingException {
        GetPropertiesResponse rt = new GetPropertiesResponse();
        rt.setGetProperties(new ArrayList<>());
        String json = om.writeValueAsString(rt);
        assertEquals("{\"getProperties\":[]}", json);
    }

    @Test
    public void testOneListItem() throws Exception {
        GetPropertiesResponse getProperties = new GetPropertiesResponse();
        PropertyListItem item = new PropertyListItem();
        item.setName("godotest");
        item.setPropKey("thisisapropertykey");
        item.setPropId(new OptionalNull<>(20372L));
        RoomType rt = new RoomType();
        rt.setName(new OptionalNull<>("Room 1"));
        rt.setQty(new OptionalNull<>(1L));
        rt.setRoomId(new OptionalNull<>(54293L));
        rt.setMinPrice(new OptionalNull<>(new BigDecimal("0.00")));


        List<PropertyListItem> list = new ArrayList<>();
        list.add(item);
        getProperties.setGetProperties(list);
        String json = om.writeValueAsString((getProperties));
        assertEquals("{\"getProperties\":[{\"name\":\"godotest\",\"propKey\":\"thisisapropertykey\",\"propId\":\"20372\"}]}", json);
    }

    @Test
    public void testManyListItems() throws Exception {
        GetPropertiesResponse rt = new GetPropertiesResponse();

        PropertyListItem item = new PropertyListItem();
        item.setName("godotest");
        item.setPropKey("thisisapropertykey");
        item.setPropId(new OptionalNull<>(20372L));

        RoomType rt1 = new RoomType();
        rt1.setName(new OptionalNull<>("Room 1"));
        rt1.setQty(new OptionalNull<>(1L));
        rt1.setRoomId(new OptionalNull<>(54293L));
        rt1.setMinPrice(new OptionalNull<>(new BigDecimal("0.00")));
        List<RoomType> list1 = new ArrayList<>();
        list1.add(rt1);
        item.setRoomTypes(list1);

        PropertyListItem item2 = new PropertyListItem();

        List<PropertyListItem> list = new ArrayList<>();
        list.add(item);
        list.add(item2);
        rt.setGetProperties(list);
        String json = om.writeValueAsString((rt));
        assertEquals("{\"getProperties\":[{\"name\":\"godotest\",\"propKey\":\"thisisapropertykey\",\"propId\":\"20372\",\"roomTypes\":[{\"name\":\"Room 1\",\"qty\":\"1\",\"roomId\":\"54293\",\"minPrice\":\"0.00\"}]},{}]}", json);
    }
}
