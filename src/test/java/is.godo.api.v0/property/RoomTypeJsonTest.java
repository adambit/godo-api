package is.godo.api.v0.property;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.fasterxml.jackson.databind.module.SimpleModule;
import is.godo.api.v0.json.Encoding;
import is.godo.api.v0.json.EncodingAnnotationApplier;
import is.godo.api.v0.json.OptionalNull;


import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.math.BigDecimal;

import java.util.Scanner;

import static org.junit.Assert.*;


public class RoomTypeJsonTest {
    private static final Logger log = LoggerFactory.getLogger(RoomTypeJsonTest.class);

    private static ObjectMapper om;

    @BeforeClass
    public static void setup(){
        om = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.setSerializerModifier(new EncodingAnnotationApplier());
        om.registerModule(module);
    }

    @Test
    public void testEmptyFields() throws JsonProcessingException {
        RoomType rt = new RoomType();
        String json = om.writeValueAsString(rt);
        assertEquals("{}", json);
    }

    @Test
    public void testJsonSerialize() throws Exception{

        RoomType rt = new RoomType();
        rt.setName(new OptionalNull<String>("Room 1"));
        rt.setQty(new OptionalNull<>(1L));

        rt.setRoomId(new OptionalNull<>( 49283L));
        rt.setMinPrice(new OptionalNull<>( new BigDecimal("0.00")));
        rt.setMaxPeople(new OptionalNull<>( 2L));
        rt.setMaxAdult(new OptionalNull<>( 0L));
        rt.setMaxChildren(new OptionalNull<>( 0L));
        rt.setUnitAllocationPerGuest(new OptionalNull<>( 0L));
        rt.setUnitNames(new OptionalNull<>( ""));
        rt.setHighlightColor(new OptionalNull<>( "ffffff"));
        rt.setDependentRoomId1(new OptionalNull<>( 0L));
        rt.setDependentRoomId2(new OptionalNull<>( 0L));
        rt.setDependentRoomId3(new OptionalNull<>( 0L));
        rt.setDependentRoomId4(new OptionalNull<>( 0L));
        rt.setDependentRoomId5(new OptionalNull<>( 0L));
        rt.setDependentRoomId6(new OptionalNull<>( 0L));
        rt.setDependentRoomId7(new OptionalNull<>( 0L));
        rt.setDependentRoomId8(new OptionalNull<>( 0L));
        rt.setDependentRoomId9(new OptionalNull<>( 0L));
        rt.setDependentRoomId10(new OptionalNull<>( 0L));
        rt.setDependentRoomId11(new OptionalNull<>( 0L));
        rt.setDependentRoomId12(new OptionalNull<>( 0L));
        rt.setDependentRoomLogic(new OptionalNull<>( RoomLogic.SUM_OF_ALL_BOOKINGS));
        rt.setP1Sync(new OptionalNull<>( 3L));
//        rt.setP2Sync(new OptionalNull<>( 0L));
        rt.setP3Sync(new OptionalNull<>( 0L));
        rt.setP4Sync(new OptionalNull<>( 0L));
        rt.setAgodaComEnableInventory(new OptionalNull<>( false));
        rt.setAgodaComEnablePrice(new OptionalNull<>( false));
        rt.setAgodaComEnableBooking(new OptionalNull<>( false));
        rt.setAgodaComRoomCode(new OptionalNull<>( ""));
        rt.setAirbnbComEnableInventory(new OptionalNull<>( false));
        rt.setAirbnbComEnableBooking(new OptionalNull<>( false));
        rt.setAirbnbComRoomCode(new OptionalNull<>( ""));
        rt.setBookingComEnableInventory(new OptionalNull<Boolean>( false).setEncoding(Encoding.STRING));
        rt.setBookingComEnableBooking(new OptionalNull<Boolean>( false).setEncoding(Encoding.NUMBER));
        rt.setBookingComRoomCode(new OptionalNull<>( ""));
        rt.setBookingComRateCode(new OptionalNull<>( ""));
        rt.setExpediaComEnableInventory(new OptionalNull<>( false));
        rt.setExpediaComEnablePrice(new OptionalNull<>( false));
        rt.setExpediaComEnableBooking(new OptionalNull<>( false));
        rt.setExpediaComRoomCode(new OptionalNull<>( ""));
        rt.setExpediaComRateCode(new OptionalNull<>( ""));
        rt.setIcalExportEnableType(new OptionalNull<>( "0"));
        rt.setIcalExportUrl( new OptionalNull<>("https://api.godo.com/ical/bookings.ics?roomid=49283&amp;token=75e3f5e5011929d705950b24d4b674b7"));
        rt.setIcalImport1EnableType(  new OptionalNull<>("0"));
        rt.setIcalImport1Url( new OptionalNull<>(""));
        rt.setIcalImport2EnableType(  new OptionalNull<>("0"));
        rt.setIcalImport2Url( new OptionalNull<>(""));
        rt.setIcalImport3EnableType(  new OptionalNull<>("0"));
        rt.setIcalImport3Url( new OptionalNull<>(""));


            String json = om.writerWithDefaultPrettyPrinter().writeValueAsString(rt);
           // System.out.printf("%s", json);

             String text = new Scanner(RoomTypeJsonTest.class.getResourceAsStream("/property/roomtype.json"), "UTF-8").useDelimiter("\\A").next();

            assertEquals(text, json);

    }

    @Test
    public void testSimpleJson() throws Exception{
        RoomType rt1 = new RoomType();
        rt1.setName(new OptionalNull<>("Room 1"));
        rt1.setQty(new OptionalNull<>(1L));
        rt1.setRoomId(new OptionalNull<>(54293L));
        rt1.setMinPrice(new OptionalNull<>(new BigDecimal("0.00")));


        String json = om.writeValueAsString(rt1);
        assertEquals("{\"name\":\"Room 1\",\"qty\":\"1\",\"roomId\":\"54293\",\"minPrice\":\"0.00\"}", json);
    }

    @Test
    public void testDeserializeUrl() throws Exception{
        String json = "{  \"icalExportUrl\" : \"https:\\/\\/api.godo.com\\/ical\\/bookings.ics?roomid=49283&amp;token=75e3f5e5011929d705950b24d4b674b7\"}";
        RoomType rt = om.readValue(json, RoomType.class);
        Assert.assertEquals("https://api.godo.com/ical/bookings.ics?roomid=49283&amp;token=75e3f5e5011929d705950b24d4b674b7", rt.getIcalExportUrl().get());
        //rt.setIcalExportUrl( new OptionalNull<>("https://api.godo.com/ical/bookings.ics?roomid=49283&amp;token=75e3f5e5011929d705950b24d4b674b7"));
    }
}
