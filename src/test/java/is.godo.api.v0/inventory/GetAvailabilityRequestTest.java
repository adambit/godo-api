package is.godo.api.v0.inventory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import is.godo.api.v0.GodoPropertyClient;
import is.godo.api.v0.json.EncodingAnnotationApplier;
import is.godo.api.v0.json.OptionalNull;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;

/**
 * Created by zuzana on 2.11.2017.
 */
public class GetAvailabilityRequestTest {
    private static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyyMMdd");
    private static ObjectMapper om;

    @BeforeClass
    public static void setup(){
        om = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.setSerializerModifier(new EncodingAnnotationApplier());
        om.registerModule(module);
    }


    @Test
    public void testEmptySerializationU() throws Exception {
        GetAvailabilityRequest req = new GetAvailabilityRequest();
        String json = om.writeValueAsString(req);
        assertEquals("{}", json);
    }

    @Test
    public void testComplexJson() throws Exception {
        GetAvailabilityRequest req = new GetAvailabilityRequest();
        req.setCheckIn(new OptionalNull<>(FORMAT.parse("20171112")));
        req.setLastNight(new OptionalNull<>(FORMAT.parse("20171212")));
        req.setCheckOut(new OptionalNull<>(FORMAT.parse("20171212")));
        req.setRoomId(new OptionalNull<>(3l));
        req.setPropId(new OptionalNull<>(463L));
        req.setOwnerId(new OptionalNull<>(4l));
        req.setNumAdult(new OptionalNull<>(2L));
        req.setNumChild(new OptionalNull<>(1L));
        req.setOfferId(new OptionalNull<>(12L));
        req.setVoucherCode(new OptionalNull<>(""));
        req.setIgnoreAvail(new OptionalNull<>(false));


        String json = om.writerWithDefaultPrettyPrinter().writeValueAsString(req);

        /*String expected = new Scanner(SetAvailabilityRequestTest.class.getResourceAsStream("/inventory/getAvailabilityRequest.json"), "UTF-8").useDelimiter("\\A").next();

        assertEquals(expected, json);*/
    }

    @Ignore
    public void testImprovedGetAvailabilities() throws Exception {
        try {
            GetAvailabilityRequest req = new GetAvailabilityRequest();

            req.setCheckIn(new OptionalNull<>(FORMAT.parse("20180501")));
            req.setCheckOut(new OptionalNull<>(FORMAT.parse("20180503")));
            req.setPropId(new OptionalNull<>(45180L));
            req.setNumAdult(new OptionalNull<>(2L));

            GodoPropertyClient client = new GodoPropertyClient("https://api.godo.is/v0");
            Map<Long, Availability> av = client.getAvailabilities(req);

            System.out.println(av.toString());
        }
        catch( Exception e ) {
            e.printStackTrace();
        }
    }

    @Ignore
    public void testSpecialJson() throws Exception {
        GetAvailabilityRequest req = new GetAvailabilityRequest();

        req.setCheckIn(new OptionalNull<>(FORMAT.parse("20180501")));
        //req.getCheckIn().setPattern("yyyyMMdd");
        req.setCheckOut(new OptionalNull<>(FORMAT.parse("20180503")));
        //req.getCheckOut().setPattern("yyyyMMdd");
        //req.setRoomId(new OptionalNull<>(104661L));
        req.setPropId(new OptionalNull<>(45180L));
        req.setNumAdult(new OptionalNull<>(2L));

        GodoPropertyClient client = new GodoPropertyClient("https://api.godo.is/v0");
        Map<Long, Availability> av = client.getAvailabilities(req);

        System.out.println("testSpecialJson results: ");
        for( Long key : av.keySet() )
            System.out.println("avail: " + av.get(key) );

        assert(true);
    }

    private String convertToJson( String str ) {
        String result = "";

        return result;
    }
}
