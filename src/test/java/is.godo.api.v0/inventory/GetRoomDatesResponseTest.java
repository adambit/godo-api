package is.godo.api.v0.inventory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import is.godo.api.v0.json.EncodingAnnotationApplier;
import org.junit.BeforeClass;
import org.junit.Test;

public class GetRoomDatesResponseTest
{

    private static ObjectMapper om;

    @BeforeClass
    public static void setup(){
        om = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.setSerializerModifier(new EncodingAnnotationApplier());
        om.registerModule(module);
    }

    @Test
    public void testComplexJsonDeserialization(){

    }
}
