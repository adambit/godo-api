package is.godo.api.v0.inventory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import is.godo.api.v0.json.OptionalNull;
import is.godo.api.v0.Authentication;
import is.godo.api.v0.json.EncodingAnnotationApplier;
import org.junit.BeforeClass;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class GetRoomDatesRequestTest {

    private static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyyMMdd");
    private static ObjectMapper om;

    @BeforeClass
    public static void setup(){
        om = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.setSerializerModifier(new EncodingAnnotationApplier());
        om.registerModule(module);
    }


    @Test
    public void testEmptySerializationU() throws Exception {
        GetRoomDatesRequest req = new GetRoomDatesRequest();
        String json = om.writeValueAsString(req);
        assertEquals("{}", json);
    }

    @Test
    public void testComplexJson() throws Exception {
        Authentication auth = new Authentication("apiKeyAsSetInAccountSettings","propKeyAsSetForTheProperty");
        GetRoomDatesRequest req = new GetRoomDatesRequest();
        req.setAuthentication(auth);;
        req.setRoomId(new OptionalNull<>(12345L));
        req.setFrom(new OptionalNull<>(FORMAT.parse("20171007")));
        req.setTo(new OptionalNull<>(FORMAT.parse("20171105")));
        req.setIncOverride(new OptionalNull<>(false));
        req.setIncMultiplier(new OptionalNull<>(false));
        req.setAllowInventoryNegative(new OptionalNull<>(false));

        String json = om.writerWithDefaultPrettyPrinter().writeValueAsString(req);

        String expected = new Scanner(SetRoomDatesRequestTest.class.getResourceAsStream("/inventory/getRoomDatesRequest.json"), "UTF-8").useDelimiter("\\A").next();

        assertEquals(expected, json);
    }
}

