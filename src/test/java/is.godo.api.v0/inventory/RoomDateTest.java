package is.godo.api.v0.inventory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import is.godo.api.v0.json.EncodingAnnotationApplier;
import is.godo.api.v0.json.OptionalNull;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class RoomDateTest {
    private static ObjectMapper om;

    @BeforeClass
    public static void setup(){
        om = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.setSerializerModifier(new EncodingAnnotationApplier());
        om.registerModule(module);
    }

    @Test
    public void testEmptySerialize() throws  Exception{
        String json = om.writeValueAsString(new RoomDate());
        assertEquals("{}", json);
    }


    @Test
    public void testSimpleJsonSerialize() throws  Exception{
        RoomDate rd = new RoomDate();

        rd.setInventory(new OptionalNull<>(0L));

        // Represent % as decimal in code but serialize as percentage. e.g. 1.23 = 123% => serializes to "123"
        rd.setMultiplierDecimal(new OptionalNull<>(new BigDecimal("1.23")));
        rd.setOverrideStatus(new OptionalNull<>(OverrideStatus.BLACKOUT));
        rd.setMinimumStay(new OptionalNull<>(5L));


        String json = om.writeValueAsString(rd);
        assertEquals("{\"i\":\"0\",\"m\":\"5\",\"o\":1,\"x\":\"123\"}", json);
    }



    @Test
    public void testEmptyJsonDeserialize() throws  Exception {
        String json = "{}";
        RoomDate rd = om.readValue(json, RoomDate.class);
        assertEquals(null, rd.getInventory());
        assertEquals(null, rd.getMinimumStay());
        assertEquals(null, rd.getMultiplierDecimal());
        assertEquals(null, rd.getOverrideStatus());
        assertEquals(null, rd.getPrice1());
        assertEquals(null, rd.getPrice2());
        assertEquals(null, rd.getPrice3());
        assertEquals(null, rd.getPrice4());
    }


    @Test
    public void testSimpleJsonDeserialize() throws  Exception {
        String json = "{\"i\":123,\"x\":321}";
        RoomDate rd = om.readValue(json, RoomDate.class);
        assertEquals(new BigDecimal("3.21"), rd.getMultiplierDecimal().get());
    }

}
