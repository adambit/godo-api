package is.godo.api.v0.inventory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import is.godo.api.v0.Authentication;
import is.godo.api.v0.json.EncodingAnnotationApplier;
import is.godo.api.v0.json.OptionalNull;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

public class SetRoomDatesRequestTest {
    private final static SimpleDateFormat FORMAT = new SimpleDateFormat("yyyyMMdd");
    private static ObjectMapper om;

    @BeforeClass
    public static void setup(){
        om = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.setSerializerModifier(new EncodingAnnotationApplier());
        om.registerModule(module);
    }

    @Test
    public void testEmptySerialization() throws Exception {
        SetRoomDatesRequest req = new SetRoomDatesRequest();
        String json = om.writeValueAsString(req);
        assertEquals("{}", json);
    }


    @Test
    public void testComplexSerialization() throws Exception {
        Authentication auth = new Authentication("apiKeyAsSetInAccountSettings","propKeyAsSetForTheProperty");


        SetRoomDatesRequest req = new SetRoomDatesRequest();
        req.setAuthentication(auth);
        req.setRoomId(new OptionalNull<>(12345L));

        Map<Date,RoomDate> dates = new HashMap<>();
        RoomDate one = new RoomDate();
        one.setPrice1(new OptionalNull<>(new BigDecimal("45.00")));
        one.setPrice2(new OptionalNull<>(new BigDecimal("55.00")));
        one.setPrice3(new OptionalNull<>(new BigDecimal("65.00")));
        one.setPrice4(new OptionalNull<>(new BigDecimal("75.00")));
        one.setInventory(new OptionalNull<>(1L));


        RoomDate two = new RoomDate();
        two.setInventory(new OptionalNull<>(0L));

        RoomDate three = new RoomDate();
        three.setPrice1(new OptionalNull<>(new BigDecimal("49.99")));
        three.setMinimumStay(new OptionalNull<>(2L));

        Date dateOne = FORMAT.parse("20141015");
        Date dateTwo = FORMAT.parse("20141016");
        Date dateThree = FORMAT.parse("20141019");

        dates.put(dateOne, one);
        dates.put(dateTwo, two);
        dates.put(dateThree, three);

        req.setDates(dates);

        String json = om.writerWithDefaultPrettyPrinter().writeValueAsString(req);
        String text = new Scanner(SetRoomDatesRequestTest.class.getResourceAsStream("/inventory/setRoomDatesRequest.json"), "UTF-8").useDelimiter("\\A").next();

        assertEquals(text,json);
    }

    @Test
    public void testComplexDeserialization() throws Exception {
        Authentication auth = new Authentication("apiKeyAsSetInAccountSettings","propKeyAsSetForTheProperty");

        SetRoomDatesRequest expected = new SetRoomDatesRequest();
        expected.setAuthentication(auth);
        expected.setRoomId(new OptionalNull<>(12345L));

        Map<Date, RoomDate> dates = new HashMap<>();
        RoomDate one = new RoomDate();
        one.setPrice1(new OptionalNull<>(new BigDecimal("45.00")));
        one.setPrice2(new OptionalNull<>(new BigDecimal("55.00")));
        one.setPrice3(new OptionalNull<>(new BigDecimal("65.00")));
        one.setPrice4(new OptionalNull<>(new BigDecimal("75.00")));
        one.setInventory(new OptionalNull<>(1L));


        RoomDate two = new RoomDate();
        two.setInventory(new OptionalNull<>(0L));

        RoomDate three = new RoomDate();
        three.setPrice1(new OptionalNull<>(new BigDecimal("49.99")));
        three.setMinimumStay(new OptionalNull<>(2L));

        Date dateOne = FORMAT.parse("20141015");
        Date dateTwo = FORMAT.parse("20141016");
        Date dateThree = FORMAT.parse("20141019");

        dates.put(dateOne, one);
        dates.put(dateTwo, two);
        dates.put(dateThree, three);

        expected.setDates(dates);

        String json = new Scanner(SetRoomDatesRequestTest.class.getResourceAsStream("/inventory/setRoomDatesRequest.json"), "UTF-8").useDelimiter("\\A").next();
        SetRoomDatesRequest actual = om.readValue(json, SetRoomDatesRequest.class);

        // Compare authentication.
        assertEquals(expected.getAuthentication().getApiKey(), actual.getAuthentication().getApiKey());
        assertEquals(expected.getAuthentication().getPropKey(), actual.getAuthentication().getPropKey());

        // Compare room ID
        assertEquals(expected.getRoomId().get(), actual.getRoomId().get());



        // Compare the date => RoomDate
        Set<Map.Entry<Date,RoomDate>> actualDates = actual.getDates().entrySet();
        for(Map.Entry<Date,RoomDate> actualPair: actualDates){
            // Try to get the expected room date from the actual dates map.
            // If it's not there then the date is handled wrong.
            RoomDate expectedRoomDate = dates.get(actualPair.getKey());
            RoomDate actualRoomDate = actualPair.getValue();



            if(expectedRoomDate.getInventory() != null) {
                assert expectedRoomDate.getInventory().equals(actualRoomDate.getInventory());
            }
            if(expectedRoomDate.getMinimumStay() != null) {
                assert expectedRoomDate.getMinimumStay().equals(actualRoomDate.getMinimumStay());
            }
            if(expectedRoomDate.getMultiplierDecimal() != null) {
                assert expectedRoomDate.getMultiplierDecimal().equals(actualRoomDate.getMultiplierDecimal());
            }
            if(expectedRoomDate.getPrice1() != null) {
                assert expectedRoomDate.getPrice1().equals(actualRoomDate.getPrice1());
            }
            if(expectedRoomDate.getOverrideStatus() != null) {
                assert expectedRoomDate.getOverrideStatus().equals(actualRoomDate.getOverrideStatus());
            }
            if(expectedRoomDate.getPrice2() != null) {
                assert expectedRoomDate.getPrice2().equals(actualRoomDate.getPrice2());
            }
            if(expectedRoomDate.getPrice3() != null) {
                assert expectedRoomDate.getPrice3().equals(actualRoomDate.getPrice3());
            }
            if(expectedRoomDate.getPrice4() != null)
            {
                assert expectedRoomDate.getPrice4().equals(actualRoomDate.getPrice4());
            }

        }
    }
}
