package is.godo.api.v0;

import is.godo.api.v0.inventory.Availability;
import is.godo.api.v0.inventory.GetAvailabilityRequest;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Created by zuzana on 17.11.2017.
 */

public class IntegrationTest {

    private GodoPropertyClient client;

    @Before
    public void beforeEach(){
        client = new GodoPropertyClient();
    }

    @Ignore
    public void testGetAvailabilities() throws GodoPropertyException {
        GetAvailabilityRequest req = new GetAvailabilityRequest();

        Map<Long, Availability> resp = client.getAvailabilities(req);

        assertEquals("", resp);

    }
}
